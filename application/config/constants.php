<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');



/*

|--------------------------------------------------------------------------

| File and Directory Modes

|--------------------------------------------------------------------------

|

| These prefs are used when checking and setting modes when working

| with the file system.  The defaults are fine on servers with proper

| security, but you may wish (or even need) to change the values in

| certain environments (Apache running a separate process for each

| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should

| always be used to set the mode correctly.

|

*/

define('FILE_READ_MODE', 0644);

define('FILE_WRITE_MODE', 0666);

define('DIR_READ_MODE', 0755);

define('DIR_WRITE_MODE', 0777);



/*

|--------------------------------------------------------------------------

| File Stream Modes

|--------------------------------------------------------------------------

|

| These modes are used when working with fopen()/popen()

|

*/



define('FOPEN_READ',							'rb');

define('FOPEN_READ_WRITE',						'r+b');

define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care

define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care

define('FOPEN_WRITE_CREATE',					'ab');

define('FOPEN_READ_WRITE_CREATE',				'a+b');

define('FOPEN_WRITE_CREATE_STRICT',				'xb');

define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');



/*

|--------------------------------------------------------------------------

| Custon define variable 

| Define by ajay @ March 10 2013

|--------------------------------------------------------------------------

*/
define('PROJECT_NAME', '');
if($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "panel.clubmate.in"){
    $baseUrl = 'https://'.$_SERVER['HTTP_HOST'].'/'.PROJECT_NAME.'/';    
    $docUrl = $_SERVER['DOCUMENT_ROOT'].'/'.PROJECT_NAME.'/';
} elseif($_SERVER['HTTP_HOST'] == "clubmate.dataklouds.co.in") {
	$baseUrl = 'http://'.$_SERVER['HTTP_HOST'].'/';    
    $docUrl = $_SERVER['DOCUMENT_ROOT'].'/';
}

define('ROOT_SITE_PATH', $baseUrl);
define('BASE_URL',                              rtrim($baseUrl,'/')); //removing last slash (/)
define('ADMIN_ASSETS_PATH',						ROOT_SITE_PATH.'assets/admin/');
define('ADMIN_PATH',                              'admin');     
//admin & dashboard path
define('ADMIN_LOGIN_PATH',					    'administrator');
define('SITE_NAME','');
define('API_PATH', 'api');
define('BACK_UP_PATH', 'backup');

/* Location: ./application/config/constants.php */