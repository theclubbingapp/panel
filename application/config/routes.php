<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');



/*



| -------------------------------------------------------------------------



| URI ROUTING



| -------------------------------------------------------------------------



| This file lets you re-map URI requests to specific controller functions.



|



| Typically there is a one-to-one relationship between a URL string



| and its corresponding controller class/method. The segments in a



| URL normally follow this pattern:



|



|	example.com/class/method/id/



|



| In some instances, however, you may want to remap this relationship



| so that a different class/function is called than the one



| corresponding to the URL.



|



| Please see the user guide for complete details:



|



|	http://codeigniter.com/user_guide/general/routing.html



|



| -------------------------------------------------------------------------



| RESERVED ROUTES



| -------------------------------------------------------------------------



|



| There area two reserved routes:



|



|	$route['default_controller'] = 'welcome';



|



| This route indicates which controller class should be loaded if the



| URI contains no data. In the above example, the "welcome" class



| would be loaded.



|



|	$route['404_override'] = 'errors/page';



|



| This route will tell the Router what URI segments to use if those provided



| in the URL cannot be matched to a valid route.



|



*/

/*

*@  Root  sating  By Katoch  

*/

$route['default_controller']                           = "login/admin";

$route['page/(:any)']                                  = 'page/page/admin/$1';

$route['404_override']                                 = 'error/page/e404';

$route['404']                                          = 'error/page/e404';
$route[API_PATH]            = 'api/index';
$route[ADMIN_LOGIN_PATH]                               = 'login/admin';
$route[ADMIN_LOGIN_PATH.'/([a-zA-Z_-]+)/(:any)']       =  '$1/admin/$2';
$route[ADMIN_PATH]                                     = 'admin/admin';
$route[ADMIN_PATH.'/logout']                           = 'login/admin/logout';
$route[ADMIN_PATH.'/([a-zA-Z_-]+)/(:any)']             = '$1/admin/$2';
$route[BACK_UP_PATH]                                     = 'backup/backup';
$route[BACK_UP_PATH.'/([a-zA-Z_-]+)/(:any)']             = '$1/backup/$2';
$route['404_override'] = 'templates/page_not_found'; 
/* Location: ./application/config/routes.php */