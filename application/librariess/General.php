<?php if (!defined('BASEPATH'))

    exit('No direct script access allowed');



class General{

    /**

     * CodeIgniter global

     *

     * @var string

     **/

    protected $ci;



    /**

     * account status ('not_activated', etc ...)

     *

     * @var string

     **/

    protected $status;



    /**

     * error message (uses lang file)

     *

     * @var string

     **/

    protected $errors = array();

    public function __construct() {
         $this->ci = &get_instance();
        define("GOOGLE_API_KEY","AIzaSyCCByIFoPHqsL9JHATAQeZ06j1tr_wrdRo");
        $this->ci = &get_instance();
        //Check block id and exit it.
        $user_ip = $this->get_real_ipaddr();
    }
    /*
    *@ IP Address Get 
    */
       //find user real ip address
    public function get_real_ipaddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) //check ip from share internet

            $ip = $_SERVER['HTTP_CLIENT_IP'];
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            //to check ip is pass from proxy

            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else
            $ip = $_SERVER['REMOTE_ADDR'];

        return $ip;
    }


    /// Get the single column

    public function getcolumn($col, $table, $key, $val)
                {

            $this->ci->db->select($col); 
            $this->ci->db->from($table);   
            $this->ci->db->where($key, $val);
            $this->ci->db->order_by($col);
            return $this->ci->db->get()->row()->$col;

        }

    public function getcolumnwhere($col,$table,$key,$val,$key2,$val2)
    {

            $this->ci->db->select($col); 

            $this->ci->db->from($table);   

            $this->ci->db->where($key, $val);
            $this->ci->db->where($key2, $val2);
                        

            return $this->ci->db->get()->row()->$col;

        }
                
                
 public function getcolumnwhereall($col,$table,$key,$val,$key2,$val2,$key3,$val3){

            $this->ci->db->select($col); 

            $this->ci->db->from($table);   

            $this->ci->db->where($key, $val);
            $this->ci->db->where($key2, $val2);
                        $this->ci->db->where($key3, $val3);
                        

            return $this->ci->db->get()->row()->$col;

        }
            

public function get_single_record($table, $field, $key, $value) {
        $this->ci->db->where($key, $value);
        $this->ci->db->select($field);
        $query = $this->ci->db->get($table);
        //echo $this->ci->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->row(1);
        }
        return false;
    }
       /****** Check phone Number **/
    public function checkphone_number($phone_number) {
    $query = $this->ci->db->order_by('phone_number', 'ASC')->get_where('users', array('phone_number' => $phone_number));
        return $query->result_array();
    }  
}//End class

