<div class="container-fluid">
<!-- Breadcrumbs-->
<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="<?php echo site_url(ADMIN_PATH);?>">Dashboard</a>
  </li>
  <li class="breadcrumb-item active">Packages List </li>
</ol>

<?php if ($this->session->flashdata('success')) { ?>
			<h3>
				  
			<?php $flash_Message =$this->session->flashdata('success');
			echo "<div style='color:green;'>$flash_Message<div>";
			 ?>
			</h3>
			<?php } ?>
			<?php if ($this->session->flashdata('error')) { ?>
			<h3>
			<?php $flash_Message =$this->session->flashdata('error');
			echo "<div style='color:red;'>$flash_Message<div>"; ?>
			</h3>
			<?php } ?>
        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            User  Tables</div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  <th>S.no</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Allowed Users</th>
                    <th>Allowed Calls</th>
                    <th>Price </th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
               <!-- <tfoot>
                  <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Office</th>
                    <th>Age</th>
                    <th>Start date</th>
                    <th>Salary</th>
                  </tr>
                </tfoot>-->
                <tbody>
                    <?php if($plan){
                      $i ='1';
                        foreach($plan as $val){  ?>
                        <tr>
                      <td><?=$i++;?></td>
                      <td><?=$val['name']?></td>
                       <td><?=$val['description']?></td>
                       <td><?=$val['allowedUsers']?></td>
                       <td><?=$val['allowedCalls']?></td>
                       <td><?=$val['price']?></td>
                       <td><?php  if($val['status'] == '1'){?>
                            <span class="badge badge-success"><?php echo ('Active');?></span> 
                     <?php  } elseif($val['status'] == '3'){?>
                      <span class="badge badge-danger"><?php echo ('Not Active');?></span> 
                     <?php }?></td>
                     
                        <td>
                        <a href="<?php echo site_url(ADMIN_PATH."/admin/".$edit_plan."/".$val['id']);?>" title="Edit plan">
                        <i class="fas fa-user-edit"></i></a>
                        <?php if($val['status'] =='3'){?>
                          <a href="<?php echo site_url(ADMIN_PATH."/admin/".$transaction."/Unblock/".$val['id']);?>" title="Un block plan">
                        <i class="fas fa-ban"></i></a>
                        <?php }else{ ?>
                          <a href="<?php echo site_url(ADMIN_PATH."/admin/".$transaction."/Block/".$val['id']);?>" title="Block plan">
                        <i class="fas fa-ban"></i></a>
                        <?php }?>
                       
                        </td>

                        
                    </tr>

                    <?php  }
                   }else{?>
                    <tr>
                    <td colspan ='5'>Data Not Found</td>
                    
                  </tr>

                  <?php  }?>
                 
                  
                </tbody>
              </table>
            </div>
          </div>
        
        </div>

      </div>
      <!-- /.container-fluid -->