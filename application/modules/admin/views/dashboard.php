<div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Overview</li>
        </ol>

        <!-- Icon Cards-->
        <div class="row">
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-primary o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-comments"></i>
                </div>
                <div class="mr-5"><?php $this->db->from('club_users');
                    echo $this->db->count_all_results();?>  All Users!</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="<?php echo site_url(ADMIN_PATH."/users_list");?>">
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-info o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-comments"></i>
                </div>
                <div class="mr-5"><?php $this->db->from('club_users');
                                        $this->db->where('status','1');
                    echo $this->db->count_all_results();?>  Active Users!</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="<?php echo site_url(ADMIN_PATH."/active_users_list");?>">
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-secondary o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-comments"></i>
                </div>
                <div class="mr-5"><?php $this->db->from('club_users');
                                 $this->db->where('status','3');
                    echo $this->db->count_all_results();?>  All Bolck  Users!</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="<?php echo site_url(ADMIN_PATH."/block_users_list");?>">
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>

          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-warning o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-list"></i>
                </div>
                <div class="mr-5"><?php $this->db->from('club_event');
                               
                    echo $this->db->count_all_results();?> All New Events!</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="<?php echo site_url(ADMIN_PATH."/events_new");?>">
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-dark o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-list"></i>
                </div>
                <div class="mr-5"><?php $this->db->from('club_event');
                    echo $this->db->count_all_results();?> All Old Events!</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="<?php echo site_url(ADMIN_PATH."/events_old");?>">
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-success o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-shopping-cart"></i>
                </div>
                <div class="mr-5"><?php echo $this->db->get_where('club_subscriptionplanmaster_activate',array('transaction_type'=>'1','activate_status'=>'1'))->num_rows();
                    ;?>  All Active Monthly Plan </div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="<?php echo site_url(ADMIN_PATH."/month_transaction");?>">
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-warning o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-shopping-cart"></i>
                </div>
                <div class="mr-5"><?php echo $this->db->get_where('club_subscriptionplanmaster_activate',array('transaction_type'=>'2','activate_status'=>'1'))->num_rows();
                    ;?> All Active Yearly Plan </div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="<?php echo site_url(ADMIN_PATH."/year_transaction");?>">
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-danger o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-life-ring"></i>
                </div>
                <div class="mr-5"><?php
                $data=$this->db
                ->select_sum('amount')
                ->from('club_subscriptionplanmaster_activate')
                ->order_by('amount desc')
                ->get();
             echo  $data->row()->amount;?> All Transactions!</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="#">
                <span class="float-left"></span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
        </div>






        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
        Results  Daly Transactions</div>
          <div class="card-body">
            <div class="widget-body no-padding container_overflow_scroll">                            
                <table id="" class="table table-striped table-bordered table-hover dt_basic2">
                            <thead>
                            <tr>
                            <th data-hide="">Sr.No</th>
                            <th data-class="expand">Date</th>
                            <th data-hide="">Number of Transaction</th>
                            <th data-hide="">Daily Amount</th>
                        </tr>                    
                    </thead>
                    <tbody>
                    <?php 
                    if(!empty($daily_transactions)) {
                      //print_r($daily_transactions);die;
                        $sn = 0;
                        foreach($daily_transactions as $data)  {?>
                        <tr class="gradeU">
                      <td><?php echo ++$sn;?></td>
                            <td><?php echo $data['date'];?></td>
                            <td><?php echo $data['total_number'];?></td>
                            <td><?php echo "&#8377;". $data['total_amount'];?></td>
                        </tr>
                    <?php
                      }                                	
                    }else{
                      echo "No Tranction";
                    }            
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>



    <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Results for Weekly Transactions</div>
          <div class="card-body">
            <div class="widget-body no-padding container_overflow_scroll">                            
                <table id="" class="table table-striped table-bordered table-hover dt_basic2">
                    <thead>
                        <tr>
                            <th data-hide="">Sr.No</th>
                            <th data-class="expand">weekly</th>
                            <th data-hide="">Number of Transaction</th>
                            <th data-hide=""> Amount</th>
                        </tr>                    
                    </thead>
                    <tbody>
                    <?php 
                    if($dash_weekly_transaction) {  $sn = 0;
                        foreach($dash_weekly_transaction as $data){      

                        ?>
                        <tr class="gradeU">
                            <td><?php echo ++$sn;?></td>
                            <td><?php echo date('F',strtotime($data['date']));?></td>
                            <td><?php echo $data['total_number'];?></td>
                            <td><?php echo "&#8377; ". $data['total_amount'];?></td>
                        </tr>
                    <?php
                        }                                   
                    }            
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>




    <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Results for Monthly Transactions</div>
          <div class="card-body">
            <div class="widget-body no-padding container_overflow_scroll">                            
                <table id="" class="table table-striped table-bordered table-hover dt_basic2">
                    <thead>
                        <tr>
                            <th data-hide="">Sr.No</th>
                            <th data-hide="">Year</th>
                            <th data-class="expand">Month</th>
                            <th data-hide="">Number of Transaction</th>
                            <th data-hide="">Daily Amount</th>
                        </tr>                    
                    </thead>
                    <tbody>
                    <?php 

                    if($monthly_transactions){
                        $sn = 0;
                        foreach($monthly_transactions as $data){?>
                        <tr class="gradeU">
                            <td><?php echo ++$sn;?></td>
                            <td><?php echo $data['year'];?></td>
                            <td><?php echo date('F',strtotime($data['date']));?></td>
                            <td><?php echo $data['total_number'];?></td>
                            <td><?php echo "&#8377; ". $data['total_amount'];?></td>
                        </tr>
                    <?php
                        }                                   
                    }            
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

        <!-- Area Chart Example-->
       <!-- <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-chart-area"></i>
            Area Chart Example</div>
          <div class="card-body">
            <canvas id="myAreaChart" width="100%" height="30"></canvas>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>-->

        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            User  Tables</div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  <th>S.no</th>
                   <th>Image</th>
                    <th>Name</th>
                    <th>Phone Number</th>
                    <th>Gender</th>
                    <th>Age</th>
                    <th>Start date</th>
                    <th>last login</th>
                  </tr>
                </thead>
               <!-- <tfoot>
                  <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Office</th>
                    <th>Age</th>
                    <th>Start date</th>
                    <th>Salary</th>
                  </tr>
                </tfoot>-->
                <tbody>
                    <?php if($user_array){
                      $i ='1';
                        foreach($user_array as $val){  ?>
                        <tr>
                      <td><?=$i++;?></td>
                      <td> <img src="<?php echo $this->admin_model->get_image_url('users',$val['id']);?>" class="img-circle" width="30" /></td>
                      <td><?=$val['full_name']?></td>
                        <td><?=$val['phone_number']?></td>
                        <td><?php  
                      $data = json_decode($val['gender'], TRUE);
                      foreach($data as $gender){
                        if($gender['isChecked'] == '1')
                         echo  $gender['value'];

                      }
                           
                           ?></td>
                        <td><?php $today = date("d-m-Y");
                            $diff = date_diff(date_create($val['dob']), date_create($today));
                           echo $diff->format('%y');?></td>
                        <td><?=$val['add_on']?></td>
                        <td><?php if($val['last_login']){
                           echo $val['last_login'];
                        }else{
                        echo 'N/A';}?></td>
                    </tr>

                    <?php  }
                   }else{?>
                    <tr>
                    <td colspan ='5'>Data Not Found</td>
                    
                  </tr>

                  <?php  }?>
                 
                  
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>

      </div>
      <!-- /.container-fluid -->