<link rel="stylesheet" href="<?=ADMIN_ASSETS_PATH?>/build/css/intlTelInput.css">

<div class="container-fluid" aligen="center">
<!-- Breadcrumbs-->
<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="<?php echo site_url('admin/'.$user_list);?>"><?=$user_list?></a>
  </li>
  <li class="breadcrumb-item active">Add Users</li>
</ol>

<?php if ($this->session->flashdata('success')) { ?>
			<h3>
				  
			<?php $flash_Message =$this->session->flashdata('success');
			echo "<div style='color:green;'>$flash_Message<div>";
			 ?>
			</h3>
			<?php } ?>
			<?php if ($this->session->flashdata('error')) { ?>
			<h3>
			<?php $flash_Message =$this->session->flashdata('error');
			echo "<div style='color:red;'>$flash_Message<div>"; ?>
			</h3>
			<?php } ?>
<?php echo form_open(base_url().'admin/admin/users_profile_edit/update/'.$id.'/'.$redrect_url , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>
                           <?php if($user_array){
                          //     echo "<pre>";print_r($user_array);die;

                           }?>
                                    <div class="form-group ">
                                        <label for="full_name" class="control-label col-lg-3">Full Name</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="full_name" name="full_name" type="text"  value="<?php echo set_value('full_name',$user_array[0]['full_name']); ?>"  placeholder="Full name"  >
                                        </div>
                                        <div style='color:red;'>
                                        <?php echo form_error('full_name'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="password" class="control-label col-lg-3">Password</label>
                                        <div class="col-lg-6">
                                        <input class="form-control" id="password" name="password" type="password" value="<?php echo set_value('password'); ?>">
                                        </div>
                                        <div style='color:red;'>
                                        <?php echo form_error('password'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="password_confirmation" class="control-label col-lg-3">Password confirmation</label>
                                        <div class="col-lg-6">
                                        <input class="form-control" id="password_confirmation" name="password_confirmation" type="password" value="<?php echo set_value('password_confirmation'); ?>" >
                                        </div>
                                        <div style='color:red;'>
                                        <?php echo "If You not Change password then send blank field" ?>
                                        <?php echo form_error('password_confirmation'); ?>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="phone_number" class="control-label col-lg-3">Phone Name</label>
                                        <div class="col-lg-6">
                                        <input class="form-control" id="phone" name="phone_number" type="tel" value="<?php echo set_value('phone_number',$user_array[0]['phone_number']); ?>"  pplaceholder="Enter  phone number  With Countre code" required>
                                        </div>
                                        <div style='color:black;'>
                                        <?php echo "Enter 10 digit mobile number with country code like +91123654789" ?>
                                        </div>
                                        <div style='color:red;'>
                                        <?php echo form_error('phone_number'); ?>
                                        </div>
                                    </div>
  
                                    
                                    <div class="form-group">
                                        <label for="dob" class="control-label col-lg-3">Date of birth</label>
                                        <div class="col-lg-6">
                                        <input class="form-control" id="dob" name="dob" type="date" value="<?php echo set_value('dob',$user_array[0]['dob']); ?>" min="1950-01-01" max="2037-12-31" required>
                                        </div>
                                        <div style='color:red;'>
                                        <?php echo form_error('dob'); ?>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="bio" class="control-label col-lg-3">Bio</label>
                                        <div class="col-lg-6">
                                        <textarea  id="bio" name="bio"  rows="4" cols="70" required><?php echo set_value('bio',$user_array[0]['bio']); ?></textarea>
                                          
                                        </div>
                                        <div style='color:red;'>
                                        <?php echo form_error('bio'); ?>
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <label for="gender" class="control-label col-lg-3" required>Gender</label>
                                        <div class="col-lg-6">
                                            <select class="form-control select2" name="gender" id="gender">
                                            <?php $genders = json_decode($user_array['0']['gender'], TRUE);
                                                        foreach($genders as $val){
                                                            if($val['isChecked'] == true){
                                                             $gend= $val['value'];
                                                            }
                                                        }?>
                                                <option value="Male"<?php if('Male' == $gend)echo 'selected';?>>Male</option>
                                                <option value="Female"<?php if('Female' == $gend)echo 'selected';?>>Female</option>
                                            </select>
                                          </div>
                                          <div style='color:red;'>
                                        <?php echo form_error('gender'); ?>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group ">
                                        <label for="food" class="control-label col-lg-3">Food</label>
                                        <div class="col-lg-6">
                                            <select class="form-control select2" name="food" id="food" required>
                                            <?php $foods = json_decode($user_array['0']['food'], TRUE);
                                                        foreach($foods as $val){
                                                            if($val['isChecked'] == true){
                                                             $food = $val['value'];
                                                            }
                                                        }?>
                                                <option value="Veg"<?php if('Veg' == $food)echo 'selected';?>>Veg</option>
                                                <option value="Non-Veg"<?php if('Non-Veg' == $food)echo 'selected';?>>Non-Veg</option>
                                            </select>
                                          </div>
                                          <div style='color:red;'>
                                        <?php echo form_error('food'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="dance" class="control-label col-lg-3">Dance</label>
                                        <div class="col-lg-6">
                                        <?php $dances = json_decode($user_array['0']['dance'], TRUE);
                                                        foreach($dances as $val){
                                                            if($val['isChecked'] == true){
                                                             $dance = $val['value'];
                                                            }
                                                        }?>
                                            <select class="form-control select2" name="dance" id="dance" required>
                                                <option value="Dance"<?php if('Dance' == $dance)echo 'selected';?>>Dance</option>
                                                <option value="No-Dance"<?php if('No-Dance' == $dance)echo 'selected';?>>No-Dance</option>
                                            </select>
                                          </div>
                                          <div style='color:red;'>
                                        <?php echo form_error('dance'); ?>
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <label class="control-label col-lg-3">Typeof Drink</label>
                                        <div  class="col-lg-12">
                                        <?php $type_of_drinks = json_decode($user_array['0']['type_of_drink'], TRUE);
                                       // print_r($type_of_drinks);die;
                                       $i=1;
                                                        foreach($type_of_drinks as $val){?>
                                                             <input type="checkbox" name="<?php $val['value']?>" value="<?php echo $val['value'] ?>"<?php echo ($val['isChecked']=='1' ? 'checked' : '');?> style="width: 30px;height:16px !important;"><?php echo $val['value'];?>
                                                             <?php 
                                                       $i++;
                                                    }
                                                            ?>
                                                        
     
                                        
                                    </div>
                                       
                                    </div>
                                    <div class="form-group ">
                                        <label class="control-label col-lg-3">Intrested In</label>
                                        <div  class="col-lg-6" >
                                        <?php $intrested_ins = json_decode($user_array['0']['intrested_in'], TRUE);
                                                        foreach($intrested_ins as $val){
                                                            if($val['isChecked'] == true){
                                                             $intrested_ins = $val['value'];
                                                            }
                                                        }?>
                                        <select class="form-control select2" name="intrested_in" id="Intrested" required>
                                              <option value="Not Intrested"<?php if('Intrested' == $plan["0"]["intrested_in"])echo 'selected';?>>Not Intrested</option>
                                                <option value="Male"<?php if('Male' == $intrested_ins)echo 'selected';?>>Male</option>
                                                <option value="Female"<?php if('Female' == $intrested_ins)echo 'selected';?>>Female</option>
                                            </select>
                                          </div>
                                    
                                    </div>

                                    <div class="form-group ">
                                        <label class="control-label col-lg-3">image</label>
                                        <div  class="col-lg-12" >
                                        <input type="file" name="userfile" value="">
                                        
                                          </div>
                                       
                                    </div>

                                    
                                    <div class="form-group">
                                        <div class="col-lg-offset-3 col-lg-6">
                                            <button class="btn btn-primary" type="submit">Save</button>
                                        </div>
                                    </div>
                                    <?php echo form_close();?>
                            </div>
                            
                            <script src="<?=ADMIN_ASSETS_PATH?>build/js/intlTelInput.js"></script>
  <script>
    var input = document.querySelector("#phone");
    window.intlTelInput(input, {
      // allowDropdown: false,
      // autoHideDialCode: false,
      // autoPlaceholder: "off",
      // dropdownContainer: document.body,
      // excludeCountries: ["us"],
      // formatOnDisplay: false,
      // geoIpLookup: function(callback) {
      //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
      //     var countryCode = (resp && resp.country) ? resp.country : "";
      //     callback(countryCode);
      //   });
      // },
      // hiddenInput: "full_number",
      // initialCountry: "auto",
      // localizedCountries: { 'de': 'Deutschland' },
      // nationalMode: false,
      // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
      // placeholderNumberType: "MOBILE",
      // preferredCountries: ['cn', 'jp'],
      // separateDialCode: true,
      utilsScript: "<?=ADMIN_ASSETS_PATH?>build/js/utils.js",
    });
  </script>