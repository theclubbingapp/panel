<div class="container-fluid">

<!-- Breadcrumbs-->
<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="<?php echo site_url(ADMIN_PATH."/".$events_list);?>">Events List</a>
  </li>
  <li class="breadcrumb-item active">Events Views </li>
</ol>

<!-- Page Content -->
   <!-- DataTables Example -->
   <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Event</div>
          <div class="card-body center">
          <?php if($all_events){
            foreach($all_events as $val){?>
              <div style="width: 50%; float:left">
                Club Name : <?=$val['club_name'];?>
              </div>

              <div style="width: 50%; float:right">
                  User Nmae : <?=$val['full_name'];?>
              </div>
              <div style="width: 50%; float:left">
              Drinks 	: <?php $data = json_decode($val['offering_a_drink'], TRUE);
                      foreach($data as $offering_a_drink){
                        if($offering_a_drink['isChecked'] == true){
                         echo  $offering_a_drink['value'].',';
                        }
                      }
                       ?>
              </div>

              <div style="width: 50%; float:right">
              Group type : <?=$val['group_type'];?>
              </div>
              <div style="width: 50%; float:left">
               Event Type: <?=$val['type'];?>
              </div>


              <div style="width: 50%; float:right">
               Club Entry  Date : <?=$val['date'];?>
              </div>
              <div style="width: 50%; float:left">
               Club Entry  Time : <?=$val['time'];?>
              </div>

              <div style="width: 50%; float:right">
              Add On : <?=$val['add_on'];?>
              </div>
</div>


<div class="table-responsive">
              <h5> Members</h5>
              <table class="table table-bordered" id="" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  <th>S.no</th>
                   <th>Image</th>
                    <th>Member Name</th>
                    <th>Phone Number</th>
                    <th>Gender</th>
                    <th>Age</th>
                  <th>Add on</th>
                  </tr>
                </thead>
               <!-- <tfoot>
                  <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Office</th>
                    <th>Age</th>
                    <th>Start date</th>
                    <th>Salary</th>
                  </tr>
                </tfoot>-->
                <tbody>
                <?php $user_member = $this->db->get_where('club_user_member',array('event_id' =>$val['event_id'] ,'user_id' =>$val['id'] ,'member_states'=>'1'))->result_array();?>
                    <?php if($user_member){
                      $i ='1';
                        foreach($user_member as $value){ 
                          $userid = $this->db->get_where('club_users',array('phone_number'=>$value['member_phone_number']))->row()->id;
                           ?>
                        <tr>
                      <td><?=$i++;?></td>
                      <td> <img src="<?php echo $this->admin_model->get_image_url('users',$userid);?>" class="img-circle" width="30" /></td>
                      <td><?=$value['member_name']?></td>
                        <td><?=$value['member_phone_number']?></td>
                        <td><?=$value['member_sex']?></td></td>
                        <td><?=$value['member_age']?></td>
                        <td><?=$value['m_add_on']?></td>
                    </tr>

                    <?php  }
                   }else{?>
                    <tr>
                    <td colspan ='5'>Data Not Found</td>
                    
                  </tr>

                  <?php  }?>
                 
                  
                </tbody>
              </table>
            </div>

                 <!-- DataTables Example -->
   
            <div class="table-responsive">
            <h5> PairUp</h5>
              <table class="table table-bordered" id="" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  <th>S.no</th>
                  <th>Image</th>
                   <th>Pair To</th>
                    <th>Phone Number</th>
                    <th>Gender</th>
                    <th>DOB</th>
                  <th>Add on</th>
                  </tr>
                </thead>
               <!-- <tfoot>
                  <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Office</th>
                    <th>Age</th>
                    <th>Start date</th>
                    <th>Salary</th>
                  </tr>
                </tfoot>-->
                <tbody>
                <?php $user_pairup = $this->db->get_where('club_my_pairup',array('event_id' =>$val['event_id'] ,'user_id' =>$val['id']))->result_array();?>
                    <?php if($user_pairup){
                      $i ='1';
                        foreach($user_pairup as $value){ 
                          $friend_profile = $this->db->get_where('club_users',array('id'=>$value['friend_id']))->result_array();
                           ?>
                        <tr>
                      <td><?=$i++;?></td>
                      <td> <img src="<?php echo $this->admin_model->get_image_url('users',$value['friend_id']);?>" class="img-circle" width="30" /></td>
                      <td><?=$friend_profile['0']['full_name']?></td>
                        <td><?=$friend_profile['0']['phone_number']?></td>
                        <td> <?php $data = json_decode($val['gender'], TRUE);
                      foreach($data as $gender){
                        if($gender['isChecked'] == true){
                         echo  $gender['value'].',';
                        }
                      }
                       ?></td></td>
                        <td><?=$friend_profile['0']['dob']?></td>
                        <td><?=$friend_profile['0']['add_on']?></td>
                    </tr>

                    <?php  }
                   }else{?>
                    <tr>
                    <td colspan ='5'>Data Not Found</td>
                    
                  </tr>

                  <?php  }?>
                 
                  
                </tbody>
              </table>
            </div>


 <!-- DataTables Example -->
   
 <div class="table-responsive">
            <h5> Drink Request Accept</h5>
              <table class="table table-bordered" id="" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  <th>S.no</th>
                   <th>Drink By</th>
                  </tr>
                </thead>
               <!-- <tfoot>
                  <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Office</th>
                    <th>Age</th>
                    <th>Start date</th>
                    <th>Salary</th>
                  </tr>
                </tfoot>-->
                <tbody>
                <?php $user_drink = $this->db->get_where('club_accept_drink_request',array('event_id' =>$val['event_id'] ,'send_by' =>$val['id'],'status'=>'1'))->result_array();?>
                    <?php if($user_drink){
                      $i ='1';
                        foreach($user_drink as $value){ 
                          $friend_profile = $this->db->get_where('club_users',array('id'=>$value['friend_id']))->result_array();
                           ?>
                        <tr>
                      <td><?=$i++;?></td>
                      <td><?=$friend_profile['0']['full_name']?></td>
                        
                    </tr>

                    <?php  }
                   }else{?>
                    <tr>
                    <td colspan ='5'>Data Not Found</td>
                    
                  </tr>

                  <?php  }?>
                 
                  
                </tbody>
              </table>
            </div>
          
         
          <?php }}else{
            echo "Data Not Found";
            }?>
      </div>

      


</div>