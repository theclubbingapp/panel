<link rel="stylesheet" href="<?=ADMIN_ASSETS_PATH?>/build/css/intlTelInput.css">

<div class="container-fluid" aligen="center">
<!-- Breadcrumbs-->
<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="<?php echo site_url('admin/users_list'.$user_list);?>">Uers list</a>
  </li>
  <li class="breadcrumb-item active">Change Monthly Paln</li>
</ol>

<?php if ($this->session->flashdata('success')) { ?>
			<h3>
				  
			<?php $flash_Message =$this->session->flashdata('success');
			echo "<div style='color:green;'>$flash_Message<div>";
			 ?>
			</h3>
			<?php } ?>
			<?php if ($this->session->flashdata('error')) { ?>
			<h3>
			<?php $flash_Message =$this->session->flashdata('error');
			echo "<div style='color:red;'>$flash_Message<div>"; ?>
			</h3>
			<?php } ?>
<?php echo form_open(base_url().'admin/admin/plan_monthly/creat/'.$user_id.'/'.$page_name , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>
                                    <div class="form-group ">
                                        <label for="plan" class="control-label col-lg-3" required>Select Plan</label>
                                        <div class="col-lg-6">
                                            <select class="form-control select2" name="plan" id="plan">
                                            <?php if(!empty($month_plan)){
                                                foreach($month_plan as  $val){?>
                                                   <option value="<?=$val['id']?>"><?=$val['name']?></option>
                                             <?php   }
                                            }?>
                                            </select>
                                          </div>
                                          <div style='color:red;'>
                                        <?php echo form_error('plan'); ?>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group ">
                                        <label for="Pyament_method" class="control-label col-lg-3">Pyament method</label>
                                        <div class="col-lg-6">
                                            <select class="form-control select2" name="Pyament_method" id="Pyament_method" required>
                                                <option value="Cash ">Cash</option>
                                               
                                            </select>
                                          </div>
                                          <div style='color:red;'>
                                        <?php echo form_error('Pyament_method'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-offset-3 col-lg-6">
                                            <button class="btn btn-primary" type="submit">Save</button>
                                        </div>
                                    </div>
                                    <?php echo form_close();?>