<div class="container-fluid">
<!-- Breadcrumbs-->
<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="<?php echo site_url(ADMIN_PATH);?>">Dashboard</a>
  </li>
  <li class="breadcrumb-item active">Pair Up lists</li>
</ol>
        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Pairup  Tables</div>
          <div class="card-body">
            <div class="table-responsive">
            <button onclick="exportTableToExcel('dataTable', 'members-data')">Downlaod Excel File</button>  <input type="button" id="btnExport" value="Export PDF" onclick="Export()" />
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  <th>S.no</th>
                  <th>User Name</th>
                  <th>Packages Name</th>
                  <th>Transaction ID</th>
                  <th>Amount</th>
                  <th>Method</th>
                  <th>Allowed Users</th>
                  <th>Allowed Calls</th>
                  <th>Status</th>
                  <th>Expiry date</th>
                  <th>Activate Status</th>
                  
                 
                  <!--  <th>Action</th>-->
                  </tr>
                </thead>
            <!--   <tfoot>
                  <tr>
                   <th>S.no</th>
                  <th>User Name</th>
                  <th>Packages Name</th>
                  <th>Transaction ID</th>
                  <th>Amount</th>
                  <th>Method</th>
                  <th>Allowed Users</th>
                  <th>Allowed Calls</th>
                  <th>Status</th>
                  <th>Expiry date</th>
                  <th>Activate Status</th>
                  </tr>
                </tfoot>-->
                <tbody>
                    <?php if($month_transaction){
                      $i ='1';
                        foreach($month_transaction as $val){  ?>
                        <tr>
                      <td><?=$i++;?></td>
                      <td>  <?=$val['full_name']?></td>
                        <td><?=$val['name']?></td>
                         <td><?= strlen($val['transaction_id'])>25?(substr($val['transaction_id'], 0, 25)."..."):($val['transaction_id'])?></td>
                        <td><?=$val['amount']?></td>
                        <td><?=$val['method']?></td>
                        <td><?=$val['allowedUsers']?></td>
                        <td><?=$val['allowedCalls']?></td>
                        <td><?=$val['status']?></td>
                        <td><?=$val['date']?></td>
                        <td> <?php  if($val['activate_status'] == '1'){?>
                            <span class="badge badge-success"><?php echo ('Active');?></span> 
                     <?php  } elseif($val['activate_status'] == '3'){?>
                      <span class="badge badge-danger"><?php echo ('Expired');?></span> 
                     <?php }?>
                     </td>
                        
                    </tr>

                    <?php  }
                   }else{?>
                    <tr>
                    <td colspan ='5'>Data Not Found</td>
                    
                  </tr>

                  <?php  }?>
                 
                  
                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>
      <!-- /.container-fluid -->
      <script type="text/javascript">
       function exportTableToExcel(dataTable, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(dataTable);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
}
</script>
  
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
    <script type="text/javascript">
        function Export() {
            html2canvas(document.getElementById('dataTable'), {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: 500
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download("Table.pdf");
                }
            });
        }
    </script>