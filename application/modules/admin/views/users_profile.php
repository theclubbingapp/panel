
<div class="container-fluid">
<!-- Breadcrumbs-->
<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="<?php echo site_url(ADMIN_PATH."/users_list");?>">User Lists</a>
  </li>
  <li class="breadcrumb-item active">Profile View</li>
</ol>

<?php if ($this->session->flashdata('success')) { ?>
			<h3>
				  
			<?php $flash_Message =$this->session->flashdata('success');
			echo "<div style='color:green;'>$flash_Message<div>";
			 ?>
			</h3>
			<?php } ?>
			<?php if ($this->session->flashdata('error')) { ?>
			<h3>
			<?php $flash_Message =$this->session->flashdata('error');
			echo "<div style='color:red;'>$flash_Message<div>"; ?>
			</h3>
			<?php } ?>
<link href="<?=ADMIN_ASSETS_PATH?>/css/profilecss.css" rel="stylesheet" type="text/css">
  <div id="w">
    <div id="content" class="clearfix">
  
      <div id="userphoto"><img src="<?php echo $this->admin_model->get_image_url('users',$user_id);?>" alt="default avatar" class="img-circle" width="150px" height="150px"></div>
      <h1> <?php echo $this->db->get_where('club_users',array('id'=>$user_id))->row()->full_name;?>  Profile </h1>

      <nav id="profiletabs">
        <ul class="clearfix">
          <li><a href="#bio" class="sel">Bio</a></li>
        <!--  <li><a href="#event">Events</a></li>-->
          <li><a href="#friends">Friends</a></li>
          <li><a href="#blockfriends">Block Friends</a></li>
          <li><a href="#sentfriend">Send Request </a></li>
          <li><a href="#receivedfriend">Received Request</a></li>
          <li><a href="#settings">Settings</a></li>
        </ul>
      </nav>
      
      <section id="bio">
        <?php  if($user_array){
           foreach($user_array  as $val){?>
        <p><font size="4">Bio :</font> <?=$val['bio']; ?></p>
      
        <p><font size="4"> Food :</font> <?php $data = json_decode($val['food'], TRUE);
                      foreach($data as $food){
                        if($food['isChecked'] == true)
                         echo  $food['value'].',';

                      } ?>
        </p> 
        <p><font size="4">Type of Drinks :</font><?php $data = json_decode($val['type_of_drink'], TRUE);
                      foreach($data as $type_of_drink){
                        if($type_of_drink['isChecked'] == true)
                         echo  $type_of_drink['value'].',';

                      } ?>
        </p>
        <p><font size="4">Intrested :</font> <?php $data = json_decode($val['intrested_in'], TRUE);
                      foreach($data as $intrested_in){
                        if($intrested_in['isChecked'] == true)
                         echo  $intrested_in['value'].',';

                      } ?>
        </p> 
        

      <?php }}else{
         echo "data not Found";
      }?>
       </section>
      
      <section id="event" class="hidden">
               <!-- DataTables Example -->
     <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Events  Tables</div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr><font >
                  <th>S.no</th>
                    <th>Club Name</th>
                    <th>Book date</th>
                    <th>Book Time </th>
                    <th>members</th>
                    <th>Group_type</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  <th>S.no</th>
                    <th>Club Name</th>
                    <th>Book date</th>
                    <th>Book Time </th>
                    <th>members</th>
                    <th>Group_type</th>
                  </tr>
                </tfoot>
                <tbody>
                    <?php if($events){
                      $i ='1';
                        foreach($events as $val){  ?>
                        <tr>
                      <td><?=$i++;?></td>
                      <td><?=$val['club_name']?></td>
                        <td><?=$val['date']?></td>
                        <td><?=$val['time']?></td>
                        <td><?=$val['time']?></td>
                        <td><?=$val['group_type']?></td>
                       </font>
                    </tr>

                    <?php  }
                   }else{?>
                    <tr>
                    <td colspan ='5'>Data Not Found</td>
                    
                  </tr>

                  <?php  }?>
                 
                  
                </tbody>
              </table>
            </div>
          </div>
          
        </div>

   
      </section>
      
      <section id="friends" class="hidden">
        <p>Friends list:</p>
        
        <ul id="friendslist" class="clearfix">
        <?php if($friends){
             $i ='1';
             foreach($friends as $val){  ?>
          <li><a href="<?php echo site_url(ADMIN_PATH."/admin/users_friends_profiles/".$val['friend'].'/'.$val['user_id']);?>" title="View Profile"><img src="<?php echo $this->admin_model->get_image_url('users',$val['friend']);?>" class="img-circle" width="22" height="22" /><?=$val['full_name']?></a></li>
          <?php }}else{
            echo "Data not Found !";
          }?>
        </ul>
      </section>
      
      <section id="settings" class="hidden">
        <p> your user settings:</p>
        <?php  if($user_array){
           foreach($user_array  as $val){?>
        <p class="setting"><span>Phone Number</span> <?=$val['phone_number']?></p>
        
        <p class="setting"><span>Birthday Date  </span><?=$val['dob']?></p>
        
        <p class="setting"><span>Gender</span> <?php  
                      $data = json_decode($val['gender'], TRUE);
                      foreach($data as $gender){
                        if($gender['isChecked'] == '1'){
                         echo  $gender['value'];
                        }

                      }?></p>
        
        <p class="setting"><span>Dance</span> 
        <?php $data = json_decode($val['dance'], TRUE);
                      foreach($data as $food){
                        if($food['isChecked'] == true){
                         echo  $food['value'].',';
                        }
                      }
                       ?> </p>
       <p class="setting"><span>Active Plan </span> 
        <?php  $get_plane =$this->db->get_where('club_subscriptionplanmaster_activate',array('user_id'=>$user_id,'activate_status'=>'1'))->result_array();
                    if($get_plane['0']['transaction_type'] =='1'){
                        echo $this->db->get_where('club_monthly_subscriptionplanmaster',array('id'=>$get_plane['0']['subscriptionplanmaster_id']))->row()->name;
                    }elseif($get_plane['0']['transaction_type'] =='2'){
                      echo $this->db->get_where('club_subscriptionplanmaster',array('id'=>$get_plane['0']['subscriptionplanmaster_id']))->row()->name;
                    }else{
                    echo "Plan Not Active please contact Admin";
                    }
                    ?> </p>
       <p class="setting"><span>Plan expiry date </span> 
        <?php echo $this->db->get_where('club_subscriptionplanmaster_activate',array('user_id'=>$user_id,'activate_status'=>'1'))->row()->date;
                       ?> </p>
       <p class="setting"><span>Change Plane  </span> 
       <a  class="btn btn-primary" href="<?php echo site_url(ADMIN_PATH."/admin/change_user_plan/".$user_id.'/users_profile');?>" title="Un Block Profile">
                       Change  plan</a> </p>
           <?php }}?>
      </section>


      <section id="blockfriends" class="hidden">
      <p>Friends list:</p>
        
        <ul id="friendslist" class="clearfix">
        <?php if($blockfriends){
             $i ='1';
             foreach($blockfriends as $val){  ?>
          <li><a href="<?php echo site_url(ADMIN_PATH."/admin/users_friends_profiles/".$val['friend'].'/'.$val['user_id']);?>" title="View Profile"><img src="<?php echo $this->admin_model->get_image_url('users',$val['friend']);?>" class="img-circle" width="22" height="22" /><?=$val['full_name']?></a></li>
          <?php }}else{
            echo "Data not Found !";
          }?>
        </ul>
      </section>
     
     
      <section id="sentfriend" class="hidden">
      <p>Friends list:</p>
        
        <ul id="friendslist" class="clearfix">
        <?php if($sendrequest){
             $i ='1';
             foreach($sendrequest as $val){  
               $user_id=$this->db->get_where('club_users',array('apiKey'=>$val['user_id']))->row()->id;
               ?>
          <li><a href="<?php echo site_url(ADMIN_PATH."/admin/users_friends_request_profiles/".$val['friend'].'/'.$user_id.'/'.$val['request_id'].'/send');?>" title="View Profile"><img src="<?php echo $this->admin_model->get_image_url('users',$val['friend']);?>" class="img-circle" width="22" height="22" /><?=$val['full_name']?></a></li>
          <?php }}else{
            echo "Data not Found !";
          }?>
        </ul>
      </section>

      <section id="receivedfriend" class="hidden">
      <p>Friends list:</p>
        
        <ul id="friendslist" class="clearfix">
        <?php if($receivedfriend){
         
             $i ='1';
             foreach($receivedfriend as $val){
              $user_id=$this->db->get_where('club_users',array('apiKey'=>$val['user_id']))->row()->id;  ?>
          <li><a href="<?php echo site_url(ADMIN_PATH."/admin/users_friends_request_profiles/".$val['friend'].'/'.$user_id.'/'.$val['request_id'].'/rec');?>" title="View Profile"><img src="<?php echo $this->admin_model->get_image_url('users',$user_id);?>" class="img-circle" width="22" height="22" /><?=$val['full_name']?></a></li>
          <?php }}else{
            echo "Data not Found !";
          }?>
        </ul>
      </section>


    </div><!-- @end #content -->
  </div><!-- @end #w -->
  <script src="<?=ADMIN_ASSETS_PATH?>js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(function(){
  $('#profiletabs ul li a').on('click', function(e){
    e.preventDefault();
    var newcontent = $(this).attr('href');
    
    $('#profiletabs ul li a').removeClass('sel');
    $(this).addClass('sel');
    
    $('#content section').each(function(){
      if(!$(this).hasClass('hidden')) { $(this).addClass('hidden'); }
    });
    
    $(newcontent).removeClass('hidden');
  });
});
</script>
