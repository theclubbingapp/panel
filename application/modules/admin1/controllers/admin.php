<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*	
 *	@Add : Hr katoch 
 *	date	: 20 April, 2019
 */

class Admin extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		/*  modle */
       		/*  modle */
        $this->load->model('admin_model');
        $this->load->model('email_model');
        $this->load->helper('url', 'form');
        $this->load->library('form_validation');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		   
		
    }
    /***default functin, redirects to login page if no admin logged in yet***/
    public function index()
    {
         if ($this->session->userdata('admin_login') != 1){
           redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
        }
        elseif ($this->session->userdata('admin_login') == 1){
            redirect(ADMIN_PATH.'/dashboard', 'refresh');exit;
          }
    }
    
    /***ADMIN DASHBOARD***/
    function dashboard($user_id='0')
    {
     if ($this->session->userdata('admin_login') != 1){
        $this->session->set_flashdata('error','Something is missing');
           redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
        }
        $this->data['daily_transactions'] = $daily_transactions = $this->admin_model->dash_daily_transaction();
        $this->data['monthly_transactions'] = $monthly_transactions = $this->admin_model->dash_monthly_transaction();
         $this->data['dash_weekly_transaction'] = $dash_weekly_transaction= $this->admin_model->dash_weekly_transaction();
              $this->data['user_array'] = $this->admin_model->get_all_user($user_id='0');
              $this->data['page_name'] = 'dashboard';
              $this->data['page_title']= 'Admin-Dashboard';
               $this->template
                ->set_layout('admin')
                ->enable_parser(FALSE)
                ->title(SITE_NAME . ' - dashboard')
                ->build('dashboard', $this->data);
    }
    public function add_news_users($param1='',$param2='',$param3=''){
      if ($this->session->userdata('admin_login') != 1){
        redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
     }
     if($param1 == 'creat'){
       
      $this->form_validation->set_rules('full_name', 'Full Name', 'required|min_length[3]');
      $this->form_validation->set_rules('password', 'Password', 'required||min_length[5]');
      $this->form_validation->set_rules('password_confirmation', 'Password Confirmation', 'required|matches[password]');
      $this->form_validation->set_rules('phone_number', 'Phone Name', 'required|is_unique[club_users.phone_number]');
      $this->form_validation->set_rules('dob', 'Date of birth', 'required');
      $this->form_validation->set_rules('gender', 'Gender', 'required');
      $this->form_validation->set_rules('food', 'Food', 'required');
      $this->form_validation->set_rules('dance', 'Dance', 'required');
     
      if ($this->form_validation->run() == FALSE)
      {
        $this->data['page_name'] = 'user_list';
              $this->data['page_title']= 'Users-Add';
              $this->data['redrct_url']= 'users_list';
      $this->template
                ->set_layout('admin')
                ->enable_parser(FALSE)
                ->title(SITE_NAME . ' - userlist')
               ->build('add_news_users', $this->data);
      }else{
        $genders    =   $this->input->post('gender');
        $food    =   $this->input->post('food');
        $dance    =   $this->input->post('dance'); 
        $Brandy    =   $this->input->post('Brandy');
        $Gin    =   $this->input->post('Gin');
        $Cachaca    =   $this->input->post('Cachaca');
        $Tequila    =   $this->input->post('Tequila');
        $Beer        =   $this->input->post('Beer');
        $Whisky       =   $this->input->post('Whisky');
        $Vodka        =   $this->input->post('Vodka');
        $Rum          =   $this->input->post('Rum');
        $Schnapps     =   $this->input->post('Schnapps');
        $Intrested    =   $this->input->post('intrested_in'); 
        if(!empty($genders)){
          if($genders == 'Male'){
          $gender = '[{"id":1,"value":"Male","isChecked":true},{"id": 2,"value": "Female","isChecked":false}]';
          }else{
            $gender = '[{"id":1,"value":"Male","isChecked":false},{"id":2,"value":"Female","isChecked":true}]';
          }
        }
        
        if(!empty($dance)){
          if($dance == 'Dance'){
          $dances = '[{"id":1,"value":"Dance","isChecked":true},{"id":2,"value":"No-Dance","isChecked":false}]';
        }else{
            $dances = '[{"id":1,"value":"Dance","isChecked":false},{"id":2,"value":"No-Dance","isChecked":true}]';
          }
        }

        if(!empty($food)){
          if($food == 'Veg'){
          $foods = '[{"id":1,"value":"Veg","isChecked":true},{"id":2,"value":"Non-Veg","isChecked":false}]';
          }else{
            $foods = '[{"id":1,"value": "Veg", "isChecked": false },{"id":2,"value":"Non-Veg","isChecked":true}]';
          }
        }

        if(!empty($Intrested)){
           if($Intrested == 'Male'){
            $Intresteds = '[{"id":1,"value":"Male","isChecked":true},{"id":2,"value":"Female","isChecked":false}]';
          }elseif($Intrested == 'Female'){
            $Intresteds = '[{"id":1,"value":"Male","isChecked":false},{"id":2,"value":"Female","isChecked":true}]';
          }else{
            $Intresteds = '[{"id":1,"value":"Male","isChecked":false},{"id":2,"value":"Female","isChecked":false}]';
          }
        }
       
          if($Brandy == 'Brandy'){
          $Brandys = '{"id":1,"value":"Brandy","isChecked":true},';
          }else{
          $Brandys = '{"id":1,"value":"Brandy","isChecked":false},';
          }
        
      
          if($Cachaca == 'Cachaca'){
          $Cachacas = '{"id":2,"value":"Cachaca","isChecked":true},';
          }else{
            $Cachacas = '{"id":2,"value":"Cachaca","isChecked":false},';
          }
        
       
          if($Gin == 'Gin'){
          $Gins = '{"id":3,"value":"Gin","isChecked":true},';
          }else{
          $Gins = '{"id":3,"value":"Gin","isChecked":false},';
          }
        
        
          if($Rum == 'Rum'){
          $Rums = '{"id":4,"value":"Rum","isChecked":true},';
          }else{
            $Rums = '{"id":4,"value":"Rum","isChecked":false},';
          }
        
       
          if($Schnapps == 'Schnapps'){
          $Schnappse = '{"id":5,"value":"Schnapps","isChecked":true},';
          }else{
            $Schnappse = '{"id":5,"value":"Schnapps","isChecked":false},';
          }
        
     
          if($Tequila == 'Tequila'){
          $Tequilas = '{"id":6,"value":"Tequila","isChecked":true},';
          }else{
            $Tequilas = '{"id":6,"value":"Tequila","isChecked":false},';
          }
        
   
          if($Vodka == 'Vodka'){
          $Vodkas = '{"id":7,"value":"Vodka","isChecked":true},';
          }else{
            $Vodkas = '{"id":7,"value":"Vodka","isChecked":false},';
          }
      
     
          if($Whisky == 'Whisky'){
          $Whiskys = '{"id":8,"value":"Whisky","isChecked":true},';
          }else{
            $Whiskys = '{"id":8,"value":"Whisky","isChecked":false},';
          }
        
        
          if($Beer == 'Beer'){
          $Beers = '{"id":9,"value":"Beer","isChecked":true}';
          }else{
            $Beers = '{"id":9,"value":"Beer","isChecked":false}';
          }
        
        $type_of_drink = "[".$Brandys.$Cachacas.$Gins.$Rums.$Schnappse.$Tequilas.$Vodkas.$Whiskys.$Beers."]";
      $data = array( 'full_name'  => $this->input->post('full_name'),
                    'apiKey'      =>md5($this->input->post('phone_number')),
                    'phone_number'=>$this->input->post('phone_number'),
                    'dob'         =>$this->input->post('dob'),
                    'password'    =>md5($this->input->post('password')),
                    'gender'      =>$gender,
                    'food'        =>$foods,
                    'dance'       =>$dances,
                    'type_of_drink'=>$type_of_drink,
                    'intrested_in'=>$Intresteds,
                    'bio'=>$this->input->post('bio'),
                    'status'=>'1'


      );
      $get_rspuns = $this->admin_model->inser_user($data);
      if($get_rspuns){
        move_uploaded_file($_FILES['userfile']['tmp_name'], 'upload_files/users/' . $get_rspuns . '.jpg');
        $this->admin_model->clear_cache();
        $this->session->set_flashdata('success', 'User  have been Added  successfully');
        redirect(ADMIN_PATH.'/admin/add_news_users', 'refresh');exit;	
      }else{
        $this->session->set_flashdata('error', 'User not Added please try again! ');
        redirect(ADMIN_PATH.'/admin/add_news_users', 'refresh');exit;
      }
        
      }
     }
   //   $this->data['user_array'] = $this->admin_model->get_all_user($user_id='0');
              $this->data['page_name'] = 'user_list';
              $this->data['page_title']= 'Users-Add';
              $this->data['redrct_url']= 'users_list';
      $this->template
                ->set_layout('admin')
                ->enable_parser(FALSE)
                ->title(SITE_NAME . ' - userlist')
                ->build('add_news_users', $this->data);
    }



    public function users_profile_edit($param1='',$param2='',$param3=''){
      if ($this->session->userdata('admin_login') != 1){
        redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
     }
     if($param1 == 'update'){
      $this->form_validation->set_rules('full_name', 'Full Name', 'required|min_length[3]');
     
      
      $this->form_validation->set_rules('dob', 'Date of birth', 'required');
      $this->form_validation->set_rules('gender', 'Gender', 'required');
      $this->form_validation->set_rules('food', 'Food', 'required');
      $this->form_validation->set_rules('dance', 'Dance', 'required');
      if(!empty($this->input->post('password'))){
        $this->form_validation->set_rules('password', 'Password', 'required||min_length[5]');
        $this->form_validation->set_rules('password_confirmation', 'Password Confirmation', 'required|matches[password]');
      } 
      $phone_numbers =$this->db->get_where('club_users',array('id'=>$param1))->row()->phone_number;
      if($this->input->post('phone_number') != $phone_numbers){
           $this->form_validation->set_rules('phone_number', 'Phone Name', 'required|is_unique[club_users.phone_number]');
      }
      if ($this->form_validation->run() == FALSE)
      {
               $this->data['id'] = $param2;
               $this->data['redrect_url'] = $param3;
               
             $this->data['page_name'] = 'user_list';
              $this->data['page_title']= 'Users-Add';
              $this->data['redrct_url']= 'users_list';
              $this->data['user_array'] = $this->admin_model->get_all_user($param2);
              $this->template
                ->set_layout('admin')
                ->enable_parser(FALSE)
                ->title(SITE_NAME . ' - userlist')
                ->build('edit_user_profile', $this->data);

      }else{
        $genders    =   $this->input->post('gender');
        $food    =   $this->input->post('food');
        $dance    =   $this->input->post('dance'); 
        $Brandy    =   $this->input->post('Brandy');
        $Gin    =   $this->input->post('Gin');
        $Cachaca    =   $this->input->post('Cachaca');
        $Tequila    =   $this->input->post('Tequila');
        $Beer        =   $this->input->post('Beer');
        $Whisky       =   $this->input->post('Whisky');
        $Vodka        =   $this->input->post('Vodka');
        $Rum          =   $this->input->post('Rum');
        $Schnapps     =   $this->input->post('Schnapps');
        $Intrested    =   $this->input->post('intrested_in'); 
        if(!empty($genders)){
          if($genders == 'Male'){
          $gender = '[{"id":1,"value":"Male","isChecked":true},{"id": 2,"value": "Female","isChecked":false}]';
          }else{
            $gender = '[{"id":1,"value":"Male","isChecked":false},{"id":2,"value":"Female","isChecked":true}]';
          }
        }
        if(!empty($dance)){
          if($dance == 'Dance'){
          $dances = '[{"id":1,"value":"Dance","isChecked":true},{"id":2,"value":"No-Dance","isChecked":false}]';
        }else{
            $dances = '[{"id":1,"value":"Dance","isChecked":false},{"id":2,"value":"No-Dance","isChecked":true}]';
          }
        }

        if(!empty($food)){
          if($food == 'Veg'){
          $foods = '[{"id":1,"value":"Veg","isChecked":true},{"id":2,"value":"Non-Veg","isChecked":false}]';
          }else{
            $foods = '[{"id":1,"value": "Veg", "isChecked": false },{"id":2,"value":"Non-Veg","isChecked":true}]';
          }
        }

        if(!empty($Intrested)){
          if($Intrested == 'Male' ||$Intrested == 'Female' ){
          $Intresteds = '[{"id":1,"value":"Male","isChecked":true },{"id":2,"value":"Female","isChecked":true}]';
          }elseif($Intrested == 'Male'){
            $Intresteds = '[{"id":1,"value":"Male","isChecked":true},{"id":2,"value":"Female","isChecked":false}]';
          }elseif($Intrested == 'Female'){
            $Intresteds = '[{"id":1,"value":"Male","isChecked":false},{"id":2,"value":"Female","isChecked":true}]';
          }else{
            $Intresteds = '[{"id":1,"value":"Male","isChecked":false},{"id":2,"value":"Female","isChecked":false}]';
          }
        }
          if($Brandy == 'Brandy'){
          $Brandys = '{"id":1,"value":"Brandy","isChecked":true},';
          }else{
          $Brandys = '{"id":1,"value":"Brandy","isChecked":false},';
          }
        
    
          if($Cachaca == 'Cachaca'){
          $Cachacas = '{"id":2,"value":"Cachaca","isChecked":true},';
          }else{
            $Cachacas = '{"id":2,"value":"Cachaca","isChecked":false},';
          }
       
       
          if($Gin == 'Gin'){
          $Gins = '{"id":3,"value":"Gin","isChecked":true},';
          }else{
          $Gins = '{"id":3,"value":"Gin","isChecked":false},';
          }
        
      
          if($Rum == 'Rum'){
          $Rums = '{"id":4,"value":"Rum","isChecked":true},';
          }else{
            $Rums = '{"id":4,"value":"Rum","isChecked":false},';
          }
        
      
          if($Schnapps == 'Schnapps'){
          $Schnappse = '{"id":5,"value":"Schnapps","isChecked":true},';
          }else{
            $Schnappse = '{"id":5,"value":"Schnapps","isChecked":false},';
          }
        
    
          if($Tequila == 'Tequila'){
          $Tequilas = '{"id":6,"value":"Tequila","isChecked":true},';
          }else{
            $Tequilas = '{"id":6,"value":"Tequila","isChecked":false},';
          }
        
  
          if($Vodka == 'Vodka'){
          $Vodkas = '{"id":7,"value":"Vodka","isChecked":true},';
          }else{
            $Vodkas = '{"id":7,"value":"Vodka","isChecked":false},';
          }
        
       
          if($Whisky == 'Whisky'){
          $Whiskys = '{"id":8,"value":"Whisky","isChecked":true},';
          }else{
            $Whiskys = '{"id":8,"value":"Whisky","isChecked":false},';
          }
        
    
          if($Beer == 'Beer'){
          $Beers = '{"id":9,"value":"Beer","isChecked":true}';
          }else{
            $Beers = '{"id":9,"value":"Beer","isChecked":false}';
          }
        
        $type_of_drink = "[".$Brandys.$Cachacas.$Gins.$Rums.$Schnappse.$Tequilas.$Vodkas.$Whiskys.$Beers."]";
          if(!empty($this->input->post('password'))){
              $data['password']         = md5($this->input->post('password'));
            }
           $data['full_name']     =  $this->input->post('full_name');
            $data['phone_number'] = $this->input->post('phone_number');
            $data['gender']       = $gender;
            $data['food']         = $food;
            $data['dob']          = $this->input->post('dob');
            $data['dance']        = $dance;
            $data['type_of_drink']= $type_of_drink;
            $data['intrested_in']= $Intresteds;
            $data['bio']         = $this->input->post('bio');
            $this->db->where('id', $param2);
            $this->db->update('club_users', $data);
            $this->session->set_flashdata('flash_message' ,"Data Update");
            redirect(ADMIN_PATH.'/admin/'.$param3, 'refresh');
      }
     }
     $this->data['user_array'] = $this->admin_model->get_all_user($param1);
     $this->data['id'] = $param1;
     $this->data['redrect_url'] = $param2;
              $this->data['page_name'] = 'user_list';
              $this->data['page_title']= 'Users-Add';
              $this->data['redrct_url']= 'users_list';
      $this->template
                ->set_layout('admin')
                ->enable_parser(FALSE)
                ->title(SITE_NAME . ' - userlist')
                ->build('edit_user_profile', $this->data);
    }


    
    public function users_list($user_id='0'){
      if ($this->session->userdata('admin_login') != 1){
        redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
     }
      $this->data['user_array'] = $this->admin_model->get_all_user($user_id='0');
   // echo "<pre>"; print_r($this->data['user_array']);die;
              $this->data['page_name'] = 'user_list';
              $this->data['page_title']= 'Users-list';
              $this->data['redrct_url']= 'users_list';
      $this->template
                ->set_layout('admin')
                ->enable_parser(FALSE)
                ->title(SITE_NAME . ' - userlist')
                ->build('user_list', $this->data);
    }
    public function active_users_list($user_id='0'){
      if ($this->session->userdata('admin_login') != 1){
        redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
     }
      $this->data['user_array'] = $this->admin_model->active_users_list($user_id='0');
              $this->data['page_name'] = 'user_list';
              $this->data['page_title']= 'Users-list';
              $this->data['redrct_url']= 'active_users_list';
      $this->template
                ->set_layout('admin')
                ->enable_parser(FALSE)
                ->title(SITE_NAME . ' - userlist')
                ->build('user_list', $this->data);
    }
    public function block_users_list($user_id='0'){
      if ($this->session->userdata('admin_login') != 1){
        redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
     }
      $this->data['user_array'] = $this->admin_model->block_users_list($user_id='0');
              $this->data['page_name'] = 'user_list';
              $this->data['page_title']= 'Users-list';
              $this->data['redrct_url']= 'block_users_list';
      $this->template
                ->set_layout('admin')
                ->enable_parser(FALSE)
                ->title(SITE_NAME . ' - userlist')
                ->build('user_list', $this->data);
    }

    

    public function users_profile($user_id='0'){
      if ($this->session->userdata('admin_login') != 1){
        redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
     }
      if(empty($user_id)){
        $this->session->set_flashdata('error','Something is missing');
        redirect(ADMIN_PATH.'/users_list', 'refresh');exit;
      }else{
          $this->data['user_array']   = $this->admin_model->get_all_user($user_id);
          $this->data['events']       = $this->admin_model->get_all_events($user_id);
          $this->data['friends']       = $this->admin_model->get_all_friends($user_id,'1');
          $this->data['blockfriends']       = $this->admin_model->get_all_friends($user_id,'2');
          $this->data['sendrequest']       = $this->admin_model->get_all_sendrequest($user_id,'send');
          $this->data['receivedfriend']       = $this->admin_model->get_all_sendrequest($user_id,'rec');
           // print_r($this->data['user_array']);die;
              $this->data['page_name']  = 'user_list';
              $this->data['page_title'] = 'Users-Profile';
              $this->data['user_id']    = $user_id;
      $this->template
                ->set_layout('admin')
                ->enable_parser(FALSE)
                ->title(SITE_NAME . ' - users_profile')
                ->build('users_profile', $this->data);
      }
    }
    public function users_friends_profiles($firend='0',$user_id='0'){
      if ($this->session->userdata('admin_login') != 1){
        redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
     }
      if(empty($firend)){
        $this->session->set_flashdata('error','Something is missing');
        redirect(ADMIN_PATH.'/users_list', 'refresh');exit;
      }else{
          $this->data['user_array']   = $this->admin_model->get_all_user($firend);
       //   $this->data['events']       = $this->admin_model->get_all_events($user_id);
         // $this->data['friends']       = $this->admin_model->get_all_friends($user_id);
          
           // print_r($this->data['user_array']);die;
              $this->data['page_name']  = 'user_list';
              $this->data['page_title'] = 'Users-Profile';
              $this->data['user_id']    = $user_id;
              $this->data['firend']    = $firend;
              $this->data['redrct_url'] = 'users_friends_profiles';
              
      $this->template
                ->set_layout('admin')
                ->enable_parser(FALSE)
                ->title(SITE_NAME . ' - users_profile')
                ->build('users_friends_profiles', $this->data);
      }
    }
    public function users_friends_request_profiles($firend='0',$user_id='0',$id='',$geter=''){
      if ($this->session->userdata('admin_login') != 1){
        redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
     }
      if(empty($firend)){
        $this->session->set_flashdata('error','Something is missing');
        redirect(ADMIN_PATH.'/users_list', 'refresh');exit;
      }else{
        if($geter =='send'){
          $this->data['user_array']   = $this->admin_model->get_all_sendrequest_profile($firend,$id,'send');
        }else{
          $this->data['user_array']   = $this->admin_model->get_all_sendrequest_profile($user_id,$id,'rec');
        }
              $this->data['geter']     = $geter;
              $this->data['page_name']  = 'user_list';
              $this->data['page_title'] = 'Users-Profile';
              $this->data['user_id']    = $user_id;
              $this->data['firend']    = $firend;
              $this->data['redrct_url'] = 'users_friends_request_profiles';
              
      $this->template
                ->set_layout('admin')
                ->enable_parser(FALSE)
                ->title(SITE_NAME . ' - users_profile')
                ->build('users_friends_request_profiles', $this->data);
      }
    }
     public function users_friend_Cenceld($param1='',$param2='',$param3=''){
      if(empty($param1)){
        $this->session->set_flashdata('error','Something is missing');
        redirect(ADMIN_PATH.'/'.$param2, 'refresh');exit;
      }else{
        $this->db->where('id',$param1);
        $this->db->delete('club_friend_request');
        $this->session->set_flashdata('success', 'Friend Request  Cenceld  successfully');
        redirect(ADMIN_PATH.'/admin/users_profile/'.$param2, 'refresh');exit;	
      }
       
     }
    public function users_friend_accept($param1='',$param2='',$param3=''){
      if(empty($param1)){
        $this->session->set_flashdata('error','Something is missing');
        redirect(ADMIN_PATH.'/'.$param2, 'refresh');exit;
      }else{
        $data   = array('user_id' => $param2,
        'friend'   => $param3,
         'status'  =>'1');
          $this->db->insert('club_my_friends',$data);

          $data1   = array('user_id' => $param3,
                  'friend'  => $param2,
                  'status'  =>'1');
          $this->db->insert('club_my_friends',$data1);
          $id = $this->db->insert_id();
          $this->db->where('id',$param1);
          $this->db->delete('club_friend_request');
        $this->session->set_flashdata('success', 'Friend Request Accept  successfully');
        redirect(ADMIN_PATH.'/admin/users_profile/'.$param2, 'refresh');exit;	
      }
    }
    
    public function users_friend_block($param1='0',$param2='',$param3=''){
      if(empty($param1)){
       $this->session->set_flashdata('error','Something is missing');
       redirect(ADMIN_PATH.'/'.$param2, 'refresh');exit;
      }else{
        $data['status']             ='3';
        $datas['status']             ='2';
        $this->db->where('user_id', $param1);
        $this->db->where('friend', $param2);
        $this->db->update('club_my_friends', $data);

        $this->db->where('user_id', $param2);
        $this->db->where('friend', $param1);
        $this->db->update('club_my_friends', $datas);
        $this->session->set_flashdata('success', 'User Block  successfully');
          redirect(ADMIN_PATH.'/admin/'.$param3.'/'.$param1.'/'.$param2, 'refresh');exit;	
      }
     }
     public function users_friend_unfriend($param1='0',$param2='',$param3=''){
      if(empty($param1)){
       $this->session->set_flashdata('error','Something is missing');
       redirect(ADMIN_PATH.'/'.$param2, 'refresh');exit;
      }else{
        $data['status']             ='3';
        $datas['status']             ='2';

        $this->db->where('user_id',$param1);
        $this->db->where('friend',$param2);
        $this->db->delete('club_my_friends');

        $this->db->where('user_id',$param2);
        $this->db->where('friend',$param1);
        $this->db->delete('club_my_friends');
        $this->session->set_flashdata('success', 'User Unfriend  successfully');
        redirect(ADMIN_PATH.'/admin/users_profile/'.$param2, 'refresh');exit;		
      }
     }
     
     public function users_friend_unblock($param1='',$param2='',$param3=''){
       if(empty($param1)){
        $this->session->set_flashdata('error','Something is missing');
        redirect(ADMIN_PATH.'/'.$param2, 'refresh');exit;
       }else{
         $data['status']='1';
         $this->db->where('user_id', $param1);
         $this->db->where('friend', $param2);
         $this->db->update('club_my_friends', $data);

         $this->db->where('user_id', $param2);
         $this->db->where('friend', $param1);
         $this->db->update('club_my_friends', $data);
         $this->session->set_flashdata('success', 'User Un Block  successfully');
         redirect(ADMIN_PATH.'/admin/'.$param3.'/'.$param1.'/'.$param2, 'refresh');exit;	
       }
      }


    public function users_profile_block($param1='0',$param2=''){
     if(empty($param1)){
      $this->session->set_flashdata('error','Something is missing');
      redirect(ADMIN_PATH.'/'.$param2, 'refresh');exit;
     }else{
       $data['status']             ='3';
       $this->db->where('id', $param1);
       $this->db->update('club_users', $data);
       $this->session->set_flashdata('success', 'User Block  successfully');
         redirect(ADMIN_PATH.'/admin/'.$param2, 'refresh');exit;	
     }
    }

    public function users_profile_delte($param1='0',$param2=''){
      if(empty($param1)){
       $this->session->set_flashdata('error','Something is missing');
       redirect(ADMIN_PATH.'/'.$param2, 'refresh');exit;
      }else{
      //Delete user  table   table
      $this ->db->where('id', $param1);
      $this->db->delete('club_users');
      //Delete  user   In    Freind  list   table
      $this ->db->where('user_id', $param1);
      $this->db->delete('club_my_friends');
      //Delete  user In Freind list table
      $this ->db->where('friend', $param1);
      $this->db->delete('club_my_friends');
    //Delete  Users  club_friend_request table
      $this ->db->where('user_id', $param1);
      $this->db->delete('club_friend_request');
        $data['status']             ='3';
        $this->db->where('id', $param1);
        $this->db->update('club_users', $data);
        $this->session->set_flashdata('success', ' User Delete successfully');
          redirect(ADMIN_PATH.'/admin/'.$param2, 'refresh');exit;	
      }
     }
    
    public function users_profile_unblock($param1='0',$param2=''){
      if(empty($param1)){
       $this->session->set_flashdata('error','Something is missing');
       redirect(ADMIN_PATH.'/'.$param2, 'refresh');exit;
      }else{
        $data['status']='1';
        $this->db->where('id', $param1);
        $this->db->update('club_users', $data);
        $this->session->set_flashdata('success', 'User Un Block  successfully');
          redirect(ADMIN_PATH.'/admin/'.$param2, 'refresh');exit;	
      }
     }
    

     public function userevents($param1='',$param2=''){
      if ($this->session->userdata('admin_login') != 1){
        redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
     }
      $this->data['all_events'] = $this->admin_model->userevents($param2);
              $this->data['page_name'] = 'user_list';
              $this->data['page_title']= 'userevents';
              $this->data['userevents']= 'userevents';
              $this->data['param1']= $param1;
              $this->data['param2']= $param2;
      $this->template
                ->set_layout('admin')
                ->enable_parser(FALSE)
                ->title(SITE_NAME . ' - events')
                ->build('userevents', $this->data);
    }
    public function get_usersevent_by_event_id($event_id,$param1,$param2){
      if ($this->session->userdata('admin_login') != 1){
        redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
     }
          $this->data['all_events'] = $this->admin_model->get_event_by_event_id($event_id);
          $this->data['page_name'] = 'user_list';
          $this->data['page_title']= 'events-list';
          $this->data['param1']= $param1;
          $this->data['param2']= $param2;
          $this->template
            ->set_layout('admin')
            ->enable_parser(FALSE)
            ->title(SITE_NAME . ' - events')
            ->build('userevent_profile', $this->data);
    }

    public function events_new($event_id='0'){
      if ($this->session->userdata('admin_login') != 1){
        redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
     }
      $this->data['all_events'] = $this->admin_model->get_all_events_new($event_id='0');
              $this->data['page_name'] = 'events';
              $this->data['page_title']= 'events-list';
              $this->data['events_list']= 'events_new';
      $this->template
                ->set_layout('admin')
                ->enable_parser(FALSE)
                ->title(SITE_NAME . ' - events')
                ->build('events', $this->data);
    }
    public function events_old($event_id='0'){
      if ($this->session->userdata('admin_login') != 1){
        redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
     }
      $this->data['all_events'] = $this->admin_model->events_old($event_id='0');
              $this->data['page_name'] = 'events';
              $this->data['page_title']= 'events-list';
              $this->data['events_list']= 'events_old';
      $this->template
                ->set_layout('admin')
                ->enable_parser(FALSE)
                ->title(SITE_NAME . ' - events')
                ->build('events', $this->data);
    }
    public function get_event_by_event_id($event_id,$events_list){
      if ($this->session->userdata('admin_login') != 1){
        redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
     }
          $this->data['all_events'] = $this->admin_model->get_event_by_event_id($event_id);
          $this->data['page_name'] = 'events';
          $this->data['page_title']= 'events-list';
          $this->data['events_list']= $events_list;
          $this->template
            ->set_layout('admin')
            ->enable_parser(FALSE)
            ->title(SITE_NAME . ' - events')
            ->build('event_profile', $this->data);
    }

    public function pairup_requests(){
      if ($this->session->userdata('admin_login') != 1){
        redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
     }
              $this->data['pairup'] = $this->admin_model->pairup_requests();
              $this->data['page_name'] = 'pairup';
              $this->data['page_title']= 'pairup-list';
              $this->data['pairup_view']= 'pairup_requests';
      $this->template
                ->set_layout('admin')
                ->enable_parser(FALSE)
                ->title(SITE_NAME . ' - pairup')
                ->build('pairup', $this->data);
    }
    public function pairup_accepted(){
      if ($this->session->userdata('admin_login') != 1){
        redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
     }
      $this->data['pairup'] = $this->admin_model->pairup_accepted();
      $this->data['page_name'] = 'pairup';
      $this->data['page_title']= 'pairup-list';
      $this->data['pairup_view']= 'pairup_accepted';
      $this->template
      ->set_layout('admin')
      ->enable_parser(FALSE)
      ->title(SITE_NAME . ' - pairup')
      ->build('pairup', $this->data);
    }
    public function month_transaction(){
      if ($this->session->userdata('admin_login') != 1){
        redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
     }
      $this->data['month_transaction'] = $this->admin_model->month_transaction();
      $this->data['page_name'] = 'transaction';
      $this->data['page_title']= 'transaction-list';
      $this->data['transaction']= 'month_transaction';
      $this->template
      ->set_layout('admin')
      ->enable_parser(FALSE)
      ->title(SITE_NAME . ' - transaction')
      ->build('transaction', $this->data);
    }
    public function year_transaction(){
      if ($this->session->userdata('admin_login') != 1){
        redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
     }
      $this->data['month_transaction'] = $this->admin_model->year_transaction();
      $this->data['page_name'] = 'transaction';
      $this->data['page_title']= 'transaction-list';
      $this->data['transaction']= 'year_transaction';
      $this->template
      ->set_layout('admin')
      ->enable_parser(FALSE)
      ->title(SITE_NAME . ' - transaction')
      ->build('transaction', $this->data);
    }
    public function month_plan($type='0',$id='0'){
      if ($this->session->userdata('admin_login') != 1){
        redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
     }
      $this->load->helper(array('form', 'url'));
      $this->load->library('form_validation');
      if($type == 'update'){
        //callback_valid_dat
        $this->form_validation->set_rules('plan_name', 'Plan Name', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('allowedUsers', 'Allowed Users', 'required');
        $this->form_validation->set_rules('allowedCalls', 'Allowed Calls', 'required');
        $this->form_validation->set_rules('price', 'price', 'required');
       
        if ($this->form_validation->run() == FALSE)
        {
          redirect(ADMIN_PATH.'/admin/edit_month_plan/'.$id, 'refresh');exit;
        }
        else
        {
           $plan_name    =   $this->input->post('plan_name');
           $description  =   $this->input->post('description');
           $allowedUsers =   $this->input->post('allowedUsers');
           $allowedCalls =   $this->input->post('allowedCalls');
           $price        =   $this->input->post('price');
           $enddate = date('Y-m-d', strtotime('+1 month', strtotime($date)));
            $data = $this->admin_model->update_month_plan($id,$plan_name,$description,$allowedUsers,$allowedCalls,$price);
            if($data){
              $this->session->set_flashdata('success', 'You have been plane update successfully');
              redirect(ADMIN_PATH.'/admin/month_plan', 'refresh');exit;	
            }else{
              $this->session->set_flashdata('error','Server error please try Again !');
              redirect(ADMIN_PATH.'/admin/edit_month_plan/'.$id, 'refresh');exit;
            }
        }

      }
      if($type == 'Block'){
        $data['status']             ='3';
        $this->db->where('id' , $id);
        $this->db->update('club_monthly_subscriptionplanmaster' , $data);
        $this->session->set_flashdata('success', 'You have been plane Block successfully');
        redirect(ADMIN_PATH.'/admin/month_plan', 'refresh');exit;	
      }
      if($type == 'Unblock'){
        $data['status']             ='1';
        $this->db->where('id' , $id);
        $this->db->update('club_monthly_subscriptionplanmaster' , $data);
        $this->session->set_flashdata('success', 'You have been plane UnBlock successfully');
        redirect(ADMIN_PATH.'/admin/month_plan', 'refresh');exit;	
      }
      $this->data['plan'] = $this->admin_model->month_plan();
      $this->data['page_name'] = 'Packages';
      $this->data['page_title']= 'plan-list';
      $this->data['transaction']= 'month_plan';
      $this->data['edit_plan']= 'edit_month_plan';
      $this->template
      ->set_layout('admin')
      ->enable_parser(FALSE)
      ->title(SITE_NAME . ' - Packages')
      ->build('Packages', $this->data);
    }
    public function year_plan($type='0',$id='0'){
      if ($this->session->userdata('admin_login') != 1){
        redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
     }
      $this->load->helper(array('form', 'url'));
      $this->load->library('form_validation');
      if($type == 'update'){
        $this->form_validation->set_rules('plan_name', 'Plan Name', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('allowedUsers', 'Allowed Users', 'required');
        $this->form_validation->set_rules('allowedCalls', 'Allowed Calls', 'required');
        $this->form_validation->set_rules('price', 'price', 'required');
       
        if ($this->form_validation->run() == FALSE)
        {
          redirect(ADMIN_PATH.'/admin/edit_year_plan/'.$id, 'refresh');exit;
        }
        else
        {
           $plan_name    =   $this->input->post('plan_name');
           $description  =   $this->input->post('description');
           $allowedUsers =   $this->input->post('allowedUsers');
           $allowedCalls =   $this->input->post('allowedCalls');
           $price        =   $this->input->post('price');
           $enddate = date('Y-m-d', strtotime('+1 year', strtotime($date)));
            $data = $this->admin_model->update_year_plan($id,$plan_name,$description,$allowedUsers,$allowedCalls,$price);
            if($data){
              $this->session->set_flashdata('success', 'Plan updated successfully');
              redirect(ADMIN_PATH.'/admin/year_plan', 'refresh');exit;	
            }else{
              $this->session->set_flashdata('error','Server error please try Again !');
              redirect(ADMIN_PATH.'/admin/year_plan/'.$id, 'refresh');exit;
            }
        }

      }
      if($type == 'Block'){
        $data['status']             ='3';
        $this->db->where('id' , $id);
        $this->db->update('club_subscriptionplanmaster' , $data);
        $this->session->set_flashdata('success', 'You have been plane Block successfully');
        redirect(ADMIN_PATH.'/admin/year_plan', 'refresh');exit;	
      }
      if($type == 'Unblock'){
        $data['status']             ='1';
        $this->db->where('id' , $id);
        $this->db->update('club_subscriptionplanmaster' , $data);
        $this->session->set_flashdata('success', 'You have been plane UnBlock successfully');
        redirect(ADMIN_PATH.'/admin/year_plan', 'refresh');exit;	
      }

      $this->data['plan'] = $this->admin_model->year_plan();
      $this->data['page_name'] = 'Packages';
      $this->data['page_title']= 'plan-list';
      $this->data['transaction']= 'year_plan';
      $this->data['edit_plan']= 'edit_year_plan';
      $this->template
      ->set_layout('admin')
      ->enable_parser(FALSE)
      ->title(SITE_NAME . ' - Packages')
      ->build('Packages', $this->data);
    }

    public function edit_month_plan($id){
      if ($this->session->userdata('admin_login') != 1){
        redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
     }
      $this->data['plan'] = $this->admin_model->edit_month_plan($id);
      $this->data['page_name'] = 'Packages';
      $this->data['page_title']= 'plan-list';
      $this->data['transaction']= 'month_plan';
      $this->data['id']= $id;
      
      $this->template
      ->set_layout('admin')
      ->enable_parser(FALSE)
      ->title(SITE_NAME . ' - Packages')
      ->build('edit_month_plan', $this->data);
    }
    public function edit_year_plan($id){
      if ($this->session->userdata('admin_login') != 1){
        redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
     }
      $this->data['plan'] = $this->admin_model->edit_year_plan($id);
      $this->data['page_name'] = 'Packages';
      $this->data['page_title']= 'plan-list';
      $this->data['transaction']= 'year_plan';
      $this->data['id']= $id;
      
      $this->template
      ->set_layout('admin')
      ->enable_parser(FALSE)
      ->title(SITE_NAME . ' - Packages')
      ->build('edit_month_plan', $this->data);
    }

    
    public function report(){
      if ($this->session->userdata('admin_login') != 1){
        redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
     }
      $this->data['report'] = $this->admin_model->report();
      $this->data['page_name'] = 'report';
      $this->data['page_title']= 'report-list';
      $this->data['transaction']= 'report';
      $this->template
      ->set_layout('admin')
      ->enable_parser(FALSE)
      ->title(SITE_NAME . ' - report')
      ->build('report', $this->data);
    }
      
    public function valid_date($date)
    { 
        $d = DateTime::createFromFormat('d-m-Y', $date);
      //  echo $d;die;
      if( $d && $d->format('d-m-Y') === $date){
        return $d && $d->format('d-m-Y') === $date;
      }else{
        $this->form_validation->set_message('valid_date', 'The %s field can not be the word "test"');
			return FALSE;
      }
        
    }

   public function record_count(){
    $notification_count = $this->db->limit(9)->get_where('club_subscriptionplanmaster_activate',array('notification_activated_status'=>'2'))->num_rows();
    $notification_counts = $this->db->get_where('club_subscriptionplanmaster_activate',array('notification_activated_status'=>'2'))->num_rows();
    $notification_count; 
    if($notification_counts >'9'){ $puls = '+';}
    $msg_count = $this->db->limit(9)->get_where('club_reports',array('msg_seen_Status'=>'2'))->num_rows();
    $msg_counts = $this->db->get_where('club_reports',array('msg_seen_Status'=>'2'))->num_rows();
    if($msg_counts >'9'){ $msg_pus = '+';}
   echo  ' <ul class="navbar-nav ml-auto ml-md-0">
    <li class="nav-item dropdown no-arrow mx-1">
      <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-bell fa-fw"></i>

        <span class="badge badge-danger">'.$notification_count.$puls.'</span>
      </a>
      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
        <a class="dropdown-item" href="#">Action</a>
        <a class="dropdown-item" href="#">Another action</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#">Something else here</a>
      </div>
    </li>
   
    <li class="nav-item dropdown no-arrow mx-1">
      <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-envelope fa-fw"></i>
        <span class="badge badge-danger">'.$msg_count.$msg_pus.'</span>
      </a>
      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="messagesDropdown">
        <a class="dropdown-item" href="#">Action</a>
        <a class="dropdown-item" href="#">Another action</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#">Something else here</a>
      </div>
    </li>
    <li class="nav-item dropdown no-arrow">
      <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-user-circle fa-fw"></i>
      </a>
      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
        <a class="dropdown-item" href="#">Settings</a>
        <a class="dropdown-item" href="#">Activity Log</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
      </div>
    </li>
  </ul>';
   }
  public function logout(){
    if ($this->session->userdata('admin_login') != 1){
      redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
   }
    $_SESSION = $this->session->userdata('admin_login');
    $this->session->unset_userdata($_SESSION);
    redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
   
 
  }
  public function settings($param1='0',$param2='0'){
    if ($this->session->userdata('admin_login') != 1){
      redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
   }
   $this->load->helper(array('form', 'url'));
   $this->load->library('form_validation');
   if($param1 =='update'){
    $this->form_validation->set_rules('username', 'username', 'required');
    if ($this->form_validation->run() == FALSE)
    {
      redirect(ADMIN_PATH.'/admin/settings/', 'refresh');exit;
    }else{
    $data['username'] = $this->input->post('username');
    if ($this->input->post('password') != '') {
     $data['password'] = md5($this->input->post('password'));
    }
    $this->db->where('admin_id', $param2);
    $this->db->update('club_admin', $data);
    $this->session->set_flashdata('success', 'Profile Update successfully');
    redirect(ADMIN_PATH.'/admin/settings', 'refresh');exit;	
   }
  }
    $this->data['settings'] = $this->admin_model->settings();
    $this->data['page_name'] = 'settings';
    $this->data['page_title']= 'settings';
    $this->data['transaction']= 'settings';
    $this->template
    ->set_layout('admin')
    ->enable_parser(FALSE)
    ->title(SITE_NAME . ' - settings')
    ->build('settings', $this->data);
  }
      public function members(){
      if ($this->session->userdata('admin_login') != 1){
        redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
     }
      $this->data['members'] = $this->admin_model->members();
      $this->data['page_name'] = 'members';
      $this->data['page_title']= 'members-list';
      $this->data['transaction']= 'members';
      $this->template
      ->set_layout('admin')
      ->enable_parser(FALSE)
      ->title(SITE_NAME . ' - members')
      ->build('members', $this->data);
    }
  public function cron_job(){
          $get_data = $this->admin_model->get_events_pairup();
      if($get_data){
            foreach($get_data as $val){
              $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
             $time = $date->format('H:i'); 
              $cruntdate = $date->format('d-m-Y'); 
              $notification_start_time= date('H:i', strtotime($val['time'].' - 30 minutes') );
              $notification_stop_time = $val['time'];
              $notification_start_date =$val['date'];
              //echo $cruntdate.' '.$notification_start_date;die;
          if($cruntdate == $notification_start_date){
           // echo $notification_start_time;die;
            if($notification_start_time < $time && $notification_stop_time > $time){
                $pairup_id=  $this->db->get_where('club_my_pairup',array('event_id'=>$val['event_id']))->row()->pairup_id;
                if(empty($pairup_id)){
                    $title = $val['full_name'];
                    $body  = 'You are not paired with anyone for the club '.$val['club_name'].' you are going on '.$notification_start_date.' at '.$notification_stop_time;
                    $datas = array('event_id'=>$val['event_id']); 
                    if($val['os'] =='1'){
                        $key = $val['gkey'];
                    }else{
                      $key   = $val['ikey'];
                    } 
                    $this->sendPushNotificationToFCMSever($key,$title,$body,$datas);
                }
              }else{
                echo "time not starto";
              }
            }else{
              echo "Time not activated";
            }
          }
      }

   }
    public function contact_us(){
      if ($this->session->userdata('admin_login') != 1){
        redirect(ADMIN_LOGIN_PATH, 'refresh');exit;
     }
      $this->data['contact_us'] = $this->admin_model->contact_us();
      $this->data['page_name'] = 'contact_us';
      $this->data['page_title']= 'contact_us';
      $this->data['transaction']= 'contact_us';
      $this->template
      ->set_layout('admin')
      ->enable_parser(FALSE)
      ->title(SITE_NAME . ' - contact_us')
      ->build('contact_us', $this->data);
    }
    
      /*** pUSH NOTIFICATION **/
    public function sendPushNotificationToFCMSever($key,$title,$body,$datas) { 
        $path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send'; 
        $fields = array(
            'registration_ids' => array($key),
            'priority' => 10,
            'notification' => array('title' => $title, 'body' => $body ,'sound'=>'Default','image'=>'Notification Image', ),
            'data' => $datas,
        );
        $headers = array(
            'Authorization:key=AAAAwtJajSA:APA91bGwBYYT8o0z646IHfs-IltGgOUZG_wPlRL9w9K_bWatUXuO9UQ69NDGsCFyXBInlDrVyfLeqQ54ZTgYNpiOXeEh8UnTJa1PJ4-ltoRr_5cnfOdE5Vi_w_4lbZpk7HLPFMoSN5Ao',
            'Content-Type:application/json'
        );  
         
        // Open connection  
        $ch = curl_init(); 
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm); 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        // Execute post   
        $result = curl_exec($ch); 
        // Close connection      
        curl_close($ch);
     print_r($result);die;
        return $result;
    }
  
   
    /****** This function is used to  add   video ****/
    private function set_upload_options()
  { 
  // upload an image options
         $config = array();
             $config['upload_path'] = './upload_files/event_video/';
            $config['allowed_types'] = 'mp4|3gp|gif|jpg|png|jpeg|pdf';
            $config['max_size']='';
            $config['max_width']='200000000';
            $config['max_height']='1000000000000';
             $config['overwrite'] = FALSE;
  return $config;
  }
}
