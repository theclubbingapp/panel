<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model {
	
	function __construct()
    {
        parent::__construct();
    }
	
	/*function classesROTS($clasid){
		$query = $this->db->query("SELECT *
			 from ");

	}*/
	function clear_cache()
	{
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
	}
	function inser_user($data){
		
		$this->db->insert('club_users',$data);
		$id =$this->db->insert_id();
		$primum = $this->db->get_where('club_subscriptionplanmaster',array('id'=>'1'))->result_array();
	
            $end = date('d-m-Y', strtotime('+1 years'));
         
		$data1 = array('user_id'       => $id,
					  'subscriptionplanmaster_id' => $primum['0']['id'],
					  'amount'          => $primum['0']['price'],
					  'currency'        => 'INR',
					  'status'          => 'captured',
					  'activate_status' => '1',
					  'transaction_id'  => 'free',
					  'transaction_type'=>'2',
					  'method'          => 'wallet',
					  'date'            =>$end,
					  );
			  $this->db->insert('club_subscriptionplanmaster_activate',$data1);
		return $id;
	}
	function get_type_name_by_id($type,$type_id='',$field='name')
	{
		return	$this->db->get_where($type,array($type.'_id'=>$type_id))->row()->$field;	
	}
	/////////get_all_user/////////////
	function get_all_user($user_id)
	{ if($user_id){
		$this->db->where('id',$user_id);
	    }
		$query	=	$this->db->order_by('id','desc')->get('club_users');
		return $query->result_array();
	}
	/////////get_active all_user/////////////
	function active_users_list($user_id)
	{ if($user_id){
		$this->db->where('id',$user_id);
	    }
		$query	=	$this->db->order_by('id','desc')->get_where('club_users',array('status'=>'1'));
		return $query->result_array();
	}
	/////////get_block all_user/////////////
	function block_users_list($user_id)
	{ if($user_id){
		$this->db->where('id',$user_id);
	    }
		$query	=	$this->db->order_by('id','desc')->get_where('club_users',array('status'=>'3'));
		return $query->result_array();
	}
	/////////get_all_events/////////////
	function get_all_events($user_id)
	{ 
		if($user_id){
		$authtoken=$this->db->get_where('club_users',array('id'=>$user_id))->row()->apiKey;
		$this->db->where('authtoken',$authtoken);
	    }
		$query	=	$this->db->order_by('event_id','desc')->get('club_event');
		return $query->result_array();
	}
	function get_all_friends($user_id,$status)
	{ 
		
		$this->db->select('club_my_friends.user_id,club_my_friends.friend,club_my_friends.friend_id,club_users.full_name,club_users.full_name');
		$this->db->from('club_my_friends');
		$this->db->join('club_users','club_users.id=club_my_friends.friend');
		if($user_id){
			$this->db->where('club_my_friends.user_id',$user_id);
			$this->db->where('club_my_friends.status',$status);
			}
		$this->db->order_by("club_my_friends.friend_id", "DESC");
		$query=$this->db->get()->result_array();
		return  $query;
	}
	function get_all_sendrequest($user_id,$parm)
	{ 
		$apiKey=$this->db->get_where('club_users',array('id'=>$user_id))->row()->apiKey;
		$this->db->select('club_friend_request.id as request_id,club_friend_request.user_id,club_friend_request.friend,club_friend_request.id,club_users.full_name,club_users.full_name');
		$this->db->from('club_friend_request');
		if($user_id){
			if($parm == 'rec'){
				$this->db->join('club_users','club_users.apiKey=club_friend_request.user_id');
				$this->db->where('club_friend_request.friend',$user_id);
			}elseif($parm == 'send'){
				$this->db->join('club_users','club_users.id=club_friend_request.friend');
				$this->db->where('club_friend_request.user_id',$apiKey);
			}
			
			//$this->db->where('club_myclub_friend_request_friends.status',$status);
			}
		$this->db->order_by("club_friend_request.id", "DESC");
		$query=$this->db->get()->result_array();
		return  $query;
	}
	function get_all_sendrequest_profile($user_id,$id,$parm)
	{ 
		$apiKey= $this->db->get_where('club_users',array('id'=>$user_id))->row()->apiKey;
		//echo $user_id,$parm;die;
		$this->db->select('club_friend_request.id as request_id,club_friend_request.user_id,club_friend_request.friend,club_friend_request.id,club_users.*');
		$this->db->from('club_friend_request');
		if($user_id){
			if($parm == 'rec'){
				$this->db->join('club_users','club_users.apiKey=club_friend_request.user_id');
				$this->db->where('club_friend_request.user_id',$apiKey);
			}elseif($parm == 'send'){
				$this->db->join('club_users','club_users.id=club_friend_request.friend');
				$this->db->where('club_friend_request.friend',$user_id);
			}
			$this->db->where('club_friend_request.id',$id);
			//$this->db->where('club_myclub_friend_request_friends.status',$status);
			}
		$this->db->order_by("club_friend_request.id", "DESC");
		$query=$this->db->get()->result_array();
		return  $query;
	}
	
	
	function userevents($param2){
		$this->db->select('club_event.*');
		$this->db->from('club_event');
		$this->db->where('club_event.authtoken',$param2);
	 $this->db->order_by("club_event.event_id", "DESC");
	 return  $query = $this->db->get()->result_array();
	}
	function get_all_events_new(){
		$this->load->helper('date');
        $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
        $date1 = $date->format('d-m-Y');
        $time = $date->format(' H:i'); 
    	$timestmp =  $date1.$time;
		$this->db->select('club_event.*');
		$this->db->from('club_event');
	   	$this->db->where('club_event.datetime >',$timestmp);
     $this->db->or_where('club_event.datetime =',$timestmp);
	 $this->db->order_by("club_event.event_id", "DESC");
	 return  $query = $this->db->get()->result_array();
		
	}
	function events_old(){
		$this->load->helper('date');
        $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
        $date1 = $date->format('d-m-Y');
        $time = $date->format(' H:i'); 
		$timestmp =  $date1.$time;
		$this->db->order_by('club_event.event_id','desc');
		$this->db->select('club_event.*');
        $this->db->from('club_event');
      //  $this->db->where('club_event.datetime =',$timestmp);
	  $this->db->where('club_event.datetime <',$timestmp);
	  $this->db->order_by("club_event.event_id", "DESC");
	  return $query = $this->db->get()->result_array();
		
	}
	function get_event_by_event_id($event_id){
		$this->db->select('club_event.*,club_users.apiKey,club_users.id,,club_users.gender,club_users.full_name,club_users.id,club_users.phone_number');
        $this->db->from('club_event');
        $this->db->join('club_users', 'club_users.apiKey = club_event.authtoken');
		$this->db->where('club_event.event_id', $event_id);
		$this->db->order_by("club_event.event_id", "DESC");
		$query = $this->db->get()->result_array();
	//	echo '<pre>';print_r($query);die;
       return $query;
	}
	function pairup_requests(){
		$this->db->select('club_pair_up_request.*,club_event.*,club_users.apiKey,club_users.id,,club_users.gender,club_users.full_name,club_users.id,club_users.phone_number');
        $this->db->from('club_pair_up_request');
		$this->db->join('club_users', 'club_users.id = club_pair_up_request.user_id');
		$this->db->join('club_event', 'club_event.event_id = club_pair_up_request.event_id');
		$this->db->order_by("club_pair_up_request.pair_up_request_id", "DESC");
		//$this->db->where('club_event.event_id', $event_id);
		$query = $this->db->get()->result_array();
		return $query;
	}
	function pairup_accepted(){
		$this->db->select('club_my_pairup.*,club_event.*,club_users.apiKey,club_users.id,club_users.gender,club_users.full_name,club_users.id,club_users.phone_number');
        $this->db->from('club_my_pairup');
		$this->db->join('club_event', 'club_my_pairup.user_id = club_my_pairup.user_id');
		$this->db->join('club_users', 'club_users.apiKey = club_event.authtoken');
		$this->db->having('club_my_pairup.event_id = club_event.event_id',false);
		$this->db->having('club_my_pairup.user_id = club_event.user_id',false);
		$this->db->order_by("club_my_pairup.pairup_id", "DESC");
		$query = $this->db->get()->result_array();
		return $query;
	}
	function month_transaction(){
		$this->db->select('club_subscriptionplanmaster_activate.*,club_users.apiKey,club_users.id,club_users.gender,club_users.full_name,club_users.id,club_users.phone_number,club_monthly_subscriptionplanmaster.name,club_monthly_subscriptionplanmaster.allowedUsers,club_monthly_subscriptionplanmaster.allowedCalls');
        $this->db->from('club_subscriptionplanmaster_activate');
		$this->db->join('club_users', 'club_users.id = club_subscriptionplanmaster_activate.user_id');
		$this->db->join('club_monthly_subscriptionplanmaster', 'club_monthly_subscriptionplanmaster.id = club_subscriptionplanmaster_activate.subscriptionplanmaster_id');
	    $this->db->where('club_subscriptionplanmaster_activate.transaction_type','1');
		$this->db->order_by("club_subscriptionplanmaster_activate.id", "DESC");
		$query = $this->db->get()->result_array();
		return $query;
	}
	function year_transaction(){
		$this->db->select('club_subscriptionplanmaster_activate.*,club_users.apiKey,club_users.id,club_users.gender,club_users.full_name,club_users.id,club_users.phone_number,club_subscriptionplanmaster.name,club_subscriptionplanmaster.allowedUsers,club_subscriptionplanmaster.allowedCalls');
        $this->db->from('club_subscriptionplanmaster_activate');
		$this->db->join('club_users', 'club_users.id = club_subscriptionplanmaster_activate.user_id');
		$this->db->join('club_subscriptionplanmaster', 'club_subscriptionplanmaster.id = club_subscriptionplanmaster_activate.subscriptionplanmaster_id');
	    $this->db->where('club_subscriptionplanmaster_activate.transaction_type','2');
		$this->db->order_by("club_subscriptionplanmaster_activate.id", "DESC");
		$query = $this->db->get()->result_array();
		return $query;
	}
	function month_plan(){
		$this->db->select('club_monthly_subscriptionplanmaster.*');
        $this->db->from('club_monthly_subscriptionplanmaster');
		$this->db->order_by("club_monthly_subscriptionplanmaster.id", "DESC");
		$query = $this->db->get()->result_array();
		return $query;
	}
	
	function year_plan(){
		$this->db->select('club_subscriptionplanmaster.*');
        $this->db->from('club_subscriptionplanmaster');
		$this->db->order_by("club_subscriptionplanmaster.id", "DESC");
		$query = $this->db->get()->result_array();
		return $query;
	}
	function edit_month_plan($id){
		$this->db->select('club_monthly_subscriptionplanmaster.*');
        $this->db->from('club_monthly_subscriptionplanmaster');
		$this->db->order_by("club_monthly_subscriptionplanmaster.id", "DESC");
		$this->db->where('id',$id);
		$query = $this->db->get()->result_array();
		return $query;
	}
	function report(){
		$this->db->select('club_reports.*,club_users.full_name');
		$this->db->from('club_reports');
		$this->db->join('club_users', 'club_users.id = club_reports.user_id');
		$this->db->order_by("club_reports.report_id", "DESC");
		$query = $this->db->get()->result_array();
		return $query;
	}
	function update_month_plan($id,$plan_name,$description,$allowedUsers,$allowedCalls,$price){
	//	echo $id.' '.$date.' '.$plan_name.' '.$description.' '.$allowedUsers.' '.$allowedCalls.' '.$price.' '.$enddate;die;
		$data   = array(
			'name'             => $plan_name,
			'description'      => $description,
			'allowedUsers'     => $allowedUsers,
			'allowedCalls'     => $allowedCalls,
			'price'            => $price,
        );

			$this->db->where('id', $id);
			$this->db->update('club_monthly_subscriptionplanmaster', $data);
			return true;
	}
	function edit_year_plan($id){
		$this->db->select('club_subscriptionplanmaster.*');
        $this->db->from('club_subscriptionplanmaster');
		$this->db->order_by("club_subscriptionplanmaster.id", "DESC");
		$this->db->where('id',$id);
		$query = $this->db->get()->result_array();
		return $query;
	}
	function settings(){
	return	$this->db->get('club_admin')->result_array();
	}
	function update_year_plan($id,$plan_name,$description,$allowedUsers,$allowedCalls,$price='0'){
		//	echo $id.' '.$date.' '.$plan_name.' '.$description.' '.$allowedUsers.' '.$allowedCalls.' '.$price.' '.$enddate;die;
			$data   = array(
				'name'             => $plan_name,
				'description'      => $description,
				'allowedUsers'     => $allowedUsers,
				'allowedCalls'     => $allowedCalls,
				'price'            => $price,
			);
	
				$this->db->where('id', $id);
				$this->db->update('club_subscriptionplanmaster', $data);
				return true;
		}
	function get_events_pairup(){
		$this->load->helper('date');
        $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
        $date1 = $date->format('d-m-Y');
        $time = $date->format(' H:i'); 
		$timestmp =  $date1.$time;
		$this->db->select('club_event.event_id,club_event.time,club_event.date,club_event.club_place_id,club_event.club_name,club_users.full_name,club_users.gkey,club_users.ikey,club_users.os');
		$this->db->from('club_event');
		$this->db->join('club_users', 'club_users.apiKey = club_event.authtoken');
		//$this->db->join('club_my_pairup', 'club_my_pairup.event_id = club_event.event_id');
	   	$this->db->where('club_event.datetime >',$timestmp);
     $this->db->or_where('club_event.datetime =',$timestmp);
	 $this->db->order_by("club_event.event_id", "DESC");
	 return  $query = $this->db->get()->result_array();	
	}
	public    function get_image_url($type = '' , $id = ''){
        if(file_exists('upload_files/'.$type.'/'.$id.'.jpg'))
            $image_url  =   base_url().'upload_files/'.$type.'/'.$id.'.jpg';
        else
            $image_url  =   base_url().'upload_files/user.png';
        return $image_url;

    }
	

	
    public function dash_daily_transaction(){ 

		$query =$this->db->query("select DATE(add_on) AS date, COUNT(id) as total_number, SUM(amount) as total_amount  from club_subscriptionplanmaster_activate  where status ='captured' AND  `add_on` >= DATE_SUB(CURDATE(), INTERVAL 1 DAY)");
        if($query->num_rows() > 0){
			//print_r($query->result_array());die;
			return $query->result_array();
		}
        return false;  

	}


    public function dash_monthly_transaction(){
	    	$this->db->select("YEAR(add_on) AS year, MONTH(add_on) AS month, DATE(add_on) AS date, COUNT(id) as total_number, SUM(amount) as total_amount ");
			$this->db->group_by('year, month');
			$this->db->where(array('status'=>'captured')); 
           $query = $this->db->get("club_subscriptionplanmaster_activate");
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return false;  
	}

    public function dash_weekly_transaction(){    
		$query =$this->db->query("select DATE(add_on) AS date, COUNT(id) as total_number, SUM(amount) as total_amount  from club_subscriptionplanmaster_activate  where status ='captured' AND  `add_on` >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)");     
		if($query->num_rows() > 0){
            return $query->result_array();
        }
        return false;  

	}
		function contact_us(){
		$this->db->select('club_contact_us.*,club_users.full_name');
		$this->db->from('club_contact_us');
		$this->db->join('club_users', 'club_users.id = club_contact_us.user_id','left');
		$this->db->order_by("club_contact_us.id", "DESC");
		$query = $this->db->get()->result_array();
		return $query;
	}
		function members(){
		$query=$this->db->distinct()->select('member_phone_number,member_name,member_age,m_add_on')->where('member_states = 1')->get('club_user_member');
		return $query->result();
	}

}//end model 

