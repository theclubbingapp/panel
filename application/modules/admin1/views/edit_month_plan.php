<div class="container-fluid" aligen="center">
<!-- Breadcrumbs-->
<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="<?php echo site_url('admin/'.$transaction);?>"><?=$transaction?></a>
  </li>
  <li class="breadcrumb-item active">Edit</li>
</ol>
<?php echo validation_errors(); ?>
<?php echo form_open(base_url().'admin/admin/'.$transaction.'/update/'.$id , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>
                           
                                    <div class="form-group ">
                                        <label for="plan_name" class="control-label col-lg-3">Plan Name</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="plan_name" name="plan_name" type="text"  value="<?php echo set_value('plan_name',$plan["0"]["name"]); ?>">
                                        </div>
                                        <?php echo form_error('plan_name');?>
                                    </div>
                                    <div class="form-group ">
                                        <label for="description" class="control-label col-lg-3">Description</label>
                                        <div class="col-lg-6">
                                        <textarea  id="description" name="description"  rows="4" cols="70"><?php echo set_value('description',$plan["0"]["description"]); ?></textarea>
                                          
                                        </div>
                                        <?php echo form_error('description'); ?>
                                    </div>

                                    <div class="form-group ">
                                        <label for="allowedUsers" class="control-label col-lg-3">Allowed Users</label>
                                        <div class="col-lg-6">
                                            <select class="form-control select2" name="allowedUsers" id="allowedUsers">
                                                <option value="">Please Select Users</option>
                                                <option value="<?php echo 'Unlimited';?>"<?php if('Unlimited' == $plan["0"]["allowedUsers"])echo 'selected';?>><?php echo 'Unlimited';?></option>
                                                <?php   for ($x = 0; $x <= 100; $x++) { ?>
                                                            <option value="<?php echo $x;?>"<?php if($x == $plan["0"]["allowedUsers"])echo 'selected';?>><?php echo $x;?></option>
                                                            <?php } ?>
                                                        </select>
                                          </div>
                                        <?php echo form_error('allowedUsers'); ?>
                                    </div>
                                    
                                    <div class="form-group ">
                                        <label for="allowedUsers" class="control-label col-lg-3">Allowed Calls</label>
                                        <div class="col-lg-6">
                                            <select class="form-control select2" name="allowedCalls" id="allowedCalls">
                                                <option value="">Please Select Calls</option>
                                                <option value="<?php echo 'Unlimited';?>"<?php if('Unlimited' == $plan["0"]["allowedCalls"])echo 'selected';?>><?php echo 'Unlimited';?></option>
                                                <?php   for ($x =   0; $x <= 100; $x++) { ?>
                                                            <option value="<?php echo $x;?>"<?php if($x == $plan["0"]["allowedCalls"])echo 'selected';?>><?php echo $x;?></option>
                                                            <?php } ?>
                                                        </select>
                                          </div>
                                        <?php echo form_error('allowedUsers'); ?>
                                    </div>

                                    <div class="form-group ">
                                        <label for="price" class="control-label col-lg-3">Price</label>
                                        <div class="col-lg-6">
                                            <input class="form-control " id="price" name="price" type="text" value="<?php echo set_value('price',$plan["0"]["price"]); ?>">
                                        </div>
                                        <?php echo form_error('price'); ?>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-offset-3 col-lg-6">
                                            <button class="btn btn-primary" type="submit">Save</button>
                                        </div>
                                    </div>
                                    <?php echo form_close();?>
                            </div>
                      
                            <script>
                              function myFunction() {
                                var checkBox = document.getElementById("myCheck");
                                var text = document.getElementById("text");
                                if (checkBox.checked == true){
                                  text.style.display = "block";
                                } else {
                                  text.style.display = "none";
                                }
                              }
                     </script>