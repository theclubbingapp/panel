<div class="container-fluid">

<!-- Breadcrumbs-->
<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="<?php echo site_url(ADMIN_PATH."/".$events_list);?>">Events List</a>
  </li>
  <li class="breadcrumb-item active">Events Views </li>
</ol>

<!-- Page Content -->
   <!-- DataTables Example -->
   <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Event</div>
          <div class="card-body center">
          <?php if($all_events){
            foreach($all_events as $val){?>
              <div style="width: 50%; float:left">
                Club Name : <?=$val['club_name'];?>
              </div>

              <div style="width: 50%; float:right">
                  User Nmae : <?=$val['full_name'];?>
              </div>
              <div style="width: 50%; float:left">
              Drinks 	: <?php $data = json_decode($val['offering_a_drink'], TRUE);
                      foreach($data as $offering_a_drink){
                        if($offering_a_drink['isChecked'] == true){
                         echo  $offering_a_drink['value'].',';
                        }else { echo "No Dance";}
                      }
                       ?>
              </div>

              <div style="width: 50%; float:right">
              Group type : <?=$val['group_type'];?>
              </div>
              <div style="width: 50%; float:left">
               Event Type: <?=$val['type'];?>
              </div>


              <div style="width: 50%; float:right">
               Club Entry  Date : <?=$val['date'];?>
              </div>
              <div style="width: 50%; float:left">
               Club Entry  Time : <?=$val['time'];?>
              </div>

              <div style="width: 50%; float:right">
              Add On : <?=$val['add_on'];?>
              </div>
</div>
                 <!-- DataTables Example -->
   
            <div class="table-responsive">
              <table class="table table-bordered" id="" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  <th>S.no</th>
                   <th>Image</th>
                    <th>Member Name</th>
                    <th>Phone Number</th>
                    <th>Gender</th>
                    <th>Age</th>
                  <th>Add on</th>
                  </tr>
                </thead>
               <!-- <tfoot>
                  <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Office</th>
                    <th>Age</th>
                    <th>Start date</th>
                    <th>Salary</th>
                  </tr>
                </tfoot>-->
                <tbody>
                <?php $user_member = $this->db->get_where('club_user_member',array('event_id' =>$val['event_id'] ,'user_id' =>$val['id'] ,'member_states'=>'1'))->result_array();?>
                    <?php if($user_member){
                      $i ='1';
                        foreach($user_member as $value){ 
                          $userid = $this->db->get_where('club_users',array('phone_number'=>$value['member_phone_number']))->row()->id;
                           ?>
                        <tr>
                      <td><?=$i++;?></td>
                      <td> <img src="<?php echo $this->admin_model->get_image_url('users',$userid);?>" class="img-circle" width="30" /></td>
                      <td><?=$value['member_name']?></td>
                        <td><?=$value['member_phone_number']?></td>
                        <td><?=$value['member_sex']?></td></td>
                        <td><?=$value['member_age']?></td>
                        <td><?=$value['m_add_on']?></td>
                    </tr>

                    <?php  }
                   }else{?>
                    <tr>
                    <td colspan ='5'>Data Not Found</td>
                    
                  </tr>

                  <?php  }?>
                 
                  
                </tbody>
              </table>
            </div>
         
          <?php }}else{
            echo "Data Not Found";
            }?>
      </div>


</div>