<div class="container-fluid">
<!-- Breadcrumbs-->
<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="<?php echo site_url(ADMIN_PATH);?>">Dashboard</a>
  </li>
  <li class="breadcrumb-item active"><?=$pairup_view;?></li>
</ol>
        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Pairup  Tables</div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  <th>S.no</th>
                    <th>Club Name</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Request By</th>
                    <?php if($pairup_view =='pairup_accepted'){?>   <th>Accepted by</th><?php }else{?>  <th>Request To</th> <?php }?>
                  
                    <th>Group Type</th>
                  <!--  <th>Action</th>-->
                  </tr>
                </thead>
               <!-- <tfoot>
                  <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Office</th>
                    <th>Age</th>
                    <th>Start date</th>
                    <th>Salary</th>
                  </tr>
                </tfoot>-->
                <tbody>
                    <?php if($pairup){
                      $i ='1';
                        foreach($pairup as $val){  ?>
                        <tr>
                      <td><?=$i++;?></td>
                      <td>  <?=$val['club_name']?></td>
                        <td><?=$val['date']?></td>
                        <td><?=$val['time']?></td>
                        <td><?php echo $this->db->get_where('club_users',array('apiKey'=>$val['authtoken']))->row()->full_name;?></td>
                        <td><?php echo $this->db->get_where('club_users',array('id'=>$val['friend_id']))->row()->full_name;?></td>
                        <td><?=$val['group_type']?></td>
                       <!-- <td>
                        <a href="<?php echo site_url(ADMIN_PATH."/admin/get_event_by_event_id/".$val['event_id'].'/'.$events_list);?>" title="View Profile">
                        <i class="fas fa-address-card"></i></a>
                     
                        </td>-->

                        
                    </tr>

                    <?php  }
                   }else{?>
                    <tr>
                    <td colspan ='5'>Data Not Found</td>
                    
                  </tr>

                  <?php  }?>
                 
                  
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>

      </div>
      <!-- /.container-fluid -->