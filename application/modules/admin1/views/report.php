<div class="container-fluid">
<!-- Breadcrumbs-->
<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="<?php echo site_url(ADMIN_PATH);?>">Dashboard</a>
  </li>
  <li class="breadcrumb-item active">Report lists</li>
</ol>
        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Report  Tables</div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  <th>S.no</th>
                    <th>Sender Name</th>
                    <th>Send To Whom</th>
                    <th>Title</th>
                    <th>Message</th>
                    <th>Add On </th>
                  </tr>
                </thead>
               <!-- <tfoot>
                  <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Office</th>
                    <th>Age</th>
                    <th>Start date</th>
                    <th>Salary</th>
                  </tr>
                </tfoot>-->
                <tbody>
                    <?php if($report){
                      $i ='1';
                        foreach($report as $val){  ?>
                        <tr>
                      <td><?=$i++;?></td>
                      <td><?=$val['full_name']?></td>
                      <td><?php echo $this->db->get_where('club_users',array('id'=>$val['friend_id']))->row()->full_name;?></td>
                      <td><?=$val['title']?></td>
                      <td><?=$val['msg']?></td>
                      <td><?=$val['add_on']?></td>
                   
                    

                        
                    </tr>

                    <?php  }
                   }else{?>
                    <tr>
                    <td colspan ='5'>Data Not Found</td>
                    
                  </tr>

                  <?php  }?>
                 
                  
                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>
      <!-- /.container-fluid -->