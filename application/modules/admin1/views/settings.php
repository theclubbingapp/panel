<div class="container-fluid" aligen="center">
<!-- Breadcrumbs-->
<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="<?php echo site_url('admin/admin/Settings');?>">Settings</a>
  </li>
  <li class="breadcrumb-item active">Edit</li>
</ol>

<?php if ($this->session->flashdata('success')) { ?>
			<h3>
				  
			<?php $flash_Message =$this->session->flashdata('success');
			echo "<div style='color:green;'>$flash_Message<div>";
			 ?>
			</h3>
			<?php } ?>
			<?php if ($this->session->flashdata('error')) { ?>
			<h3>
			<?php $flash_Message =$this->session->flashdata('error');
			echo "<div style='color:red;'>$flash_Message<div>"; ?>
			</h3>
			<?php } ?>
<?php echo validation_errors(); ?>
<?php echo form_open(base_url().'admin/admin/settings/update/'.$settings["0"]["admin_id"] , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>
                           
                                    <div class="form-group ">
                                        <label for="username" class="control-label col-lg-3">Username</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="plan_name" name="username" type="text"  value="<?php echo set_value('username',$settings["0"]["username"]); ?>">
                                        </div>
                                        <?php echo form_error('username');?>
                                    </div>
                                    <div class="form-group ">
                                        <label for="password" class="control-label col-lg-3">Password</label>
                                        <div class="col-lg-6">
                                        <input type="settings" class=" form-control" id="password" name="password"  rows="4" cols="70"><?php echo set_value('password'); ?>
                                        </div>
                                        <?php echo form_error('password'); ?>
                                    </div>
                                    <div class="form-group ">
                                        <label for="con_password" class="control-label col-lg-3">confirm Password</label>
                                        <div class="col-lg-6">
                                        <input type="con_password" class=" form-control"  id="con_password" name="con_password"  rows="4" cols="70"><?php echo set_value('con_password'); ?>
                                          
                                        </div>
                                        <?php echo form_error('con_password'); ?>
                                    </div>

                                 
                                    <div class="form-group">
                                        <div class="col-lg-offset-3 col-lg-6">
                                            <button class="btn btn-primary" type="submit">Save</button>
                                        </div>
                                    </div>
                                    <?php echo form_close();?>
                            </div>
                      
                            <script>
                              function myFunction() {
                                var checkBox = document.getElementById("myCheck");
                                var text = document.getElementById("text");
                                if (checkBox.checked == true){
                                  text.style.display = "block";
                                } else {
                                  text.style.display = "none";
                                }
                              }
                     </script>