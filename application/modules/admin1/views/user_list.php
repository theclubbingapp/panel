<div class="container-fluid">
<!-- Breadcrumbs-->
<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="<?php echo site_url(ADMIN_PATH);?>">Deshboard</a>
  </li>
  <li class="breadcrumb-item active">User List</li>
</ol>

<?php if ($this->session->flashdata('success')) { ?>
			<h3>
				  
			<?php $flash_Message =$this->session->flashdata('success');
			echo "<div style='color:green;'>$flash_Message<div>";
			 ?>
			</h3>
			<?php } ?>
			<?php if ($this->session->flashdata('error')) { ?>
			<h3>
			<?php $flash_Message =$this->session->flashdata('error');
			echo "<div style='color:red;'>$flash_Message<div>"; ?>
			</h3>
			<?php } ?>
        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            User  Tables</div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  <th>S.no</th>
                   <th>Image</th>
                    <th>Name</th>
                    <th>Phone Number</th>
                    <th>Gender</th>
                    <th>Age</th>
                    <th>Total Events</th>
                    <th>Start date</th>
                    <th>last login</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
               <!-- <tfoot>
                  <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Office</th>
                    <th>Age</th>
                    <th>Start date</th>
                    <th>Salary</th>
                  </tr>
                </tfoot>-->
                <tbody>
                    <?php if($user_array){
                      $i ='1';
                        foreach($user_array as $val){  ?>
                        <tr>
                      <td><?=$i++;?></td>
                      <td> <img src="<?php echo $this->admin_model->get_image_url('users',$val['id']);?>" class="img-circle" width="30" /></td>
                      <td><?=$val['full_name']?></td>
                        <td><?=$val['phone_number']?></td>
                        <td><?php  
                      $data = json_decode($val['gender'], TRUE);
                      foreach($data as $gender){
                        if($gender['isChecked'] == '1')
                         echo  $gender['value'];

                      }
                           
                           ?></td>
                        <td><?php $today = date("d-m-Y");
                            $diff = date_diff(date_create($val['dob']), date_create($today));
                           echo $diff->format('%y');?></td>
                           <td><?php $count = $this->db->get_where('club_event',array('authtoken'=>$val['apiKey']))->num_rows();
                                        if($count){?><a href="<?php echo site_url(ADMIN_PATH."/admin/userevents/".$val['id'].'/'.$val['apiKey']);?>" title="View Events"><?php echo $count.'</a>';}else{echo 'N/A';}?>
                                        
                                        </td>
                        <td><?=$val['add_on']?></td>
                        <td><?php if($val['last_login']){
                           echo $val['last_login'];
                        }else{
                        echo 'N/A';}?></td>
                        <td>
                        <?php  if($val['status'] == '1'){?>
                            <span class="badge badge-success"><?php echo ('Active');?></span> 
                     <?php  } elseif($val['status'] == '3'){?>
                      <span class="badge badge-danger"><?php echo ('Blocked');?></span> 
                     <?php }?></td>
                        </td>
                        <td>
                        <a href="<?php echo site_url(ADMIN_PATH."/admin/users_profile/".$val['id']);?>" title="View Profile">
                        <i class="fas fa-address-card"></i></a>
                      <!-- <a href="<?php echo site_url(ADMIN_PATH."/admin/users_profile_edit/".$val['id'].'/'.$redrct_url);?>" title="Edit Profile">
                        <i class="fas fa-user-edit"></i></a>-->
                        <?php if($val['status'] =='3'){?>
                          <a href="<?php echo site_url(ADMIN_PATH."/admin/users_profile_unblock/".$val['id'].'/'.$redrct_url);?>" title="Un Block Profile" onclick="return confirm('are you sure you want to Un Block  this user');">
                        <i class="fas fa-ban"></i></a>
                        <?php }else{ ?>
                          <a href="<?php echo site_url(ADMIN_PATH."/admin/users_profile_block/".$val['id'].'/'.$redrct_url);?>" title="Block Profile" onclick="return confirm('are you sure you want to Block this user');">
                        <i class="fas fa-ban"></i></a>
                        <?php }?>
                        <a href="<?php echo site_url(ADMIN_PATH."/admin/users_profile_delte/".$val['id'].'/'.$redrct_url);?>" title="Block Profile" 
 onclick="return confirm('are you sure you want to delete this user');">
                        <i class="fa fa-trash" style="color:red"></i></a>
                    </tr>

                    <?php  }
                   }else{?>
                    <tr>
                    <td colspan ='5'>Data Not Found</td>
                    
                  </tr>

                  <?php  }?>
                 
                  
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>

      </div>
      <!-- /.container-fluid -->