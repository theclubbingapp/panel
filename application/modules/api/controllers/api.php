<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller
{
    private static $API_SERVER_KEY = 'AAAAyEcnNUE:APA91bFF4oteKu4CCvKcnHXJRB2KXXhjdrlBDurvNoelj2Ao2yM_U_vZOma3Y-jxkoJO1ShOXxAdul_gkS51Ic_jBIOjG4ynnNpTYcOVcO80Ugj-vkPgolLD-4g_HaCCiOr0eYcfdvow';
    private static $is_background = "TRUE";
    function __construct() {

        parent::__construct();
        #Load model
        $this->load->model('rest_api');
        $this->load->library('pagination');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->helper('date');
    }

    public function index(){
      $action = $this->input->get('action');
        $authtoken = $this->input->post('authtoken');
       $status = $this->db->get_where('club_users',array('apiKey'=>$authtoken))->row()->status;
       if($status =='3'){
                $arrReturn = array('flag' => 0, 'msg' => '400','error'=>400);
                echo json_encode($arrReturn); exit;
       }
    switch($action) {
      /*All user/admin login  */
    case 'login':
        $this->userlogin();
    break;
    case 'user_signup':
        $this->user_signup();
    break;
    case 'verification':
        $this->verification();
    break;

    case 'forgotpassword':
        $this->forgotpassword();
    break;
    case 'get_user_profile':
      $this->get_user_profile();
    break;
    case 'update_profile_info':
      $this->update_profile_info();
    break;
    case 'resnd_otp':
      $this->resnd_otp();
    break;
    case 'password_change':
      $this->password_change();
    break;
    case 'events':
      $this->register_event();
      break;
    case 'event_count':
      $this->event_count_by_place_id();
      break;
    case 'event_users':
      $this->event_users_by_place_id();
      break;
    case 'view_user_profile':
      $this->view_user_profile();
      break;
    case 'send_otp_message_member':
      $this->send_otp_message_member();
      break;

    case 'member_verification':
      $this->member_verification();
      break;
    case 'delte_member':
          $this->delte_member_by_Event_id();
      break;
      case 'getall_event_by_user_id':
      $this->getall_event_by_user_id();
        # code...
        break;
        #my  All sed  Friend  Request 
    case 'send_firend_request_view':
      $this->send_firend_request_view();
      break;
      # Me  Send  Friequest  my new  Friend 
    case 'send_friend_request':
      $this->send_friend_request();
        # code...
        break;
        #Recived  my friend Request  view 
    case 'received_firend_request_view':
      $this->received_firend_request_view();
      break;
      case 'reject_friend_request':
        $this->reject_friend_request();
        break;
      case 'my_all_friend':
          $this->getallfiends();
        break;
    case 'accept_friend_request':
        $this->accept_friend_request();
      # code...
      break;
    case 'block_my_friend':
      $this->block_my_friend();
      # code...
      break;
      case 'list_block_friend':
      $this->list_block_friend();
        # code...$th
        break;
      case 'un_block_my_friend':
      $this->un_block_my_friend();
        # code...
        break;
        /*pair Up Request  View*/
        case 'send_pairUp_request_view':
        $this->send_pairUp_request_view();
          # code...
          break;
        case 'send_pairUp_request':
        $this->send_pairUp_request();
          # code...
          break;
        case 'received_pairUp_request_view':
        $this->received_pairUp_request_view();
          # code...
          break;
          case 'reject_pairUp_request':
            $this->reject_pairUp_request();
            break;
          case 'accept_pair_up_request':
          $this->accept_pair_up_request();
            # code...
            break;
        case 'getall_pairUpfiends':
          $this->getall_pairUpfiends();
          # code...
          break;
        case 'delete_my_friend':
        $this->delete_my_friend();
          # code...
          break;
        case 'delete_my_pairUp_friend':
          $this->delete_my_pairUp_friend();
          break;

        case 'send_report':
          $this->send_report();
          # code...
          break;
        case 'cancel_friend_request':
        $this->cancel_friend_request();
          # code...
          break;

        case 'cancel_pairUp_request':
        $this->cancel_pairUp_request();
          # code...
          break;
        case 'getSubscriptionPlanlist':
            $this->getSubscriptionPlanlist_post();
        break;
        case 'getSubscriptionPlanlist_monthly':
        $this->getSubscriptionPlanlist_monthly();
      break;
      
        case 'getSubscriptionPlanlist_activates':
        $this->getSubscriptionPlanlist_activates();
            break;
      case 'premiun_user_status':
          $this->premiun_user_status();
      break;
      case 'user_radios_sating':
          $this->user_radios_sating();
        break;
      case 'distance':
        $this->sent_pushnotification();
        break;
      
    /*   Use code  get sendPushNotificationToFCMSever only*/
    case 'sendPushNotificationToFCMSever':
        $this->sendPushNotificationToFCMSever();
    break;
    case'Razorpay_check_status':
      $this->Razorpay_check_status();
    break;
    case 'club_premium_user_use_insert':
      $this->club_premium_user_use_insert();
    break;
    case 'premium_user_call_insert':
    $this->club_premium_user_call_insert();
      break;
    case 'premium_user_call_check':
        $this->premium_user_call_check();
    break;
      
    case 'user_update_location':
      $this->user_update_location();
      break;
    case 'check_pairup':
    $this->check_pairup();
      break;
    case 'received_broadcast_view':
      $this->received_broadcast_view();
      break;
    case 'user_radios_sating_view':
      $this->user_radios_sating_view();
      # code...
      break;
      case 'contact_us':
      $this->contact_us();
      # code...
      break;
      case 'delete_my_club_registers':
      $this->delete_my_club_registers();
      # code...
      break;
      case 'reject_drink_request':
          $this->reject_drink_request();
        break;
      case 'accept_drink_request':
        $this->accept_drink_request();
      break;
      case 'cancel_drink_request':
      $this->cancel_drink_request();
      break;
      case 'check_request_drink':
      $this->check_request_drink();
      break;
      case 'send_drinkrequest':
      $this->send_drinkrequest();
      break;
    
    default:
          echo json_encode('Something is missing');
          break;
      }
    }
    /*
    *
    *@ This  function  is  used  User login  in app
    */
    private function userlogin(){
        #do login
        $phone_number = $this->input->post('phone_number');
        $password     = $this->input->post('password');
        $imei         = $this->input->post('imei');
        $devicetoken  = $this->input->post('devicetoken');
        $ostype       = $this->input->post('ostype');
      if($phone_number==""|| $devicetoken =="" || $ostype==""|| $imei=="" || $password ==""){
                $arrReturn = array('flag' => 0, 'msg' => 'Phone Number Devicetoken password and Ostype field is required','error'=>400);
                echo json_encode($arrReturn); exit;
          }
            $apiKey = $this->rest_api->dologin();
                if ($apiKey['status'] =='1'){
                    $arrResponse = array('flag' => 1,'authtoken' => $apiKey['apiKey'],'username'=>$apiKey['username'],'user_id'=>$apiKey['userid'],'image'=>$apiKey['image'],'phone_number'=>$apiKey['phone_number'],'msg'=>'you have  login  successfully ','success'=>200);
                }elseif($apiKey['status'] =='3'){
                    $arrResponse = array('flag' => 0, 'authtoken' => '','msg' => 'your account has been blocked please contact admin','error'=>400);
                }
                else{
                    $arrResponse = array('flag' => 0, 'authtoken' => '','msg' => 'Invalid Phone Number or password ','error'=>400);
                }
        echo json_encode($arrResponse); exit;
    }
    /*This  function is used to  register new  user  */
    private function user_signup(){
        $phone_number      = $this->input->post('phone_number');
        $dob               = $this->input->post('dob');
        $full_name         = $this->input->post('full_name');
        $password          = $this->input->post('password');
        $gender            = $this->input->post('gender');
        $food              = $this->input->post('food');
        $dance             = $this->input->post('dance');
        $type_of_drink     = $this->input->post('type_of_drink');
        $intrested_in      = $this->input->post('intrested_in');
        $bio               = $this->input->post('bio');
        $apiKey            = uniqid().time();
        if($phone_number==""|| $full_name =="" || $password=="" || $gender==""|| $food==""){
                $arrReturn = array('flag' => 0, 'msg' => 'Phone Number full_name and password field is required','error'=>400);
                echo json_encode($arrReturn); exit;
        }else{
          $phone_numbers =  $this->db->get_where('club_users',array('phone_number'=>$phone_number))->row()->phone_number;
          if($phone_numbers == $phone_number){
            $arrResponse = array('flag' => 0,'msg'=>'Phone Number already registered please Enter Another phone number','error'=>400);
              echo json_encode($arrResponse); exit;
          }else{
          $id =$this->rest_api->user_signup($phone_number,$dob,$full_name,$password,$gender,$food,$dance,$type_of_drink,$intrested_in,$bio,$apiKey);
          if(!empty($id)){
                  $message_thread_code = mt_rand(100000, 999999);
                  $this->rest_api->send_otp_message($message_thread_code,$apiKey);
                  $masg = "Hi ".$full_name. " thank you for register with us your OTP Number is ".$message_thread_code;
                  $this->send_sms($phone_number,$masg,$message_thread_code);
                $arrResponse = array('flag' => 1,'authtoken' => $apiKey,'msg'=>'you have  Register  successfully ','success'=>200);
          }else{
                $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
                
          }
          echo json_encode($arrResponse); exit;
        }
      }
    }
    /*This function is   verified  otp   in our app*/
    private function verification(){
      $code      = $this->input->post('code');
      $authtoken = $this->input->post('authtoken');
      if($code == ""){
                $arrReturn = array('flag' => 0, 'msg' => 'Authtoken AND Code  field is required','error'=>400);
                echo json_encode($arrReturn); exit;
        }else{
          $getcode = $this->db->get_where('club_verification',array('code'=>$code))->row()->code;

          if($getcode == $code){
              $code = $this->rest_api->verification($code,$authtoken);
              $full_name =  $this->db->get_where('club_users',array('apiKey'=>$authtoken))->row()->full_name;
              $arrResponse = array('flag' => 1,'authtoken' => $authtoken,'username'=>$full_name,'user_id'=>$code,'msg'=>'OTP verified successfully','success'=>200);
            }else{
              $arrResponse = array('flag' => 0, 'msg' => 'OTP does not exist','error'=>400);
            }
              echo json_encode($arrResponse); exit; 
        }
    }
    /*
    THIS  FUNCTION IS USED TO   RESEND  OTP  REGISTER  PHONE
    */ 
    private function resnd_otp(){
      $authtoken = $this->input->post('authtoken');
      if($authtoken == ""|| $authtoken == 'null' ){
                $arrReturn = array('flag' => 0, 'msg' => 'Authtoken   field is required','error'=>400);
                echo json_encode($arrReturn); exit;
        }else{
          $getcode = $this->db->get_where('club_verification',array('authtoken'=>$authtoken))->row()->code;
          if($getcode){
            $userid = $this->getuserid($authtoken);
            $phone_number =  $this->db->get_where('club_tem_register',array('apiKey'=>$authtoken))->row()->phone_number;
            $masg = " Your OTP Number is ".$getcode;
            $this->send_sms($phone_number,$masg,$getcode);
            $arrResponse = array('flag' => 1,'authtoken' => $authtoken,'msg'=>'OTP has been sent to your registered mobile number','success'=>200);
            }else{
              $arrResponse = array('flag' => 0, 'msg' => 'Sever error  please  try again','error'=>400);
            }
              echo json_encode($arrResponse); exit; 
        }
    } 

    /*
        Password  Chnge   funcation 
    */
  private function password_change(){
          $authtoken       = $this->input->post('authtoken');
          $password        =$this->input->post('password', true);
          $newpassword     = $this->input->post('newpassword', true);
          $compassword     = $this->input->post('compassword', true);
            $userid       = $this->getuserid($authtoken);
          if($authtoken=="" || $authtoken == 'null' || $password=""||$newpassword=="" ||$compassword=="" )
              {
              $arrReturn = array('flag' => 0, 'msg' => 'Authtoken Password New Password  And Confirm password  field is required','error'=>400);
              echo json_encode($arrReturn); exit;
                }
            if($compassword != $newpassword){
                $arrReturn = array('flag' => 0, 'msg' => 'Confirm password does not match check Confirm password ','error'=>400);
                  echo json_encode($arrReturn); exit;
                  }
            else {
                $query = $this->db->get_where('club_users', array('id' => $userid));
                  foreach ($query->result() as $row){
                          $pass=$row->password;
                  }
                $password       =$this->input->post('password', true);
              if($pass==md5($password)){
                  $data = array('password' => md5($newpassword));
                  $this->db->where('id',$userid);
                  $this->db->update('club_users', $data);
                  $arrReturn = array('flag' => 1, 'msg' => 'Password has been changed Successfully','success'=>200);
                  echo json_encode($arrReturn); exit();
              }else{
                    $arrReturn = array('flag' => 0, 'msg' => 'password does not match please check your password','error'=>400);
                  echo json_encode($arrReturn); exit;
              }
            }
    }

    /*
    this function is   get new   forgot password 

    */
    private function forgotpassword(){
            $phone_number  = $this->input->post('phone_number', true);
            if($phone_number==""){  
                $arrReturn = array('flag' => 0, 'msg' => 'Phone Number full_name and password field is required','error'=>400);
                echo json_encode($arrReturn); exit;
            }else{
                $query = $this->db->get_where('club_users', array('phone_number' => $phone_number));
                foreach ($query->result() as $row){
                $fname=$row->full_name;
                $phone_number1=$row->phone_number;
                }
              if($phone_number1==$phone_number){
                
                  $code = mt_rand(100000, 999999);
                  $data = array(                
                  'password' => md5($code));
                  $this->db->where('phone_number',$phone_number);
                  $this->db->update('club_users', $data);
                  $masg = " Dear users the OTP for the clubmate application is ".$code."  kindly do not share it with others- Clubmate"; 
                //  $masg = "Hi ".$fname. " OTP  is  ".$code;
                  $this->send_sms($phone_number,$masg,$code);
                  $arrResponse = array('flag' => 1,'msg'=>'OTP  Send your  register  phone Number  ','success'=>200,'data'=>array('authtoken'=>$row->apiKey));
                  echo json_encode($arrResponse); exit;
              }else{
                      $arrReturn = array('flag' => 0, 'msg' => 'phone number  does not exists.','error'=>400);
                      echo json_encode($arrReturn); exit;
                }
            }


    } 


    /*this functon is used  to get user profile */
    private function get_user_profile(){
      $authtoken  = $this->input->post('authtoken', true);
    if($authtoken==""|| $authtoken == 'null' ){  
                    $arrReturn = array('flag' => 0, 'msg' => 'Authtoken field is required','error'=>400);
                    echo json_encode($arrReturn); exit;
        }
    else{
      $userid = $this->getuserid($authtoken);
          if($userid)
          {
            $getdata = $this->rest_api->get_user_profile_by_user_id($userid); 
            $arrResponse = array('flag' => 1,'msg'=>'User profile info','success'=>200,'data'=>$getdata);
                    echo json_encode($arrResponse); exit;

          }else{
            $arrReturn = array('flag' => 0, 'msg' => 'Data Not Found','error'=>400);
                        echo json_encode($arrReturn); exit;
          }
          $arrReturn = array('flag' => 0, 'msg' => 'Sever Error please try again','error'=>400);
                  echo json_encode($arrReturn); exit;
      }
    }
    /*
        User profile  Edit
    */
        private function update_profile_info(){
        $authtoken         = $this->input->post('authtoken');
        $dob               = $this->input->post('dob');
        $full_name         = $this->input->post('full_name');
        $gender            = $this->input->post('gender');
        $food              = $this->input->post('food');
        $dance             = $this->input->post('dance');
        $type_of_drink     = $this->input->post('type_of_drink');
        $intrested_in      = $this->input->post('intrested_in');
        $bio               = $this->input->post('bio');
        if($full_name ==""|| $authtoken == 'null'  ||$gender==""||  $food==""){
                $arrReturn = array('flag' => 0, 'msg' => 'All  field is required','error'=>400);
                echo json_encode($arrReturn); exit;
        }else{
          $userid  = $this->getuserid($authtoken);
          $this->rest_api->update_profile_info($userid,$dob,$full_name,$gender,$food,$dance,$type_of_drink,$intrested_in,$bio);
          if(!empty($userid)){
            move_uploaded_file($_FILES['userfile']['tmp_name'], 'upload_files/users/' . $userid . '.jpg');
                $this->rest_api->clear_cache();
                $arrResponse = array('flag' => 1,'msg'=>'your profile has been successfully updated','success'=>200);
          }else{
                $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
          }
          echo json_encode($arrResponse); exit;
        }
    }
    /*
        Registration for  Event Function 
    */
    private function register_event(){
      $authtoken    = $this->input->post('authtoken');
      $type         = $this->input->post('type');
        $club_place_id= $this->input->post('club_place_id');
        $date         = $this->input->post('date');
        $time         = $this->input->post('time');
        $club_name    = $this->input->post('club_name');
        $latitude     = $this->input->post('latitude');
        $longitude     = $this->input->post('longitude');
        $offering_a_drink  = $this->input->post('offering_a_drink');
        $request_a_drink  = $this->input->post('request_a_drink');
        $group_type    = $this->input->post('group_type');
        $multiple_notification  = $this->input->post('multiple_notification');
        

        
    if($authtoken =="" || $authtoken == 'null'  ||$club_place_id==""|| $date =="" || $time=="" ||$offering_a_drink ==""|| $group_type=="" || $club_name =='' || $latitude=='' || $longitude ==''|| $request_a_drink ==''){
                $arrReturn = array('flag' => 0, 'msg' => 'All  field is required','error'=>400);
                echo json_encode($arrReturn); exit;
        }else{
          $sender_id = $this->getuserid($authtoken);
        $register_id = $this->rest_api->register_event($authtoken,$club_place_id,$date,$time,$type,$group_type,$club_name,$latitude,$longitude,$offering_a_drink,$request_a_drink,$sender_id);
        $data = array('register_id' =>$register_id,
                        'group_type' =>$group_type);
          if(!empty($register_id)){
            if($multiple_notification == '1'){
              
                $this->sent_pushnotification($latitude ,$longitude,$sender_id,$club_name,$register_id,$club_place_id,$date,$time);
            }
                $arrResponse = array('flag' => 1,'msg'=>'Your event has been register successfully','success'=>200,'data'=>$data);
          }else{
                $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
          }
            echo json_encode($arrResponse); exit;
        }

    }

/*  Send  OTP  Register Member  */
    private function send_otp_message_member(){
        $authtoken     = $this->input->post('authtoken');
        $member_name   = $this->input->post('member_name');
        $date          = $this->input->post('date');
        $time          = $this->input->post('time');
        $member_phone_no = $this->input->post('member_phone_no');
        $message_thread_code = mt_rand(100000, 999999);
        
    if($authtoken =="" || $authtoken == 'null'  ||$member_name==""|| $member_phone_no==""){
                $arrReturn = array('flag' => 0, 'msg' => 'All  field is required','error'=>400);
                echo json_encode($arrReturn); exit;
        }else{
          $userid = $this->getuserid($authtoken);
        $phone_number =  $this->db->get_where('club_users',array('id'=>$userid))->row()->phone_number;
          if($phone_number == $member_phone_no){
            $arrReturn = array('flag' => 0, 'msg' => 'Phone Number already exist','error'=>400);
            echo json_encode($arrReturn); exit;
          }else{
            $member_id = $this->rest_api->send_otp_messages($message_thread_code,$member_phone_no);
            $data = array('member_id' =>$member_id);

            $masg = "Hi ".$member_name. " your OTP Number is ".$message_thread_code;
            $this->send_sms($member_phone_no,$masg,$message_thread_code);
          $arrResponse = array('flag' => 1,'msg'=>' OTP  Sent successfully','success'=>200,'data'=>$data);
            echo json_encode($arrResponse); exit;
          }
        }
    }
    /*This function is Memeber   verified  otp   in our app*/
    private function member_verification(){
      $code             = $this->input->post('code');
      $authtoken        = $this->input->post('authtoken');
      $place_id         = $this->input->post('place_id');
      $event_id         = $this->input->post('register_id');
      $member_phone_number = $this->input->post('member_phone_number');
      $member_name      = $this->input->post('member_name');
      $member_age       = $this->input->post('member_age');
      $member_sex       = $this->input->post('member_sex');
      if($code == "" || $authtoken == 'null' || $authtoken =="" || $place_id=="" || $event_id =="" || $member_phone_number =="" || $member_name == ""|| $member_age ==""||$member_sex =="" ){
                $arrReturn = array('flag' => 0, 'msg' => 'All  field is required','error'=>400);
                echo json_encode($arrReturn); exit;
        }else{
        $userid = $this->getuserid($authtoken);
          $getcode = $this->db->get_where('club_verification',array('code'=>$code))->row()->code;
          if($getcode == $code){
              $member_id = $this->rest_api->member_verification($code,$userid,$place_id,$event_id,$member_phone_number,$member_name,$member_age,$member_sex);
                $data = array('member_name'=>$member_name,
                                    'member_phone_number'=>$member_phone_number,
                                  'member_age'   =>$member_age,
                                  'member_sex'   =>$member_sex,
                                  'member_id' => $member_id);

            $arrResponse = array('flag' => 1,'msg'=>'Your verification has been verified successfully','success'=>200,'data'=>$data);
            }else{
              $arrResponse = array('flag' => 0, 'msg' => 'OTP does not exist','error'=>400);
            }
              echo json_encode($arrResponse); exit; 
        }
    }
    /* Delete  Member  by Event Id*/

    private function delte_member_by_Event_id(){
        $member_register_id             = $this->input->post('member_id');
        if($member_register_id == "" ){
                  $arrReturn = array('flag' => 0, 'msg' => 'All  field is required','error'=>400);
                  echo json_encode($arrReturn); exit;
          }else{
        $this->rest_api->delete_member_info($member_register_id);
                $arrReturn = array('flag' => 1, 'msg' => 'Member  info delete successfuly','success'=>200);
                echo json_encode($arrReturn); exit;
      }
  }

      /*
        This Function is used Event Count  By place Id 
    */
    private function event_count_by_place_id(){
      
      $authtoken    = $this->input->post('authtoken');
        $club_place_id= $this->input->post('club_place_id');
        $timezone= $this->input->post('timezone');
    if($authtoken =="" || $authtoken == 'null' ||$club_place_id==""){
                $arrReturn = array('flag' => 0, 'msg' => 'All  field is required','error'=>400);
                echo json_encode($arrReturn); exit;
        }else{
          date_default_timezone_set($timezone);
          $timestmp = date('d-m-Y');
           
          $users = $this->rest_api->getall_event_by_user_id($authtoken,$type,$timestmp);
        $user_Count[]  = $this->rest_api->event_count_by_place_id($authtoken,$club_place_id,$timestmp);
          if(!empty($user_Count)){
                $arrResponse = array('flag' => 1,'msg'=>'Total Requst ','success'=>200,'data'=>$user_Count);
          }else{
                $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
          }
            echo json_encode($arrResponse); exit;
        }

    }
    /*Get user  By Event  */
    private function event_users_by_place_id(){
     
      $authtoken    = $this->input->post('authtoken');
        $club_place_id= $this->input->post('club_place_id');
        $timezone= $this->input->post('timezone');
        if($authtoken =="" || $authtoken == 'null'  ||$club_place_id==""){
                $arrReturn = array('flag' => 0, 'msg' => 'All  field is required','error'=>400);
                echo json_encode($arrReturn); exit;
        }else{
          date_default_timezone_set($timezone);
          $timestmp = date('d-m-Y');
          $users = $this->rest_api->getall_event_by_user_id($authtoken,$type,$timestmp);
            $userid  = $this->getuserid($authtoken);
            $users = $this->rest_api->event_users_by_place_id($authtoken,$club_place_id,$timestmp,$userid);
            //print_r($user_mamber);die();\
          if(!empty($users)){
                $arrResponse = array('flag' => 1,'msg'=>'Total Requst ','success'=>200,'data'=>$users);
          }else{
                $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
          }
            echo json_encode($arrResponse); exit;

        }
      
    }
        private function getall_event_by_user_id(){
              $authtoken    = $this->input->post('authtoken');
               $timezone= $this->input->post('timezone');
               $type     = $this->input->post('type');
                 if($authtoken =="" || $authtoken == 'null'  || $type ==""){
                        $arrReturn = array('flag' => 0, 'msg' => 'All  field is required','error'=>400);
                        echo json_encode($arrReturn); exit;
                }else{
                    $userid  = $this->getuserid($authtoken);
                    date_default_timezone_set($timezone);
                    $timestmp = date('d-m-Y');
                    $users = $this->rest_api->getall_event_by_user_id($authtoken,$type,$timestmp);
                    
                    //print_r($user_mamber);die();\
                  if(!empty($users)){
                        $arrResponse = array('flag' => 1,'msg'=>'Total Requst ','success'=>200,'data'=>$users);
                        echo json_encode($arrResponse); exit;
                      }else{
                        $arrResponse = array('flag' =>1,'msg'=>'Data not  Found','success'=>200,'data'=>$users);
                        echo json_encode($arrResponse); exit;
                      }
                }
                $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
                echo json_encode($arrResponse); exit;
            }


    /*view profile*/
      /*Get user  profile  By Id   */
      private function view_user_profile(){
      $authtoken  = $this->input->post('authtoken', true);
      $friend_authtoken  = $this->input->post('friend_authtoken', true);
      $event_id= $this->input->post('event_id');
    if($authtoken=="" || $authtoken == 'null' || $event_id =='' || $friend_authtoken ==''){  
    
                    $arrReturn = array('flag' => 0, 'msg' => 'Authtoken,event_id and friend_authtoken field is required','error'=>400);
                    echo json_encode($arrReturn); exit;
        }
    else{
      $userid = $this->getuserid($authtoken);
      $friend_id = $this->getuserid($friend_authtoken);
          if($userid)
          {
            $getdata = $this->rest_api->view_user_profile_by_user_id($userid,$friend_id,$authtoken,$event_id,$friend_authtoken); 
            if(!empty($eventid)){
            $user_mamber['mambers']= $this->rest_api->event_usersmember_by_event_id($event_id);
            $arrResponse = array('flag' => 1,'msg'=>'User profile info','success'=>200,'data'=>array_merge($getdata,$user_mamber));
            }else{
              $arrResponse = array('flag' => 1,'msg'=>'User profile info','success'=>200,'data'=>array_merge($getdata));
            }
            echo json_encode($arrResponse); exit;

          }else{
            $arrReturn = array('flag' => 0, 'msg' => 'Data Not Found','error'=>400);
                        echo json_encode($arrReturn); exit;
          }
          $arrReturn = array('flag' => 0, 'msg' => 'Sever Error please try again','error'=>400);
                  echo json_encode($arrReturn); exit;
      }
    }
  /* this function is used to  show  Send friend Request View*/
  private function send_firend_request_view(){

      $authtoken     = $this->input->post('authtoken', true);
    if($authtoken==""|| $authtoken == 'null' ){  
      $arrReturn = array('flag' => 0, 'msg' => 'Authtoken field is required','error'=>400);
        echo json_encode($arrReturn); exit;
        }else{
          $data = $this->rest_api->send_firend_request_view($authtoken);
          $arrResponse = array('flag' => 1,'msg'=>'Show  Friend  Request  data ','success'=>200,'data'=>$data);
                  echo json_encode($arrResponse); exit;
        }
        $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
          echo json_encode($arrResponse); exit;

    }


    /*Send Freind   */
    private function send_friend_request(){
      $authtoken     = $this->input->post('authtoken', true);
      $user_authtoken  = $this->input->post('friend_authtoken');
    if($authtoken==""|| $authtoken == 'null'  || $user_authtoken ==''){  
      $arrReturn = array('flag' => 0, 'msg' => 'Authtoken field is required','error'=>400);
        echo json_encode($arrReturn); exit;
        }else{
          $user_id   = $this->getuserid($authtoken);
          $friend_id = $this->getuserid($user_authtoken);
          $check_friend =  $this->db->get_where('club_friend_request',array('user_id'=>$authtoken,'friend'=>$friend_id))->row()->friend;
          if($check_friend ==$friend_id){
            $arrReturn = array('flag' => 0, 'msg' => 'Friend request already sended','error'=>400);
            echo json_encode($arrReturn); exit;
          }else{
              $data = $this->rest_api->requet_send_data($authtoken,$friend_id);
                if(!empty($data)){
                  $frinddata= $this->db->get_where('club_users',array('id'=>$friend_id))->result_array();
                    $os= $frinddata['0']['os'];
                    if($os =='1'){
                        $key = $frinddata['0']['gkey'];
                    }else{
                      $key   = $frinddata['0']['ikey'];
                    }   

                  $user_name = $this->general->get_single_record('club_users', 'full_name', 'id', $user_id);
                  $title = $user_name->full_name;
                  $body  = 'has sent you a friend request';
                  $datas = array('id'=>$user_id,
                                'name'=>$user_name,
                                'requestid' =>$data,
                                  'user_authtoken'=>$authtoken,
                                  'type'=>'frienRequest'); 
                  $this->sendPushNotificationToFCMSever($key,$title,$body,$datas);

                            $arrResponse = array('flag' => 1,'msg'=>'Friend request has been sent successfully','success'=>200,'data'=>$data);
                      }else{
                            $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
                      }
                      echo json_encode($arrResponse); exit;
            }
        }
    }

    private function received_firend_request_view(){

      $authtoken     = $this->input->post('authtoken', true);
    if($authtoken==""|| $authtoken == 'null' ){  
      $arrReturn = array('flag' => 0, 'msg' => 'Authtoken field is required','error'=>400);
        echo json_encode($arrReturn); exit;
        }else{
          $user_id = $this->getuserid($authtoken);
          $data = $this->rest_api->received_firend_request($user_id);
          $arrResponse = array('flag' => 1,'msg'=>'Friend request views','success'=>200,'data'=>$data);
                  echo json_encode($arrResponse); exit;
        }
        $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
          echo json_encode($arrResponse); exit;

    }

      /*reject_friend_request Send Freind   */ 
    private function cancel_friend_request(){
      $authtoken     = $this->input->post('authtoken', true);
      $friend_authtoken  = $this->input->post('friend_authtoken');
    if($authtoken=="" || $authtoken == 'null' || $friend_authtoken ==''){  
      $arrReturn = array('flag' => 0, 'msg' => 'Authtoken  and friend_authtoken field is required','error'=>400);
        echo json_encode($arrReturn); exit;
        }else{

          $friend_id = $this->getuserid($friend_authtoken);
          $data = $this->rest_api->reject_friend_request($authtoken,$friend_id);
          $arrResponse = array('flag' => 1,'msg'=>'Friend request has been rejected successfully','success'=>200,'data'=>$data);
                  echo json_encode($arrResponse); exit;
        }
        $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
          echo json_encode($arrResponse); exit;
    }


    /*Cencel Request Id*/
          /*reject_friend_request Send Freind   */
    private function reject_friend_request(){
        $authtoken     = $this->input->post('authtoken', true);
        $friend_authtoken  = $this->input->post('friend_authtoken');
      if($authtoken=="" || $authtoken == 'null' || $friend_authtoken ==''){  
        $arrReturn = array('flag' => 0, 'msg' => 'Authtoken  and friend_authtoken field is required','error'=>400);
          echo json_encode($arrReturn); exit;
          }else{
            
            $user_id = $this->getuserid($authtoken);
            $data = $this->rest_api->cancel_friend_request($friend_authtoken,$user_id);
            $arrResponse = array('flag' => 1,'msg'=>'Friend request has been Cancel successfully','success'=>200,'data'=>$data);
                    echo json_encode($arrResponse); exit;
          }
          $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
            echo json_encode($arrResponse); exit;
      }
      /* accept_friend_request*/
      private function accept_friend_request(){
        $authtoken  = $this->input->post('authtoken', true);
      // $requestid        = $this->input->post('requestid');
        $friend_authtoken  = $this->input->post('friend_authtoken');
      if($authtoken=="" || $authtoken == 'null' || $friend_authtoken == ''){  
        $arrReturn = array('flag' => 0, 'msg' => 'Authtoken field is required','error'=>400);
          echo json_encode($arrReturn); exit;
          }else{
              $user_id = $this->getuserid($authtoken);
              $friend_id = $this->getuserid($friend_authtoken);
              $check_friend =  $this->db->get_where('club_my_friends',array('user_id'=>$user_id,'friend'=>$friend_id))->row()->friend;
              if($check_friend == $friend_id){
                $arrReturn = array('flag' => 0, 'msg' => 'Friend request already Accepted','error'=>400);
                echo json_encode($arrReturn); exit;
              }else{
                    $data = $this->rest_api->accept_friend_request($user_id,$friend_id,$friend_authtoken);
                    $frinddata= $this->db->get_where('club_users',array('id'=>$friend_id))->result_array();
                          $os= $frinddata['0']['os'];
                          if($os =='1'){
                              $key = $frinddata['0']['gkey'];
                          }else{
                            $key   = $frinddata['0']['ikey'];
                          }   
                        $user_name = $this->general->get_single_record('club_users', 'full_name', 'id', $user_id);
                        $title = $user_name->full_name;
                        $body  = ' has accept your friend request';
                        $datas = array('id'=>$user_id,
                                      'name'=>$user_name,
                                      'requestid' =>$data,
                                        'user_authtoken'=>$authtoken,
                                        'type'=>'frienRequest'); 
                        $this->sendPushNotificationToFCMSever($key,$title,$body,$datas);
                    $arrResponse = array('flag' => 1,'msg'=>'Friend request has been Acceptd successfully','success'=>200,'data'=>$data);
                            echo json_encode($arrResponse); exit;
                }
          }
          $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
            echo json_encode($arrResponse); exit;
      }
    /*My all Friend Request */

  private function getallfiends(){
    $authtoken     = $this->input->post('authtoken', true);
    if($authtoken==""|| $authtoken == 'null' ){  
      $arrReturn = array('flag' => 0, 'msg' => 'Authtoken field is required','error'=>400);
        echo json_encode($arrReturn); exit;
        }else{
            $user_id = $this->getuserid($authtoken);
          $data = $this->rest_api->my_friend_list($user_id);
          $arrResponse = array('flag' => 1,'msg'=>'my all friend list','success'=>200,'data'=>$data);
                  echo json_encode($arrResponse); exit;
        }
        $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
          echo json_encode($arrResponse); exit;


  }
/* Block  all Frend List*/
  

  private function list_block_friend(){
    $authtoken     = $this->input->post('authtoken', true);
    if($authtoken==""|| $authtoken == 'null' ){  
      $arrReturn = array('flag' => 0, 'msg' => 'Authtoken field is required','error'=>400);
        echo json_encode($arrReturn); exit;
        }else{
            $user_id = $this->getuserid($authtoken);
          $data = $this->rest_api->list_block_friend($user_id);
          $arrResponse = array('flag' => 1,'msg'=>'my all  blocked friend list','success'=>200,'data'=>$data);
                  echo json_encode($arrResponse); exit;
        }
        $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
          echo json_encode($arrResponse); exit;


  }
/*Un Block  My friend  */

  private function un_block_my_friend(){
      $authtoken     = $this->input->post('authtoken', true);
      $friend_authtoken     = $this->input->post('friend_authtoken', true);
    if($authtoken==" " || $authtoken == 'null' || $friend_authtoken == " " || $friend_authtoken == 'null' ){  
      $arrReturn = array('flag' => 0, 'msg' => 'Authtoken field is required','error'=>400);
        echo json_encode($arrReturn); exit;
        }else{
            $user_id = $this->getuserid($authtoken);
            $friend_id = $this->getuserid($friend_authtoken);
          $data = $this->rest_api->un_block_my_friend($user_id,$friend_id);
          $arrResponse = array('flag' => 1,'msg'=>'This Friend request has been Unbloked successfully','success'=>200,'data'=>$data);
                  echo json_encode($arrResponse); exit;
        }
        $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
          echo json_encode($arrResponse); exit;
  }

  /*bolck My firend  */
  private function block_my_friend(){
      $authtoken     = $this->input->post('authtoken', true);
      $friend_authtoken     = $this->input->post('friend_authtoken', true);
    if($authtoken=="" || $authtoken == 'null' || $friend_authtoken == ""){  
      $arrReturn = array('flag' => 0, 'msg' => 'Authtoken field is required','error'=>400);
        echo json_encode($arrReturn); exit;
        }else{
            $user_id   = $this->getuserid($authtoken);
            $friend_id = $this->getuserid($friend_authtoken);
          $data = $this->rest_api->block_my_friend($user_id,$friend_id);
          $arrResponse = array('flag' => 1,'msg'=>'This Friend request has been booked successfully','success'=>200,'data'=>$data);
                  echo json_encode($arrResponse); exit;
        }
        $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
          echo json_encode($arrResponse); exit;
  }

  /* Delete  My Friend  in List*/
      private function delete_my_friend(){
          $authtoken     = $this->input->post('authtoken', true);
          $user_authtoken     = $this->input->post('friend_authtoken', true);
        if($authtoken==""|| $authtoken == 'null'  || $user_authtoken == ""){  
          $arrReturn = array('flag' => 0, 'msg' => 'Authtoken field is required','error'=>400);
            echo json_encode($arrReturn); exit;
            }else{
                $user_id = $this->getuserid($authtoken);
                $friend_id = $this->getuserid($user_authtoken);
              // echo $user_id.' '.$friend_id;die();
              $data = $this->rest_api->delete_my_friend($user_id,$friend_id);
              $arrResponse = array('flag' => 1,'msg'=>'This Friend request has been Deleted successfully','success'=>200,'data'=>$data);
                      echo json_encode($arrResponse); exit;
            }
            $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
              echo json_encode($arrResponse); exit;   
    }

  /*pair Up */


  /* this function is used to  show  Send friend Request View*/
  private function send_pairUp_request_view(){

      $authtoken     = $this->input->post('authtoken', true);
    if($authtoken==""){  
      $arrReturn = array('flag' => 0, 'msg' => 'Authtoken field is required','error'=>400);
        echo json_encode($arrReturn); exit;
        }else{
          $user_id = $this->getuserid($authtoken);
          $data = $this->rest_api->send_pairUp_request_view($user_id);
          $arrResponse = array('flag' => 1,'msg'=>'Show  pair Up  Request  data ','success'=>200,'data'=>$data);
                  echo json_encode($arrResponse); exit;
        }
        $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
          echo json_encode($arrResponse); exit;

    }


    /*Send Freind   */
    private function send_pairUp_request(){
      $authtoken     = $this->input->post('authtoken', true);
      $user_authtoken  = $this->input->post('friend_authtoken');
      $event_id        = $this->input->post('event_id');
    if($authtoken=="" || $user_authtoken =='' || $event_id == ""){  
      $arrReturn = array('flag' => 0, 'msg' => 'Authtoken field is required','error'=>400);
        echo json_encode($arrReturn); exit;
        }else{
          $user_id = $this->getuserid($authtoken);
          $friend_id = $this->getuserid($user_authtoken);
            $frinddata= $this->db->get_where('club_users',array('id'=>$friend_id))->result_array();
            $os= $frinddata['0']['os'];
            if($os =='1'){
                $key = $frinddata['0']['gkey'];
            }else{
              $key   = $frinddata['0']['ikey'];
            }   
            $data = $this->rest_api->pairUprequet_send_data($user_id,$friend_id,$event_id);
            if(!empty($data)){
              $user_name = $this->general->get_single_record('club_users', 'full_name', 'id', $user_id);
              $title =  $user_name->full_name;
              $body  ='has sent you a pair request';
              $datas = array('id'=>$user_id,
                            'event_id' =>$event_id,
                            'name'=>$user_name,
                            'requestid' =>$data,
                            'user_authtoken'=>$authtoken,
                              'type'=>'pair'); 
              $this->sendPushNotificationToFCMSever($key,$title,$body,$datas);
                        $arrResponse = array('flag' => 1,'msg'=>'Pair up request has been sent successfully','success'=>200,'data'=>$data);
                  }else{
                        $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
                  }
                  echo json_encode($arrResponse); exit;
        }
    }

    private function received_pairUp_request_view(){

      $authtoken     = $this->input->post('authtoken', true);
    if($authtoken==""|| $authtoken == 'null' ){  
      $arrReturn = array('flag' => 0, 'msg' => 'Authtoken field is required','error'=>400);
        echo json_encode($arrReturn); exit;
        }else{
          $user_id = $this->getuserid($authtoken);
          $data = $this->rest_api->received_pairUp_request_view($user_id);
          $arrResponse = array('flag' => 1,'msg'=>'Pair Up request views','success'=>200,'data'=>$data);
                  echo json_encode($arrResponse); exit;
        }
        $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
          echo json_encode($arrResponse); exit;

    }

      /*reject_friend_request Send Freind   */
    private function reject_pairUp_request(){
      $authtoken     = $this->input->post('authtoken', true);
      $event_id      = $this->input->post('event_id', true);
      $friend_authtoken  = $this->input->post('friend_authtoken');
    if($authtoken=="" || $authtoken == 'null' || $friend_authtoken =='' || $event_id == ''){  
      $arrReturn = array('flag' => 0, 'msg' => 'Authtoken event_id And friend_authtoken field is required','error'=>400);
        echo json_encode($arrReturn); exit;
        }else{
          $user_id = $this->getuserid($authtoken);
          $friend_id = $this->getuserid($friend_authtoken);
        // $friends = $this->general->get_single_record('club_pair_up_request', 'user_id', 'pair_up_request_id', $requestid);  
            $frinddata= $this->db->get_where('club_users',array('id'=>$friend_id))->result_array();
            $os= $frinddata['0']['os'];
            if($os =='1'){
                $key = $frinddata['0']['gkey'];
            }else{
              $key   = $frinddata['0']['ikey'];
            }   
          $user_name = $this->general->get_single_record('club_users', 'full_name', 'id', $user_id);
              $title = $user_name->full_name;
              $body  = 'has rejected your pair Up request';
              $datas = array('id'=>$user_id,
                            'name'=>$user_name,
                              'requestid' =>$requestid,
                              'event_id' =>$event_id,
                              'user_authtoken'=>$authtoken,
                              'type'=>'pair'); 
            $data = $this->rest_api->reject_pairUp_request($user_id,$friend_id,$event_id);
              $this->sendPushNotificationToFCMSever($key,$title,$body,$datas);

          $arrResponse = array('flag' => 1,'msg'=>'Pair Up request has been rejected successfully','success'=>200,'data'=>$data);
                  echo json_encode($arrResponse); exit;
        }
        $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
          echo json_encode($arrResponse); exit;
    }

      /*reject_friend_request Send Freind   */
    private function cancel_pairUp_request(){
      $authtoken     = $this->input->post('authtoken', true);
      $event_id     = $this->input->post('event_id', true);
      $friend_authtoken  = $this->input->post('friend_authtoken');
    if($authtoken=="" || $authtoken == 'null' || $friend_authtoken =='' || $event_id ==''){  
      $arrReturn = array('flag' => 0, 'msg' => 'Authtoken event_id, And friend_authtoken field is required','error'=>400);
        echo json_encode($arrReturn); exit;
        }else{
          $user_id = $this->getuserid($authtoken);
          $friend_id = $this->getuserid($friend_authtoken);

    //     $friends = $this->general->get_single_record('club_pair_up_request', 'user_id', 'pair_up_request_id', $requestid);  
            $frinddata= $this->db->get_where('club_users',array('id'=>$friend_id))->result_array();
            $os= $frinddata['0']['os'];
            if($os =='1'){
                $key = $frinddata['0']['gkey'];
            }else{
              $key   = $frinddata['0']['ikey'];
            }   
          $user_name = $this->general->get_single_record('club_users', 'full_name', 'id', $user_id);
              $title = $user_name->full_name;
              $body  = 'has rejected your pair Up request';
              $datas = array('id'=>$user_id,
                            'name'=>$user_name,
                              'requestid' =>$requestid,
                              'event_id' =>$event_id,
                              'user_authtoken'=>$authtoken,
                              'type'=>'pair'); 
            $data = $this->rest_api->cancel_pairUp_request($user_id,$friend_id,$event_id);
              $this->sendPushNotificationToFCMSever($key,$title,$body,$datas);

          $arrResponse = array('flag' => 1,'msg'=>'Pair Up request has been rejected successfully','success'=>200,'data'=>$data);
                  echo json_encode($arrResponse); exit;
        }
        $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
          echo json_encode($arrResponse); exit;
    }
    /* accept_friend_request*/
    private function accept_pair_up_request(){
      $authtoken  = $this->input->post('authtoken', true);
    // $requestid  = $this->input->post('requestid');
      $event_id    = $this->input->post('event_id');
      $friend_authtoken  = $this->input->post('friend_authtoken');
    if($authtoken=="" || $authtoken == 'null' || $friend_authtoken == '' || $event_id =='' || $event_id =='null'){  
      $arrReturn = array('flag' => 0, 'msg' => 'Authtoken field is required','error'=>400);
        echo json_encode($arrReturn); exit;
        }else{
          $user_id = $this->getuserid($authtoken);
          $friend_id = $this->getuserid($friend_authtoken);
  //    $friends = $this->general->get_single_record('club_pair_up_request', 'user_id', 'pair_up_request_id', $requestid);  
            $frinddata= $this->db->get_where('club_users',array('id'=>$friend_id))->result_array();
            $os= $frinddata['0']['os'];
            if($os =='1'){
                $key = $frinddata['0']['gkey'];
            }else{
              $key   = $frinddata['0']['ikey'];
            }   
          $user_name = $this->general->get_single_record('club_users', 'full_name', 'id', $user_id);
              $title = $user_name->full_name;
              $body  = 'has Acceptd your pair Up request';
              $datas = array('id'=>$user_id,
                            'name'=>$user_name,
                              'requestid' =>$requestid,
                              'event_id' =>$event_id,
                              'user_authtoken'=>$authtoken,
                              'type'=>'pair'); 
            $data = $this->rest_api->accept_pair_up_request($user_id,$friend_id,$event_id);
              $this->sendPushNotificationToFCMSever($key,$title,$body,$datas);
          $arrResponse = array('flag' => 1,'msg'=>'pair request has been Acceptd successfully','success'=>200,'data'=>$data);
                  echo json_encode($arrResponse); exit;
        }
        $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
          echo json_encode($arrResponse); exit;
    }
    /*My all Friend Request */

  private function getall_pairUpfiends(){
    $authtoken     = $this->input->post('authtoken', true);
    if($authtoken==""|| $authtoken == 'null' ){  
      $arrReturn = array('flag' => 0, 'msg' => 'Authtoken field is required','error'=>400);
        echo json_encode($arrReturn); exit;
        }else{
            $user_id = $this->getuserid($authtoken);
          $data = $this->rest_api->my_pairUpfriend_list($user_id);
          $arrResponse = array('flag' => 1,'msg'=>'my all friend list','success'=>200,'data'=>$data);
                  echo json_encode($arrResponse); exit;
        }
        $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
          echo json_encode($arrResponse); exit;


  }
  /*Delete  my pair up List  */
    private function delete_my_pairUp_friend(){
          $authtoken     = $this->input->post('authtoken', true);
          $user_authtoken     = $this->input->post('friend_authtoken', true);
          $event_id        = $this->input->post('event_id', true);
        if($authtoken==""|| $authtoken == 'null'  || $user_authtoken == "" || $event_id ==''){  
          $arrReturn = array('flag' => 0, 'msg' => 'Authtoken event_id field is required','error'=>400);
            echo json_encode($arrReturn); exit;
            }else{
                $user_id = $this->getuserid($authtoken);
                $friend_id = $this->getuserid($user_authtoken);
              // echo $user_id.' '.$friend_id;die();
              $data = $this->rest_api->delete_my_pairUp_friend($user_id,$friend_id,$event_id);
              $arrResponse = array('flag' => 1,'msg'=>'PairUp Friend request has been Deleted successfully','success'=>200,'data'=>$data);
                      echo json_encode($arrResponse); exit;
            }
            $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
              echo json_encode($arrResponse); exit;   
    }
  /*  Report  */
    private function send_report(){
      $authtoken  = $this->input->post('authtoken', true);
      $friend    = $this->input->post('friend_authtoken', true);
      $title    = $this->input->post('title', true);
      $msg     = $this->input->post('msg', true);
      if($authtoken=="" || $authtoken == 'null' ||  $friend == "" || $title =="" || $msg == ""){  
      $arrReturn = array('flag' => 0, 'msg' => 'All field is required','error'=>400);
        echo  json_encode($arrReturn); exit;
        }else{
            $user_id = $this->getuserid($authtoken);
          $data = $this->rest_api->send_report($user_id,$friend,$title,$msg);
          if($data){
                  $arrResponse = array('flag' => 1,'msg'=>'Report Send successfully to admin','success'=>200,'data'=>$data); 
          }else{
            $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400); 
          }
          echo json_encode($arrResponse); exit;
        }
        $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
          echo json_encode($arrResponse); exit;
    }

    private function getSubscriptionPlanlist_post(){
      $authtoken     = $this->input->post('authtoken', true);
      if($authtoken=="" || $authtoken == "null"){  
      $arrReturn = array('flag' => 0, 'msg' => 'All field is required','error'=>400);
        echo  json_encode($arrReturn); exit;
        }else{
          $user_id = $this->getuserid($authtoken);
             $date = date('d-m-Y');
            $getdate  = $this->db->get_where('club_subscriptionplanmaster_activate',array('user_id'=>$user_id,'activate_status'=>'1'))->row()->date;
           // echo   $getdate;die;
            if(strtotime($getdate) < strtotime($date)){
                $this->db->update('club_subscriptionplanmaster_activate', array('activate_status' =>'3'), array('date' => $getdate));
            }
          $data = $this->rest_api->getSubscriptionPlanlist_post($user_id);
          $active_plns = $this->db->get_where('club_subscriptionplanmaster_activate', array('user_id' =>$user_id,'activate_status'=>'1','transaction_type'=>'2'))->result_array();
          if($data){
            $total_users  =$this->db->get_where('club_premium_user_use',array('transaction_id'=>$active_plns['0']['id']))->num_rows();
            $total_calls = $this->db->get_where('club_premium_user_call_insert',array('transaction_id'=>$active_plns['0']['id']))->num_rows();
                  $arrResponse = array('flag' => 1,'msg'=>'Subscription Plan list ','success'=>200,'data'=>$data,'active_plns'=>$active_plns['0']['subscriptionplanmaster_id'],'expiry_date'=>$active_plns['0']['date'],'total_calls'=>"$total_calls",'total_users'=>"$total_users"); 
          }else{
            $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400); 
          }
          echo json_encode($arrResponse); exit;
        }
        $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
          echo json_encode($arrResponse); exit;
    } 


    private function getSubscriptionPlanlist_monthly(){
      $authtoken     = $this->input->post('authtoken', true);
      if($authtoken=="" || $authtoken == "null"){  
      $arrReturn = array('flag' => 0, 'msg' => 'All field is required','error'=>400);
        echo  json_encode($arrReturn); exit;
        }else{
          $user_id = $this->getuserid($authtoken);
             $date = date('d-m-Y');
            $getdate  = $this->db->get_where('club_subscriptionplanmaster_activate',array('user_id'=>$user_id,'activate_status'=>'1'))->row()->date;
            if(strtotime($getdate) < strtotime($date)){
                $this->db->update('club_subscriptionplanmaster_activate', array('activate_status' =>'3'), array('date' => $getdate));
            }
          $data = $this->rest_api->getSubscriptionPlanlist_monthly($user_id);
          $active_plns = $this->db->get_where('club_subscriptionplanmaster_activate', array('user_id' =>$user_id,'activate_status'=>'1','transaction_type'=>'1'))->result_array();
          if($data){
                //  print_r($user_id);die;
            $total_users  =$this->db->get_where('club_premium_user_use',array('transaction_id'=>$active_plns['0']['id']))->num_rows();
            $total_calls = $this->db->get_where('club_premium_user_call_insert',array('transaction_id'=>$active_plns['0']['id']))->num_rows();

                  $arrResponse = array('flag' => 1,'msg'=>'Subscription Plan list ','success'=>200,'data'=>$data,'active_plns'=>$active_plns['0']['subscriptionplanmaster_id'],'expiry_date'=>$active_plns['0']['date'],'total_calls'=>"$total_calls",'total_users'=>"$total_users"); 
          }else{
            $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400); 
          }
          echo json_encode($arrResponse); exit;
        }
        $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
          echo json_encode($arrResponse); exit;
    } 
  // callback method
    private function getSubscriptionPlanlist_activates(){ 
              $authtoken        = $this->input->post('authtoken');
              $subscriptionplanmaster_id	 = $this->input->post('subscriptionplanmaster_id');
              $amounts            = $this->input->post('amount');
              $payment_id         = $this->input->post('payment_id');
              $transaction_type      = $this->input->post('transaction_type');
            
              if($authtoken ==""|| $authtoken == 'null' || $transaction_type =='' || $subscriptionplanmaster_id=="" || $amounts ==""  || $payment_id == ""){
                        $arrReturn = array('flag' => 0, 'msg' => 'All  field is required','error'=>400);
                        echo json_encode($arrReturn); exit;
                }else{
                    $userid = $this->getuserid($authtoken);
                  $payment_data =  $this->Razorpay_check_status($payment_id,$amounts);
                  $response_array = json_decode($payment_data, true);
                //print_r($response_array);die;
                  if($response_array['status'] =='captured'){
                  $transaction_id =  $response_array['id'];
                  $amounts =  $response_array['amount'];
                  $currency =  $response_array['currency'];
                  $status =  $response_array['status'];
                  $method =  $response_array['method'];
                  //$price =  $response_array['status'];
                //  $price =  $response_array['status'];
                  $amount=   sprintf('%.2f', $amounts / 100);
                    $data = $this->rest_api->getSubscriptionPlanlist_activates($userid,$subscriptionplanmaster_id,$currency,$amount,$status,$transaction_id,$method,$transaction_type);
                    $arrResponse = array('flag' => 1,'msg'=>'Your Transaction has been successfully Updated','success'=>200,'data'=>$data,'transaction_type'=>$transaction_type);
                  }else{
                    $arrResponse = array('flag' => 0, 'msg' => 'Transaction failed ','error'=>400);
                  }
                  echo json_encode($arrResponse); exit; 
              }
    }
  // premiun_user_status//
  private function premiun_user_status(){
    $authtoken        = $this->input->post('authtoken');
    if($authtoken ==""|| $authtoken == 'null'){
              $arrReturn = array('flag' => 0, 'msg' => 'All  field is required','error'=>400);
              echo json_encode($arrReturn); exit;
      }else{
        $userid = $this->getuserid($authtoken);
        $subscriptionplanmaster_id = $this->db->get_where('club_subscriptionplanmaster_activate',array('user_id'=>$userid,'transaction_id != '=>'free','activate_status'=>'1'))->result_array();
        if($subscriptionplanmaster_id['0']['transaction_type'] =='1'){
        $datas = $this->db->get_where('club_monthly_subscriptionplanmaster',array('id'=>$subscriptionplanmaster_id['0']['subscriptionplanmaster_id'],'alloweBroadcast '=>'yes'))->row()->id;
      }elseif($subscriptionplanmaster_id['0']['transaction_type'] =='2'){
        $datas = $this->db->get_where('club_subscriptionplanmaster',array('id'=>$subscriptionplanmaster_id['0']['subscriptionplanmaster_id'],'alloweBroadcast '=>'yes'))->row()->id;
      }
      
        
        if($datas){
          $arrReturn = array('flag' => 1,'msg'=>'Your premium id get successfully','success'=>200,'premium_id'=>$datas,'premium_status'=>true);
        }else{
          $arrReturn = array('flag' => 1,'msg'=>'Your premium not activate','success'=>200,'premium_id'=>$datas,'premium_status'=>false);
        }
        echo json_encode($arrReturn); exit;
      }
    }
    // club_premium_user_use
    private function club_premium_user_use_insert(){
      $authtoken        = $this->input->post('authtoken');
      $friend     = $this->input->post('friend_authtoken', true);
      if($authtoken ==""|| $authtoken == 'null' || $friend ==''){
                $arrReturn = array('flag' => 0, 'msg' => 'All  field is required','error'=>400);
                echo json_encode($arrReturn); exit;
        }else{
          $userid = $this->getuserid($authtoken);
          $friend = $this->getuserid($friend);
             $date = date('d-m-Y');
            $getdate  = $this->db->get_where('club_subscriptionplanmaster_activate',array('user_id'=>$userid,'activate_status'=>'1'))->row()->date;
            if(strtotime($getdate) < strtotime($date)){
                $this->db->update('club_subscriptionplanmaster_activate', array('activate_status' =>'3'), array('date' => $getdate));
            }
          $getfrindid  = $this->db->get_where('club_premium_user_use',array('user_id'=>$userid,'friend_id'=>$friend))->row()->friend_id;
          if($getfrindid ==$friend ){
            $arrReturn = array('flag' => 1, 'msg' => 'user  On Chate','errsuccessor'=>200,'chat_status'=>'1');
            echo json_encode($arrReturn); exit;
          }
          $premium_id =$this->db->get_where('club_subscriptionplanmaster_activate',array('user_id' =>$userid,'activate_status'=>'1'))->result_array();
          if($premium_id['0']['transaction_type'] =='1'){
            $subscriptionplanmaster = $this->db->get_where('club_monthly_subscriptionplanmaster',array('id' =>$premium_id['0']['subscriptionplanmaster_id']))->result_array();
          }elseif($premium_id['0']['transaction_type'] == '2'){
            $subscriptionplanmaster = $this->db->get_where('club_subscriptionplanmaster',array('id' =>$premium_id['0']['subscriptionplanmaster_id']))->result_array();
          }
          $count  = $this->db->get_where('club_premium_user_use',array('user_id'=>$userid))->num_rows();
            if($subscriptionplanmaster['0']['allowedUsers'] == 'Unlimited'){
              $this->rest_api->club_premium_user_use_insert($userid,$friend,$premium_id['0']['subscriptionplanmaster_id'],$premium_id['0']['id']);
              $arrReturn = array('flag' => 1,'msg'=>'user  insert successfully ','success'=>200,'chat_status'=>'1');
              echo json_encode($arrReturn); exit;
            }elseif($count == $subscriptionplanmaster['0']['allowedUsers']){
              $arrReturn = array('flag' => 1, 'msg' => 'porsche premium package ','error'=>400,'chat_status'=>'2');
              echo json_encode($arrReturn); exit;
            }else{
          $data   = $this->rest_api->club_premium_user_use_insert($userid,$friend,$premium_id['0']['subscriptionplanmaster_id'],$premium_id['0']['id']);
          if($data){
            $arrReturn = array('flag' => 1,'msg'=>'user  insert successfully ','success'=>200,'chat_status'=>'1');
          }else{
            $arrReturn = array('flag' => 1,'msg'=>'Server Error please try Again','error'=>400,'chat_status'=>'0');
          }
          echo json_encode($arrReturn); exit;
        }
      }
    }

      // Culing
      private function club_premium_user_call_insert(){
        $authtoken        = $this->input->post('authtoken');
        $friend     = $this->input->post('friend_authtoken', true);
        if($authtoken ==""|| $authtoken == 'null' || $friend ==''){
                  $arrReturn = array('flag' => 0, 'msg' => 'All  field is required','error'=>400);
                  echo json_encode($arrReturn); exit;
          }else{
            $userid = $this->getuserid($authtoken);
            $friend = $this->getuserid($friend);
            $getfrindid  = $this->db->get_where('club_premium_user_call_insert',array('user_id'=>$userid,'friend_id'=>$friend))->row()->friend_id;
             $date = date('d-m-Y');
            $getdate  = $this->db->get_where('club_subscriptionplanmaster_activate',array('user_id'=>$userid,'activate_status'=>'1'))->row()->date;
            if(strtotime($getdate) < strtotime($date)){
                $this->db->update('club_subscriptionplanmaster_activate', array('activate_status' =>'3'), array('date' => $getdate));
            }
            $premium_id =$this->db->get_where('club_subscriptionplanmaster_activate',array('user_id' =>$userid,'activate_status'=>'1'))->result_array();
            if($premium_id['0']['transaction_type'] =='1'){
              $subscriptionplanmaster = $this->db->get_where('club_monthly_subscriptionplanmaster',array('id' =>$premium_id['0']['subscriptionplanmaster_id']))->result_array();
            }elseif($premium_id['0']['transaction_type'] == '2'){
              $subscriptionplanmaster = $this->db->get_where('club_subscriptionplanmaster',array('id' =>$premium_id['0']['subscriptionplanmaster_id']))->result_array();
            }
            $count  = $this->db->get_where('club_premium_user_call_insert',array('user_id'=>$userid))->num_rows();
            if($subscriptionplanmaster['0']['allowedCalls'] == 'Unlimited'){
              $this->rest_api->club_premium_user_call_insert($userid,$friend,$premium_id['0']['subscriptionplanmaster_id'],$premium_id['0']['id']);
              $arrReturn = array('flag' => 1,'msg'=>'user  insert successfully ','success'=>200,'call_status'=>'1');
              echo json_encode($arrReturn); exit;
            }elseif($count == $subscriptionplanmaster['0']['allowedCalls']){
              $arrReturn = array('flag' => 1, 'msg' => 'porsche premium package ','error'=>400,'call_status'=>'2');
              echo json_encode($arrReturn); exit;
            }else{
            $data   = $this->rest_api->club_premium_user_call_insert($userid,$friend,$premium_id['0']['subscriptionplanmaster_id'],$premium_id['0']['id']);
            if($data){
              $arrReturn = array('flag' => 1,'msg'=>'user  insert successfully ','success'=>200,'call_status'=>'1');
            }else{
              $arrReturn = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400,'call_status'=>'0');
            }
            echo json_encode($arrReturn); exit;
          }
        }
      }

      // call chaeks
    private function premium_user_call_check(){
        $authtoken        = $this->input->post('authtoken');
        $friend     = $this->input->post('friend_authtoken', true);
        if($authtoken ==""|| $authtoken == 'null' || $friend ==''){
                  $arrReturn = array('flag' => 0, 'msg' => 'All  field is required','error'=>400);
                  echo json_encode($arrReturn); exit;
          }else{
            $userid = $this->getuserid($authtoken);
            $friend = $this->getuserid($friend);
            $date = date('d-m-Y');
            $getdate  = $this->db->get_where('club_subscriptionplanmaster_activate',array('user_id'=>$userid,'activate_status'=>'1'))->row()->date;
            if(strtotime($getdate) < strtotime($date)){
                $this->db->update('club_subscriptionplanmaster_activate', array('activate_status' =>'3'), array('date' => $getdate));
            }
            $premium_id =$this->db->get_where('club_subscriptionplanmaster_activate',array('user_id' =>$userid,'activate_status'=>'1'))->result_array();
            if($premium_id['0']['transaction_type'] =='1'){
              $subscriptionplanmaster = $this->db->get_where('club_monthly_subscriptionplanmaster',array('id' =>$premium_id['0']['subscriptionplanmaster_id']))->result_array();
            }elseif($premium_id['0']['transaction_type'] == '2'){
              $subscriptionplanmaster = $this->db->get_where('club_subscriptionplanmaster',array('id' =>$premium_id['0']['subscriptionplanmaster_id']))->result_array();
            }
            $count  = $this->db->get_where('club_premium_user_call_insert',array('user_id'=>$userid))->num_rows();
            if($subscriptionplanmaster['0']['allowedCalls'] == 'Unlimited'){
              
              $arrReturn = array('flag' => 1,'msg'=>'user  On call','success'=>200,'call_status'=>'1');
              echo json_encode($arrReturn); exit;
            }elseif($count < $subscriptionplanmaster['0']['allowedCalls']){
              $arrReturn = array('flag' => 1, 'msg' => 'user  On call','error'=>200,'call_status'=>'1');
              echo json_encode($arrReturn); exit;
            }
            elseif($count == $subscriptionplanmaster['0']['allowedCalls']){
              $arrReturn = array('flag' => 1, 'msg' => 'porsche premium package ','error'=>400,'call_status'=>'2');
              echo json_encode($arrReturn); exit;
            }
          }
        }
      
    private function user_radios_sating(){
      $authtoken               = $this->input->post('authtoken');
      $notification_status     = $this->input->post('notification_status', true);
      $radios_range            = $this->input->post('radios_range', true);
      if($authtoken ==""|| $authtoken == 'null' || $notification_status ==''){
                $arrReturn = array('flag' => 0, 'msg' => 'All  field is required','error'=>400);
                echo json_encode($arrReturn); exit;
        }else{
          $userid = $this->getuserid($authtoken);
          $data   = $this->rest_api->user_radios_sating($userid,$notification_status,$radios_range);
        
          if($data){
          $arrReturn = array('flag' => 1,'msg'=>'your Setting has been changed successfully','success'=>200);
          }else{
          $arrReturn = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
          }
        echo json_encode($arrReturn); exit;
        }
    }
    private function user_update_location(){
      $authtoken        = $this->input->post('authtoken');
      $lat             = $this->input->post('lat', true);
      $long            = $this->input->post('long', true);
      if($authtoken ==""|| $authtoken == 'null' || $lat ==''||$long=='' ){
                $arrReturn = array('flag' => 0, 'msg' => 'All  field is required','error'=>400);
                echo json_encode($arrReturn); exit;
        }else{
          $userid = $this->getuserid($authtoken);
          $data   = $this->rest_api->user_update_location($userid,$lat,$long);
        
          if($data){
              $arrReturn = array('flag' => 1,'msg'=>'your Location Update   successfully','success'=>200);
              }else{
              $arrReturn = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
              }
            echo json_encode($arrReturn); exit;
        }
    }
  private function sent_pushnotification($latitude='',$longitude='',$sender_id='',$club_name='',$club_id='',$club_place_id='',$date='',$time=''){
    
$n =1;
  
  $getdatas = $this->db->query("SELECT *,(((acos(sin((".$latitude."*pi()/180)) * sin((`lat`*pi()/180))+cos((".$latitude."*pi()/180)) * cos((`lat`*pi()/180)) * cos(((".$longitude."- `long`)*pi()/180))))*180/pi())*60*1.1515*1.609344) as distance 
          FROM `club_user_update_location` 
          WHERE notification_status ='1' AND user_id != ".$sender_id."
          GROUP BY radios_range
          HAVING distance < club_user_update_location.radios_range  || distance  = club_user_update_location.radios_range
          ORDER BY radios_range")->result_array();
           
      foreach($getdatas as $val){
        $data_insert = array( 'sender_id' => $sender_id,
                      'receiver_id'=>$val['user_id'],
                      'club_name'=>$club_name,
                      'event_id'=>$club_id,
                      'place_id'=>$club_place_id,
                      'date'  =>$date,
                      'time' =>$time,
        );
     
        $this->db->insert('club_broadcasts',$data_insert);
        $id = $this->db->insert_id();

        $frinddata= $this->db->get_where('club_users',array('id'=>$val['user_id']))->result_array();
        $os= $frinddata['0']['os'];
        if($os =='1'){
            $key = $frinddata['0']['gkey'];
        }elseif($os =='2'){
        $key   = $frinddata['0']['ikey'];
        }   
      $user_name = $this->general->get_single_record('club_users', 'full_name', 'id', $sender_id);
      $title = $user_name->full_name;
      $body  = 'has sent you a broadcast';
      $datas = array('id'            =>$sender_id,
                    'name'           =>$user_name,
                    'requestid'      =>$club_id,
                    'user_authtoken' =>$user_name->apiKey,
                    'type'          =>'broadcast'); 
      $this->sendPushNotificationToFCMSever($key,$title,$body,$datas);
      $n++;
      }
    return $id;
  }
  //  This  function is used to get  broadcast   view
  private function received_broadcast_view(){

    $authtoken     = $this->input->post('authtoken', true);
  if($authtoken==""|| $authtoken == 'null' ){  
    $arrReturn = array('flag' => 0, 'msg' => 'Authtoken field is required','error'=>400);
      echo json_encode($arrReturn); exit;
      }else{
        $user_id = $this->getuserid($authtoken);
        $data = $this->rest_api->received_broadcast_view($user_id);
        $arrResponse = array('flag' => 1,'msg'=>'broad cast view','success'=>200,'data'=>$data);
                echo json_encode($arrResponse); exit;
      }
      $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
        echo json_encode($arrResponse); exit;

  }
  // this Function is used to Check user  pair Up Request 
  private function check_pairup(){
      $authtoken        = $this->input->post('authtoken');
      $friend_authtoken = $this->input->post('friend_authtoken', true);
      $eventid         = $this->input->post('event_id', true);
      if($authtoken ==""|| $authtoken == 'null' || $friend_authtoken =='' ){
                $arrReturn = array('flag' => 0, 'msg' => 'All  field is required','error'=>400);
                echo json_encode($arrReturn); exit;
        }else{
          $userid = $this->getuserid($authtoken);
          $friend_id = $this->getuserid($friend_authtoken);
          $club_pair_up_request = $this->db->get_where('club_pair_up_request', array('user_id' =>$userid,'friend_id'=>$friend_id,'event_id'=>$eventid))->row()->friend_id;
        $club_pair_up_requests = $this->db->get_where('club_pair_up_request', array('user_id' =>$friend_id,'friend_id'=>$userid,'event_id'=>$eventid))->row()->friend_id;
        $club_my_pairup = $this->db->get_where('club_my_pairup', array('user_id' =>$userid,'friend_id'=>$friend_id,'event_id'=>$eventid))->row()->friend_id;
          if($eventid == 'null' ){ 
            $pairUp_status = '5'; 
          }elseif(!empty($club_pair_up_request)){
              $pairUp_status = '2';
          }elseif(!empty($club_pair_up_requests)){
            $pairUp_status = '4';
          }
          elseif(!empty($club_my_pairup)){
            $pairUp_status = '1'; 
          }else{
            $pairUp_status = '0';
          }
          $arrReturn = array('flag' => 1,'msg'=>'Friend  pairup status','success'=>200,'pairUp_status'=>$pairUp_status);
            echo json_encode($arrReturn); exit;
      }
      $arrReturn = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
      echo json_encode($arrReturn); exit;
    }


// seting view list  
private function user_radios_sating_view(){
  $authtoken        = $this->input->post('authtoken');
  if($authtoken ==""|| $authtoken == 'null'){
            $arrReturn = array('flag' => 0, 'msg' => 'All  field is required','error'=>400);
            echo json_encode($arrReturn); exit;
    }else{
      $userid = $this->getuserid($authtoken);
      $data = $this->rest_api->user_radios_sating_view($userid);
      if($data){
        $arrReturn = array('flag' => 1,'msg'=>'your Data is successfully  get','success'=>200,'data'=>$data);
        }else{
        $arrReturn = array('flag' => 1,'msg'=>'Data not found  please try Again','error'=>400);
        }
      echo json_encode($arrReturn); exit;
    }
    
    $arrReturn = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
    echo json_encode($arrReturn); exit;
}
    //contact_us
      private function contact_us()
      {
            $authtoken    = $this->input->post('authtoken');
            $full_name    = $this->input->post('full_name');
            $email        = $this->input->post('email');
            $phone_number = $this->input->post('phone_number');
            $msg          = $this->input->post('msg');

            if($authtoken ==""|| $authtoken == 'null' || $full_name == "" ||$email==""||$phone_number =="" || $msg ==""){
                      $arrReturn = array('flag' => 0, 'msg' => 'All  field is required','error'=>400);
                      echo json_encode($arrReturn); exit;
              }else{
                $userid = $this->getuserid($authtoken);
                $data = $this->rest_api->contact_us($userid,$full_name,$email,$phone_number,$msg);
                if($data){
                  $arrReturn = array('flag' => 1,'msg'=>'your Data is successfully  Sent  Server','success'=>200,'data'=>$data);
                  }else{
                  $arrReturn = array('flag' => 1,'msg'=>'Data not found  please try Again','error'=>400);
                  }
                echo json_encode($arrReturn); exit;
              }
        }
    //delete_my_club_registers
    private function delete_my_club_registers()
      {
        $authtoken    = $this->input->post('authtoken');
        $event_id    = $this->input->post('event_id');

        if($authtoken ==""|| $authtoken == 'null' || $event_id == "" ){
                  $arrReturn = array('flag' => 0, 'msg' => 'All  field is required','error'=>400);
                  echo json_encode($arrReturn); exit;
          }else{
            $userid = $this->getuserid($authtoken);
            //Delete  Event Register  club_event   table
            $this ->db-> where('event_id', $event_id);
            $this->db-> delete('club_event');
            //Delete  Event Member   club_user_member   table
            $this ->db-> where('event_id', $event_id);
            $this->db-> delete('club_user_member');
          //Delete  Event  Accept pairUp   club_my_pairup   table
            $this ->db-> where('event_id', $event_id);
            $this->db-> delete('club_my_pairup');
          //Delete  Event  Request pairUp   club_pair_up_request   table
            $this ->db-> where('event_id', $event_id);
            $this->db-> delete('club_pair_up_request');
              $arrReturn = array('flag' => 1,'msg'=>'your  Event successfully  Deleted ','success'=>200,'data'=>$data);
              echo json_encode($arrReturn); exit;
            }
          
      }
    

      /*cancel_drink_request Send Freind   */ 
      private function cancel_drink_request(){
        $authtoken      = $this->input->post('authtoken', true);
        $event_id      = $this->input->post('event_id');
      if($authtoken=="" || $authtoken == 'null' || $event_id ==''){  
        $arrReturn = array('flag' => 0, 'msg' => 'Authtoken  and friend_authtoken field is required','error'=>400);
          echo json_encode($arrReturn); exit;
          }else{
            $user_id = $this->getuserid($authtoken);
            $data = $this->rest_api->cancel_drink_request($event_id);
            $arrResponse = array('flag' => 1,'msg'=>'Drink request has been rejected successfully','success'=>200,'data'=>$data);
                    echo json_encode($arrResponse); exit;
          }
          $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
            echo json_encode($arrResponse); exit;
      }
  
  
      /*Cencel Request Id*/
            /*reject_friend_request Send Freind   */
      private function reject_drink_request(){
        $authtoken  = $this->input->post('authtoken', true);
        // $requestid        = $this->input->post('requestid');
          $event_id  = $this->input->post('event_id');
        if($authtoken=="" || $authtoken == 'null' || $event_id == ''){  
          $arrReturn = array('flag' => 0, 'msg' => 'Authtoken field is required','error'=>400);
            echo json_encode($arrReturn); exit;
            }else{
                $user_id = $this->getuserid($authtoken);
                $check_friend =  $this->db->get_where('club_accept_drink_request',array('friend_id'=>$user_id,'event_id'=>$event_id,'status'=>'3'))->row()->friend_id;
                if($check_friend == $user_id){
                  $arrReturn = array('flag' => 0, 'msg' => 'Friend request already Reject','error'=>400);
                  echo json_encode($arrReturn); exit;
                }else{
                      $data = $this->rest_api->reject_drink_request($user_id,$event_id); 
                    $authtokens = $this->db->get_where('club_event',array('event_id'=>$event_id))->row()->authtoken;
                      $frinddata= $this->db->get_where('club_users',array('apiKey'=>$authtokens))->result_array();
                            $os= $frinddata['0']['os'];
                            if($os =='1'){
                                $key = $frinddata['0']['gkey'];
                            }else{
                              $key   = $frinddata['0']['ikey'];
                            }   
                          $user_name = $this->general->get_single_record('club_users', 'full_name', 'id', $user_id);
                          $title = $user_name->full_name;
                          $body  = ' has reject your Drink  request';
                          $datas = array('id'=>$user_id,
                                        'name'=>$user_name,
                                        'requestid' =>$data,
                                          'user_authtoken'=>$authtoken,
                                          'type'=>'request_drink'); 
                          $this->sendPushNotificationToFCMSever($key,$title,$body,$datas);
                      $arrResponse = array('flag' => 1,'msg'=>'Drink request has been reject successfully','success'=>200,'data'=>$data);
                              echo json_encode($arrResponse); exit;
                  }
            }
            $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
              echo json_encode($arrResponse); exit;
        }
        /* accept_friend_request*/
        private function accept_drink_request(){
          $authtoken  = $this->input->post('authtoken', true);
        // $requestid        = $this->input->post('requestid');
          $event_id  = $this->input->post('event_id');
        if($authtoken=="" || $authtoken == 'null' || $event_id == ''){  
          $arrReturn = array('flag' => 0, 'msg' => 'Authtoken field is required','error'=>400);
            echo json_encode($arrReturn); exit;
            }else{
                $user_id = $this->getuserid($authtoken);
                $check_friend =  $this->db->get_where('club_accept_drink_request',array('friend_id'=>$user_id,'event_id'=>$event_id,'status'=>'1'))->row()->friend_id;
                if($check_friend == $user_id){
                  $arrReturn = array('flag' => 0, 'msg' => 'Friend request already Accepted','error'=>400);
                  echo json_encode($arrReturn); exit;
                }else{
                      $data = $this->rest_api->accept_drink_request($user_id,$event_id); 
                    $authtokens = $this->db->get_where('club_event',array('event_id'=>$event_id))->row()->authtoken;
                      $frinddata= $this->db->get_where('club_users',array('apiKey'=>$authtokens))->result_array();
                            $os= $frinddata['0']['os'];
                            if($os =='1'){
                                $key = $frinddata['0']['gkey'];
                            }else{
                              $key   = $frinddata['0']['ikey'];
                            }   
                          $user_name = $this->general->get_single_record('club_users', 'full_name', 'id', $user_id);
                          $title = $user_name->full_name;
                          $body  = ' has accept your Drink  request';
                          $datas = array('id'=>$user_id,
                                        'name'=>$user_name,
                                        'requestid' =>$data,
                                          'user_authtoken'=>$authtoken,
                                          'type'=>'request_drink'); 
                          $this->sendPushNotificationToFCMSever($key,$title,$body,$datas);
                      $arrResponse = array('flag' => 1,'msg'=>'Drink request has been Acceptd successfully','success'=>200,'data'=>$data);
                              echo json_encode($arrResponse); exit;
                  }
            }
            $arrResponse = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
              echo json_encode($arrResponse); exit;
        }

          // this Function is used to Check user  pair Up Request 
    private function check_request_drink(){
              $authtoken        = $this->input->post('authtoken');
              $eventid          = $this->input->post('event_id', true);
              $friend_authtoken          = $this->input->post('friend_authtoken', true);
              if($authtoken ==""|| $authtoken == 'null' ||  $eventid ==''|| $eventid == 'null' || $friend_authtoken==""){
                        $arrReturn = array('flag' => 0, 'msg' => 'All  field is required','error'=>400);
                        echo json_encode($arrReturn); exit;
                }else{
                  $userid = $this->getuserid($authtoken);
                  $friend_id = $this->getuserid($friend_authtoken);

                  $getdrinkrquest_type = $this->db->get_where('club_request_a_drink', array('event_id'=>$eventid))->result_array();
                if($getdrinkrquest_type['0']['drink_type']  == 'Requesting Drink'){
                    $club_mydrink_request = $this->db->get_where('club_request_a_drink', array('user_id' =>$userid,'event_id'=>$eventid))->result_array();
                     $requestcheck  = $this->db->get_where("club_accept_drink_request",array("event_id"=>$eventid,"friend_id"=>$friend_id,"send_by"=>$userid))->result_array();
                    $requestcheck_user  = $this->db->get_where("club_accept_drink_request",array("event_id"=>$eventid,"friend_id"=>$userid,"send_by"=>$friend_id))->result_array();
                    //  0=Accept-Reject   friends
                    // 1=You accepted drink request from {this.state.name}
                    // 2= You rejected drink request from {this.state.name}
                    // 3=You requested for drink to {this.state.name}
                    // 4={this.state.name} has offered you drink
                    // 5=You offered drink to {this.state.name}
                    // 6=REQUEST DRINK
                    // 7=Nothing
                    // 8={this.state.name} has accepted your drink request
                    if($club_mydrink_request['0']['user_id'] == $userid && $requestcheck['0']['status'] !='1' && $requestcheck['0']['status'] !='3'){ 
                        // Send  Request  Other  user  
                        $drink_status = '3'; 
                      }elseif($requestcheck['0']['send_by'] == $userid && $requestcheck['0']['status']=='2'){
                    $drink_status = '3';
                      }/*elseif($requestcheck_user['0']['friend_id'] == $userid && $requestcheck_user['0']['status']=='2'){
                    $drink_status = '0';
                   }*/elseif($requestcheck_user['0']['status'] =='1'){
                    $drink_status = '1';
                   }elseif($requestcheck['0']['status'] =='1'){
                    $drink_status = '8';
                   }elseif($requestcheck['0']['status'] =='3'){
                    $drink_status = '6';
                   }elseif($requestcheck_user['0']['status'] =='3'){
                    $drink_status = '2';
                   }else{
                    // show  btun Accept And  Delete   Drink Request  second user 
                    $drink_status = '0';
                  }
                  $arrReturn = array('flag' => 1,'msg'=>'Friend  Drink status','success'=>200,'drink_status'=>$drink_status);
                    echo json_encode($arrReturn); exit;
                  }elseif($getdrinkrquest_type['0']['drink_type'] =='None'){
                     $requestcheck  = $this->db->get_where("club_accept_drink_request",array("event_id"=>$eventid,"friend_id"=>$friend_id,"send_by"=>$userid))->result_array();
                    $requestcheck_user  = $this->db->get_where("club_accept_drink_request",array("event_id"=>$eventid,"friend_id"=>$userid,"send_by"=>$friend_id))->result_array();
                    //  0=Accept-Reject   friends
                    // 1=You accepted drink request from {this.state.name}
                    // 2= You rejected drink request from {this.state.name}
                    // 3=You requested for drink to {this.state.name}
                    // 4={this.state.name} has offered you drink
                    // 5=You offered drink to {this.state.name}
                    // 6=REQUEST DRINK
                    // 7=Nothing
                    // 8={this.state.name} has accepted your drink request
                    if($userid ==$getdrinkrquest_type['0']['user_id'] && empty($requestcheck)){
                      // you offer  drink to other  user  
                            $drink_status = '6';
                    }elseif($requestcheck['0']['send_by'] == $userid && $requestcheck['0']['status']=='2'){
                    $drink_status = '3';
                   }elseif($requestcheck_user['0']['friend_id'] == $userid && $requestcheck_user['0']['status']=='2'){
                    $drink_status = '0';
                   }elseif($requestcheck_user['0']['status'] =='1'){
                    $drink_status = '1';
                   }elseif($requestcheck['0']['status'] =='1'){
                    $drink_status = '8';
                   }elseif($requestcheck['0']['status'] =='3'){
                    $drink_status = '6';
                   }elseif($requestcheck_user['0']['status'] =='3'){
                    $drink_status = '2';
                   }
                    $arrReturn = array('flag' => 1,'msg'=>'Offer Drink status','success'=>200,'drink_status'=>$drink_status);
                    echo json_encode($arrReturn); exit;
                  
                  } else{
                    if($getdrinkrquest_type['0']['user_id'] == $userid){
                      // you offer  drink to other  user  
                    $drink_status = '5';
                    }else{
                      // show ohter  user  Drink Request  show 
                    $drink_status ='4';
                    }
              $arrReturn = array('flag' => 1,'msg'=>'Offer Drink status','success'=>200,'drink_status'=>$drink_status);
              echo json_encode($arrReturn); exit;
            }
          }
            $arrReturn = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
            echo json_encode($arrReturn); exit;
      }
      //  Add drink  request  inserver   //
    private function send_drinkrequest(){
      $authtoken        = $this->input->post('authtoken');
      $friend_authtoken        = $this->input->post('friend_authtoken');
    //  $drink_type        = '';
      $eventid          = $this->input->post('event_id', true);
      if($authtoken ==""|| $authtoken == 'null' ||  $eventid ==''|| $eventid == 'null' || $friend_authtoken==""){
                $arrReturn = array('flag' => 0, 'msg' => 'All  field is required','error'=>400);
                echo json_encode($arrReturn); exit;
      }else{
        $request_a_drink_id= $this->db->get_where('club_request_a_drink',array('event_id'=>$eventid))->row()->id;
        $userid = $this->getuserid($authtoken);
        $frined = $this->getuserid($friend_authtoken);
        $request_check = $this->db->get_where('club_accept_drink_request',array('friend_id'=>$frined,'send_by'=>$userid,'event_id'=>$eventid))->row()->event_id;
     //  echo $request_check;die;
        if($request_check ==$eventid){
          $data['status'] = '2';
          $this->db->where('friend_id', $frined);
          $this->db->update('club_accept_drink_request', $data);
          $arrReturn = array('flag' => 1,'msg'=>'Offer Drink  Added','success'=>200,'drink_id'=>$id);
          echo json_encode($arrReturn); exit;
        }else{
        $data = array('friend_id'=>$frined,
            'request_a_drink_id'=>$request_a_drink_id,
                      'send_by' =>$userid,
                      'event_id'=>$eventid,
                      'status'=>'2');
        $this->db->insert('club_accept_drink_request',$data);
        $id = $this->db->insert_id();
        if($id){
          $arrReturn = array('flag' => 1,'msg'=>'Offer Drink  Added','success'=>200,'drink_id'=>$id);
          echo json_encode($arrReturn); exit;
        }else{
          $arrReturn = array('flag' => 0,'msg'=>'Server Error please try Again','error'=>400);
          echo json_encode($arrReturn); exit;
        }
      }
      }
    }
  /**

  *  Send  sms  Code  
  */
    private function send_sms($phone_number,$masg,$message_thread_code){ 

  
      $url = 'http://api.msg91.com/api/sendotp.php?authkey=267797ALq6CREKIdD5c8ca99a&mobile='.$phone_number.'&message='.$masg.'&sender=611332&otp='.$message_thread_code;
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_TIMEOUT, 5);
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $data = curl_exec($ch);
      curl_close($ch);
      return $data;
    }
  /*

    *@ This  function  is   used get  user Id

    */

    private function getuserid($authtoken){
        #get user student_id from auth key
        $user = $this->general->get_single_record('club_users', 'id', 'apiKey', $authtoken);
        return $user->id;
    }

    /*** pUSH NOTIFICATION **/
    public function sendPushNotificationToFCMSever($key,$title,$body,$datas) { 
        $path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send'; 
        $fields = array(
            'registration_ids' => array($key),
            'priority' => 10,
            'notification' => array('title' => $title, 'body' => $body ,'sound'=>'Default','image'=>'Notification Image', ),
            'data' => $datas,
        );
        $headers = array(
            'Authorization:key=AAAAwtJajSA:APA91bGwBYYT8o0z646IHfs-IltGgOUZG_wPlRL9w9K_bWatUXuO9UQ69NDGsCFyXBInlDrVyfLeqQ54ZTgYNpiOXeEh8UnTJa1PJ4-ltoRr_5cnfOdE5Vi_w_4lbZpk7HLPFMoSN5Ao',
            'Content-Type:application/json'
        );  
        
        // Open connection  
        $ch = curl_init(); 
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm); 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        // Execute post   
        $result = curl_exec($ch); 
        // Close connection      
        curl_close($ch);
    // print_r($result);die;
        return $result;
    }
private function  Razorpay_check_status($payment_id,$amount){
        $url = 'https://api.razorpay.com/v1/payments/'.$payment_id.'/capture';
      /*  $key_id = 'rzp_live_xeV6MZpXDdxsnc';
         $key_secret = 'aaTcgE7Tb4gKCjQoIuXD8ijn';*/
         $key_id = 'rzp_live_xeV6MZpXDdxsnc';
         $key_secret = 'aaTcgE7Tb4gKCjQoIuXD8ijn';
        
        $fields_string = "amount=$amount";
        //cURL Request
        $ch = curl_init();
        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERPWD, $key_id.':'.$key_secret);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__).'/ca-bundle.crt');
        // Close connection      

                $result = curl_exec($ch);
                return $result;

}

}/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

