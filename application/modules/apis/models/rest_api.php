<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rest_api extends CI_Model
{
    public function __construct()
    {
            parent::__construct();
    }
    /*
    *@  This  functionn is used to   update  password
    */
    function clear_cache()
    {
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }
    /*
    *@ THis  function is used  to login   user
    */
    public function dologin(){
        #get data
        $phone_number = $this->input->post('phone_number');
        $imei         = $this->input->post('imei');
        $devicetoken  = $this->input->post('devicetoken');
        $ostype       = $this->input->post('ostype');
        $password     = $this->input->post('password');
        $query        = $this->db->select('*')
                            ->from('club_users')
                            ->where('phone_number', $phone_number)
                            ->where('password', md5($password))
                            ->limit(1)
                            ->get();
        if ($query->num_rows() > 0){
            $id = $query->row()->id;
            $image =$this->get_image_url('users',$id);
            $this->update_last_login($id, $imei, $ostype);
            #user data
            $apikey = $query->row()->apiKey;
            #update device token
            if ($ostype == 1){
                $this->db->update('club_users', array('gkey' => $devicetoken), array('id' => $id));
                } else if ($ostype == 2){
                $this->db->update('club_users', array('ikey' => $devicetoken), array('id' => $id));
            }
         } else {
            $apikey = '';
        }
        #retun array
        $arrReturn = array('apiKey' => $apikey,'status'=>$query->row()->status,'username'=>$query->row()->full_name,'userid'=>$query->row()->id,'image'=>$image,'phone_number' =>$query->row()->phone_number,);
        return $arrReturn;
    }
   /*
   * @ this function is used to  Update user   login  info
   */
    public function update_last_login($id, $imei, $ostype){
        $ip_addr      = $this->general->get_real_ipaddr();
        $this->db->update('club_users', array('last_login' => date('Y-m-d h:m:s'),'last_ip_address' =>$ip_addr, 'imei' => $imei, 'os' => $ostype), array('id' => $id));
        return $this->db->affected_rows() == 1;
    }
    /*
    * @ this function is  used to New Regisert User Account 
    */
    public function user_signup($phone_number,$dob,$full_name,$password,$gender,$food,$dance,$type_of_drink,$intrested_in,$bio,$apiKey){
          $data   = array('phone_number'   => $phone_number,
                               'dob'            => $dob,
                             'full_name'        => $full_name,
                             'password'         => md5($password),
                             'gender'           => $gender,
                             'food'             => $food,
                             'dance'             => $dance,
                             'type_of_drink'    => $type_of_drink,
                             'intrested_in'     => $intrested_in,
                             'bio'              => $bio,
                             'apiKey'           => $apiKey,
                );
            $this->db->insert('club_tem_register',$data);
            $ids =$this->db->insert_id();
            return $ids;
    }

    // Send  OTP  
    function send_otp_message($message_thread_code,$apiKey) {

        $data_message['code']        = $message_thread_code;
        $data_message['authtoken']   = $apiKey;
        $data_message['status']      = '2';
        $this->db->insert('club_verification', $data_message);
        return $message_thread_code;

    }
    // Send  OTP  member 
    function send_otp_messages($message_thread_code,$member_phone_no) {

        $data_message['code']        = $message_thread_code;
        $data_message['member_phone_no']   = $member_phone_no;
        $data_message['status']      = '2';
        $this->db->insert('club_verification', $data_message);
        return $this->db->insert_id();

    }
    /*
    * @ this function is   creat  a code  and send 
    */
    public function verification($code ='0',$authtoken='0'){
            $this->db->update('club_verification', array('status' => '1'), array('code' => $code,'authtoken'=>$authtoken));
            $this->db->update('club_tem_register', array('status' => '2'), array('apiKey' => $authtoken));

           $alldata =  $this->db->get_where('club_tem_register',array('apiKey'=>$authtoken,'status'=>'2'))->result_array();
          if($alldata){
              foreach ($alldata as $key) {
                  $data   = array('phone_number'   => $key['phone_number'],
                             'full_name'        => $key['full_name'],
                             'password'         => $key['password'],
                             'gender'           => $key['gender'],
                             'offering_a_drink' => $key['offering_a_drink'],
                             'food'             => $key['food'],
                             'type_of_drink'    => $key['type_of_drink'],
                             'intrested_in'     => $key['intrested_in'],
                             'bio'              => $key['bio'],
                              'dob'             => $key['dob'],
                              'dance'           => $key['dance'],
                             'apiKey'           => $key['apiKey'],
                );
            $this->db->insert('club_users',$data);
            $id =$this->db->insert_id();
            $primum = $this->db->get_where('club_subscriptionplanmaster',array('id'=>'1'))->result_array();
            $end = date('d-m-Y', strtotime('+1 years'));
            $data1 = array('user_id'       => $id,
                          'subscriptionplanmaster_id' => $primum['0']['id'],
                          'amount'          => $primum['0']['price'],
                          'currency'        => 'INR',
                          'status'          => 'captured',
                          'activate_status' => '1',
                          'transaction_id'  => 'free',
                          'transaction_type'=>'2',
                          'method'          => 'wallet',
                          'date'           =>$end,
                          );
                  $this->db->insert('club_subscriptionplanmaster_activate',$data1);
          }
             $this ->db-> where('apiKey', $authtoken);
             $this->db-> delete('club_tem_register');
             $this ->db-> where('authtoken', $authtoken);
             $this->db-> delete('club_verification');
       return  $id;//$this->db->affected_rows() == 1;  
      }else{
        return  'server error'; 
      }
    }

    /*
    * @ this function is  used  add  member  with  verification 
    */
    public function member_verification($code,$userid='0',$place_id,$event_id,$member_phone_number,$member_name,$member_age,$member_sex){
            $this->db->update('club_verification', array('status' => '1'), array('code' => $code,));

                  $data   = array('user_id'      => $userid,
                             'event_id'        => $event_id,
                             'place_id'         => $place_id,
                             'member_phone_number'=> $member_phone_number,
                             'member_name'      => $member_name,
                             'member_age'       => $member_age,
                             'member_sex'        => $member_sex,
                             'member_states'     => '1',
                );
            $this->db->insert('club_user_member',$data);
            $id =$this->db->insert_id();
       return  $id;//$this->db->affected_rows() == 1;  
      }
      /* Delete member  in sever    */
       public  function delete_member_info($member_register_id){
        $this->db->where('member_id',$member_register_id);
        $this->db->delete('club_user_member');
    }
    /*
        This function is  get  user profile  datel by user id
    */ 
    public function get_user_profile_by_user_id($userid){
    //  echo $userid;die();
           $alldata =  $this->db->get_where('club_users',array('id'=>$userid))->result_array();
           $image =$this->get_image_url('users',$userid);
         foreach ($alldata as $key) {
                $data   = array('phone_number'   => $key['phone_number'],
                             'full_name'        => $key['full_name'],
                             'gender'           => $key['gender'],
                             'offering_a_drink' => $key['offering_a_drink'],
                             'food'             => $key['food'],
                             'type_of_drink'    => $key['type_of_drink'],
                             'intrested_in'     => $key['intrested_in'],
                             'bio'              => $key['bio'],
                             'dob'              => $key['dob'],
                             'dance'            => $key['dance'],
                             'image'            => $image,
                             'apiKey'           => $key['apiKey']);
                 }
            return $data;
         }

        /*

            user update profile 

        */
    public function update_profile_info($userid,$dob,$full_name,$gender,$food,$dance,$type_of_drink,$intrested_in,$bio){
               $data   = array(
                             'dob'              => $dob,
                             'full_name'        => $full_name,
                             'gender'           => $gender,
                             //'offering_a_drink' => $offering_a_drink,
                             'food'             => $food,
                             'dance'             => $dance,
                             'type_of_drink'    => $type_of_drink,
                             'intrested_in'     => $intrested_in,
                             'bio'              => $bio,
                );

           $this->db->where('id', $userid);
           $this->db->update('club_users', $data);
            return $this->db->insert_id();
    }
    /*Regiser Event  Server */

    public function register_event($authtoken,$club_place_id,$date,$time,$type,$group_type,$club_name,$latitude,$longitude,$offering_a_drink,$request_a_drink,$sender_id){
      $datetime=   $date;
         $data   = array(   'authtoken'    => $authtoken,
                            'club_place_id'=> $club_place_id,
                             'date'        => $date,
                             'time'        => $time,
                             'type'        => $type,
							 'offering_a_drink'=>$offering_a_drink,
						     'user_id'      =>$sender_id,
                             'group_type'  => $group_type,
                             'datetime'    =>strtotime($datetime),
                            'club_name'    =>$club_name,
                             'latitude' =>$latitude,
                            'longitude'   =>$longitude);
                        //print_r($data);die;
    
           $this->db->insert('club_event',$data);
            $id = $this->db->insert_id();
             $data1 =array('user_id'=>$sender_id,
                            'drink_type'=>$request_a_drink,
                            'event_id' =>$id);
          $this->db->insert('club_request_a_drink',$data1);
             return $id;

    }
    /*
          Event Count  by place  Id  
    */
    public function event_count_by_place_id($authtoken,$club_place_id,$timestmp){
       // echo $authtoken,$club_place_id,$timestmp;die;
       
        $club_my_pairup = $this->db->get('club_my_pairup')->num_rows();
        //print_r($club_my_pairup);die;
     
         $this->db->where('datetime = ',strtotime($timestmp));
        $this->db->or_where('datetime > ',strtotime($timestmp));
        $this->db->where('authtoken != ',$authtoken);
        $this->db->where('club_place_id = ', $club_place_id);
        $this->db->group_by('club_event.event_id');
        $this->db->order_by("club_event.event_id", "desc");
       $this->db->from('club_event');
      /* if(!empty($club_my_pairup)){
        $this->db->join('club_my_pairup', 'club_my_pairup.event_id != club_event.event_id');
       }*/
   $num_rows=$this->db->get()->num_rows();
    return $num_rows;
    }
     /*
          Event Count users 
    */
    public function event_users_by_place_id($authtoken,$club_place_id,$timestmp,$userid){
        $club_my_pairup = $this->db->get('club_my_pairup')->num_rows();
        $this->db->select('club_event.club_place_id,club_event.user_id,club_event.offering_a_drink,club_event.request_a_drink,club_users.apiKey,club_users.apiKey,club_users.id as user_id,club_users.gender,club_users.full_name,club_users.id,club_users.phone_number,club_event.authtoken,club_event.date,club_event.time,club_event.event_id,club_event.group_type');
        $this->db->from('club_event');
        $this->db->join('club_users', 'club_users.apiKey = club_event.authtoken');
      if(!empty($club_my_pairup)){
        $this->db->join('club_my_pairup', 'club_my_pairup.event_id != club_event.event_id');
      }
        
      //  $this->db->distinct();
        $this->db->where('club_event.datetime =',strtotime($timestmp));
        $this->db->or_where('club_event.datetime >',strtotime($timestmp));
        $this->db->where('club_event.authtoken !=',$authtoken);
       // $this->db->or_where('club_my_pairup.event_id !=',$userid);
        $this->db->where('club_event.club_place_id =', $club_place_id);
        $this->db->group_by('club_event.event_id');
        $this->db->order_by("club_event.event_id", "desc");
     $query = $this->db->get()->result_array();
    // print_r($query);die();
     foreach ($query as $key) {
          $image =$this->get_image_url('users',$key['user_id']);
          $user_member = $this->db->get_where('club_user_member',array('event_id' =>$eventid,'user_id' =>$key['user_id'] ,'member_states'=>'1'))->result_array();
          if(is_array($user_member)&& count($user_member)>0){
          foreach($user_member as $val){
              $member_id  = $this->db->get_where('club_users',array('phone_number'=>$val['member_phone_number']))->row()->id;
              $images  = $this->get_image_url('users',$member_id); 
              
              $get_members[] = array("member_id"    =>$val['member_id'],
                              "user_id"      =>$val['user_id'],
                              "event_id"      =>$val['event_id'],
                              "place_id"      =>$val['place_id'],
                              "member_phone_number"=>$val['member_phone_number'],
                              "member_states"=>$val['member_states'],
                              "member_name"=>$val['member_name'],
                              "member_age"=>$val['member_age'],
                              "member_sex" =>$val['member_sex'],
                              'images' =>$images);
           }
          }else{
              $get_members =$user_member;
          } 
        $data1[] = array(
            'event_id' => $key['event_id'],
            'group_type' => $key['group_type'],
            'club_place_id' => $key['club_place_id'],
            'offering_a_drink' => $key['offering_a_drink'],
            'request_a_drink' => $key['request_a_drink'],
            'apiKey' => $key['apiKey'],
            'gender' =>  $key['gender'],
            'full_name' => $key['full_name'],
            'phone_number' => $key['phone_number'],
            'authtoken' => $key['authtoken'],
            'date' => $key['date'],
            'time' => $key['time'],
            'mambers'  => array_merge($get_members),
            'image'   =>$image
       );
     }
    return $data1;
    }
    /*
    Get  User  Member  By  Event  Id 
    */
    public function event_usersmember_by_event_id($eventid){
        $user_member = $this->db->order_by("member_id", "desc")->get_where('club_user_member',array('event_id' =>$eventid ,'member_states'=>'1'))->result_array();
        foreach ($user_member as $value) {
            $userid = $this->db->get_where('club_users',array('phone_number'=>$value['member_phone_number']))->row()->id;
            $image =$this->get_image_url('users',$userid);
            $data[] = array('member_id'   =>$value['member_id'],
                'member_phone_number' =>$value['member_phone_number'],
                'member_name'         =>$value['member_name'],
                'member_age'          =>$value['member_age'],
                'member_sex'          =>$value['member_sex'],
                 'image'              => $image);
            # code...
        }
         return $data;
    }
  /*Get  Event  all  */

      public function getall_event_by_user_id($authtoken,$type,$timestmp){
        $data1[] ='0';
        $this->db->select('club_event.club_place_id,club_event.event_id,club_event.group_type,club_users.apiKey,club_users.id,club_users.gender,club_users.full_name,club_users.id,club_users.phone_number,club_event.authtoken,club_event.date,club_event.time,club_event.event_id');
        $this->db->from('club_event');
        $this->db->join('club_users', 'club_users.apiKey = club_event.authtoken');
        if($type =='past'){
           $this->db->where("club_event.datetime < ",strtotime($timestmp));
           $this->db->where("club_event.datetime !=",strtotime($timestmp));
          }elseif($type =='future'){
            $this->db->where('club_event.datetime',strtotime($timestmp));
            $this->db->or_where('club_event.datetime >',strtotime($timestmp));
           
          }
          $this->db->where('club_event.authtoken =',$authtoken);
          $this->db->order_by("club_event.event_id", "desc");
     $query = $this->db->get()->result_array();
     foreach ($query as $key) {
        $user_member = $this->db->get_where('club_user_member',array('event_id' =>$key['event_id'],'user_id' =>$key['id'] ,'member_states'=>'1'))->result_array();
        if(is_array($user_member)&& count($user_member)>0){
        foreach($user_member as $val){
            $member_id  = $this->db->get_where('club_users',array('phone_number'=>$val['member_phone_number']))->row()->id;
            $images  = $this->get_image_url('users',$member_id); 
            
            $get_members[] = array("member_id"    =>$val['member_id'],
                            "user_id"      =>$val['user_id'],
                            "event_id"      =>$val['event_id'],
                            "place_id"      =>$val['place_id'],
                            "member_phone_number"=>$val['member_phone_number'],
                            "member_states"=>$val['member_states'],
                            "member_name"=>$val['member_name'],
                            "member_age"=>$val['member_age'],
                            "member_sex" =>$val['member_sex'],
                            'images' =>$images);
         }
        }else{
            $get_members =$user_member;
        } 
        $data1[] = array(
            'event_id' => $key['event_id'],
            'club_place_id' => $key['club_place_id'],
            'group_type'   => $key['group_type'],
            'apiKey' => $key['apiKey'],
            'gender' =>  $key['gender'],
            'full_name' => $key['full_name'],
            'phone_number' => $key['phone_number'],
            'authtoken' => $key['authtoken'],
            'date' => $key['date'],
            'timestmp'=>$timestmp,
            'time' => $key['time'],
            'mambers'  => array_merge($get_members),
            'image'   =>$image
       );
    
     }
  //   print_r($data1);die;
      return $data1;
 }

    /*
 User  Profile  View 

    */
     /*
        This function is  get  user profile  datel by user id
    */ 
    public function view_user_profile_by_user_id($userid,$friend_id,$authtoken,$eventid,$friend_authtoken){
        $alldata =  $this->db->get_where('club_users',array('id'=>$friend_id))->result_array();
         $image =$this->get_image_url('users',$friend_id);
         foreach ($alldata as $key) {
           $request_friend = $this->db->get_where('club_friend_request', array('user_id' =>$authtoken,'friend'=>$friend_id))->row()->friend;
           $request_friends = $this->db->get_where('club_friend_request', array('user_id' =>$friend_authtoken,'friend'=>$userid))->row()->friend;
           $accept_request = $this->db->get_where('club_my_friends', array('user_id' =>$userid,'friend'=>$friend_id))->row()->status;
           if(!empty($request_friend)){
               $status = '2';
           }elseif(!empty($request_friends)){
                 $status = '4';
           }
           elseif(!empty($accept_request)){
                if($accept_request == '1'){
                   $status ='1';
               }else{
                $status ='3';
               }
           }else{
             $status = '0';
           }if($key['os'] =='1'){
            $device_token = $key['gkey'];
           }else{
            $device_token =$key['ikey'];
           }
           $user_member ='0';
           $user_member = $this->db->get_where('club_user_member',array('event_id' =>$eventid,'user_id' =>$friend_id ,'member_states'=>'1'))->result_array();
          
         foreach($user_member as $val){
            $member_id  = $this->db->get_where('club_users',array('phone_number'=>$val['member_phone_number']))->row()->id;
            $images  = $this->get_image_url('users',$member_id); 
            $get_members[] = array("member_id"    =>$val['member_id'],
                            "user_id"      =>$val['user_id'],
                            "event_id"      =>$val['event_id'],
                            "place_id"      =>$val['place_id'],
                            "member_phone_number"=>$val['member_phone_number'],
                            "member_states"=>$val['member_states'],
                            "member_name"=>$val['member_name'],
                            "member_age"=>$val['member_age'],
                            "member_sex" =>$val['member_sex'],
                            'images' =>$images);
         }
           $drink = $this->db->get_where('club_event', array('event_id' =>$eventid,'authtoken'=>$friend_authtoken))->result_array();
            $data   = array('phone_number'      => $key['phone_number'],
                             'full_name'        => $key['full_name'],
                             'gender'           => $key['gender'],
                             'dob'              => $key['dob'],
                             'request_a_drink' => $drink['0']['request_a_drink'],
                             'offering_a_drink' => $drink['0']['offering_a_drink'],
                             'food'             => $key['food'],
                             'dance'            => $key['dance'],
                             'type_of_drink'    => $key['type_of_drink'],
                             'intrested_in'     => $key['intrested_in'],
                             'bio'              => $key['bio'],
                             'user_authtoken'   =>$friend_authtoken,
                             'user_id'          =>$friend_id,
                             'friend_status'    =>$status,
                             'image'            => $image,
                             'mambers'          => array_merge($get_members),
                             'device_token'     =>$device_token);
                 }
            return $data;
         }

         /*This function is used to send Request  New Friend*/
         public function requet_send_data($authtoken,$friend_id){
            $data   = array('user_id' => $authtoken,
                            'friend'  => $friend_id);
            $this->db->insert('club_friend_request',$data);
            return $this->db->insert_id();

         }
         public function received_firend_request($user_id){
             $this->db->select('club_friend_request.id as requestid,club_friend_request.user_id,club_users.apiKey,club_users.gender,club_users.full_name,club_users.dob,club_users.id,club_users.phone_number');
                $this->db->from('club_friend_request');
                $this->db->join('club_users', 'club_users.apiKey = club_friend_request.user_id');
                $this->db->where('club_friend_request.friend =',$user_id);
                $this->db->order_by("club_friend_request.id", "DESC");
             $query = $this->db->get()->result_array();
            // print_r($query);die();
             foreach ($query as $key) {
                  $image =$this->get_image_url('users',$key['id']);
                $user_member ='0';
                $data1[] = array(
                    'request_id'   =>  $key['requestid'],
                    'friend_authtoken'    => $key['apiKey'],
                    'gender'       =>  $key['gender'],
                    'full_name'    => $key['full_name'],
                    'phone_number' => $key['phone_number'],
                    'dob'          => $key['dob'],
                    'image'        =>$image);
             }
                return $data1;
        }

        /*send_firend_request_view*/

         public function send_firend_request_view($authtoken){
             $this->db->select('club_friend_request.id as requestid,club_friend_request.user_id,club_users.apiKey,club_users.gender,club_users.full_name,club_users.dob,club_users.id,club_users.phone_number');
                $this->db->from('club_friend_request');
                $this->db->join('club_users', 'club_users.id = club_friend_request.friend');
                $this->db->where('club_friend_request.user_id =',$authtoken);
                $this->db->order_by("club_friend_request.id", "DESC");
             $query = $this->db->get()->result_array();
            // print_r($query);die();
             foreach ($query as $key) {
                  $image =$this->get_image_url('users',$key['id']);
                $user_member ='0';
                $data1[] = array(
                    'request_id'   =>  $key['requestid'],
                    'friend_authtoken'    => $key['apiKey'],
                    'gender'       =>  $key['gender'],
                    'full_name'    => $key['full_name'],
                    'phone_number' => $key['phone_number'],
                    'dob'          => $key['dob'],
                    'image'        =>$image);
             }
                return $data1;
        }

         /*reject_friend_request*/
         public function reject_friend_request($authtoken,$friend_id){
            $this->db->where('user_id',$authtoken);
             $this->db->where('friend',$friend_id);
             $this->db->delete('club_friend_request');

         }

         /*cancel_friend_request*/
        public function cancel_friend_request($friend_authtoken,$user_id){
            $this->db->where('user_id',$friend_authtoken);
             $this->db->where('friend',$user_id);
             $this->db->delete('club_friend_request');

         }

         /*accept_friend_request*/
         public function accept_friend_request($user_id,$friend_id,$friend_authtoken){
             $data   = array('user_id' => $user_id,
                            'friend'   => $friend_id,
                             'status'  =>'1');
            $this->db->insert('club_my_friends',$data);

             $data1   = array('user_id' => $friend_id,
                            'friend'  => $user_id,
                             'status'  =>'1');
            $this->db->insert('club_my_friends',$data1);
            $id = $this->db->insert_id();
             $this->db->where('user_id',$friend_authtoken);
             $this->db->where('friend',$user_id);
             $this->db->delete('club_friend_request');
            return $id;
         }

         /*this funtion  is usrd  my_friend_list*/

         public function my_friend_list($authtoken){
             $this->db->select('club_my_friends.friend_id,club_my_friends.friend,club_users.gender,club_users.apiKey,club_users.full_name,club_users.dob,club_users.apiKey,club_users.id,club_users.phone_number');
                $this->db->from('club_my_friends');
                $this->db->join('club_users', 'club_users.id = club_my_friends.friend');
                $this->db->where('club_my_friends.user_id =',$authtoken);
                 $this->db->where('club_my_friends.status','1');
                 $this->db->order_by("club_my_friends.friend_id", "DESC");
             $query = $this->db->get()->result_array();
            // print_r($query);die();
             foreach ($query as $key) {
                  $image =$this->get_image_url('users',$key['friend']);
                $user_member ='0';
                $data1[] = array(
                    'request_id'    => $key['friend_id'],
                    'friend_id'    => $key['friend'],
                    'gender'       =>  $key['gender'],
                    'friend_authtoken'    => $key['apiKey'],
                    'full_name'    => $key['full_name'],
                    'phone_number' => $key['phone_number'],
                    'user_authtoken'=> $key['apiKey'],
                    'dob'          => $key['dob'],
                    'image'        =>$image);
             }
                return $data1;
        }
public function list_block_friend($authtoken){
             $this->db->select('club_my_friends.friend_id,club_my_friends.friend,club_users.gender,club_users.apiKey,club_users.full_name,club_users.dob,club_users.id,club_users.phone_number');
                $this->db->from('club_my_friends');
                $this->db->join('club_users', 'club_users.id = club_my_friends.friend');
                $this->db->where('club_my_friends.user_id =',$authtoken);
                 $this->db->where('club_my_friends.status','2');
                 $this->db->order_by("club_my_friends.friend_id", "DESC");
             $query = $this->db->get()->result_array();
            // print_r($query);die();
             foreach ($query as $key) {
                  $image =$this->get_image_url('users',$key['friend']);
                $user_member ='0';
                $data1[] = array(
                    'request_id'    => $key['friend_id'],
                    'friend_id'    => $key['friend'],
                    'friend_authtoken' => $key['apiKey'],
                    'gender'       =>  $key['gender'],
                    'full_name'    => $key['full_name'],
                    'phone_number' => $key['phone_number'],
                    'dob'          => $key['dob'],
                    'image'        =>$image);
             }
                return $data1;
        }


    public function block_my_friend($user_id,$friend_id){
          $data  = array('status' => '2');
           $this->db->where('user_id', $user_id);
           $this->db->where('friend', $friend_id);
           $this->db->update('club_my_friends', $data);
           $datas  = array('status' => '3');
          $this->db->where('user_id', $friend_id);
           $this->db->where('friend', $user_id);
           $this->db->update('club_my_friends', $datas);

            return $this->db->insert_id();
    }
    
/*Unblock  */
     public function un_block_my_friend($user_id,$friend_id){
          $data  = array('status' => '1', );
      
           $this->db->where('user_id',$user_id);
           $this->db->where('friend',$friend_id);
           $this->db->update('club_my_friends', $data);

          $this->db->where('user_id', $friend_id);
           $this->db->where('friend', $user_id);
           $this->db->update('club_my_friends', $data);

            return $this->db->insert_id();
    }
/*Delete  My Friend In freind  list */
     public function delete_my_friend($user_id,$friend_id){
            $this->db->where('user_id',$friend_id);
             $this->db->where('friend',$user_id);
             $this->db->delete('club_my_friends');

              $this->db->where('user_id',$user_id);
             $this->db->where('friend',$friend_id);
             $this->db->delete('club_my_friends');
        }

        /*Dellete My pairup  Friend Request  */
        
         public function delete_my_pairUp_friend($user_id,$friend_id,$event_id){
            $this->db->where('user_id',$friend_id);
             $this->db->where('friend_id',$user_id);
             $this->db->where('event_id',$event_id);
             $this->db->delete('club_my_pairup');

              $this->db->where('user_id',$user_id);
              $this->db->where('event_id',$event_id);
             $this->db->where('friend_id',$friend_id);
             $this->db->delete('club_my_pairup');
        }

    /*Use This  Functon send_pairUp_request_view*/
     public function send_pairUp_request_view($user_id){
             $this->db->select('club_pair_up_request.pair_up_request_id  as requestid,club_pair_up_request.event_id,club_pair_up_request.user_id,club_users.apiKey,club_users.gender,club_users.full_name,club_users.dob,club_users.id,club_users.phone_number,club_users.apiKey');
                $this->db->from('club_pair_up_request');
                $this->db->join('club_users', 'club_users.id = club_pair_up_request.friend_id');
                $this->db->where('club_pair_up_request.user_id =',$user_id);
                $this->db->order_by("club_pair_up_request.pair_up_request_id", "DESC");
             $query = $this->db->get()->result_array();
            // print_r($query);die();
             foreach ($query as $key) {
                 $dataEvent =$this->db->get_where('club_event',array('event_id' =>$key['event_id']))->result_array();
                  $image =$this->get_image_url('users',$key['id']);
                $user_member ='0';
                $data1[] = array(
                    'request_id'   =>  $key['requestid'],
                    'friend_authtoken' => $key['apiKey'],
                    'event_id'     =>  $key['event_id'],
                    'gender'       =>  $key['gender'],
                    'full_name'    => $key['full_name'],
                    'phone_number' => $key['phone_number'],
                    'dob'          => $key['dob'],
                    'club_place_id'=>  $dataEvent['0']['club_place_id'],
                    'date'        => $dataEvent['0']['date'],
                    'time'        => $dataEvent['0']['time'],
                    'image'        =>$image);
             }
                return $data1;
        }


        /*this Function is  used to  Send pairUp Request  To server*/
    public function pairUprequet_send_data($user_id,$friend_id,$event_id){
            $data   = array('user_id' => $user_id,
                            'friend_id'  => $friend_id,
                            'event_id'  =>$event_id,);
            $this->db->insert('club_pair_up_request',$data);
            return $this->db->insert_id();

    }
    /*This  Function to used  Get  pairUp Request  data   in Server  key['*/
    
    public function received_pairUp_request_view($user_id){
             $this->db->select('club_pair_up_request.pair_up_request_id as requestid,club_pair_up_request.event_id,club_pair_up_request.user_id,club_users.apiKey,club_users.gender,club_users.full_name,club_users.dob,club_users.id,club_users.phone_number');
                $this->db->from('club_pair_up_request');
                $this->db->join('club_users', 'club_users.id = club_pair_up_request.user_id');
                $this->db->where('club_pair_up_request.friend_id =',$user_id);
                $this->db->order_by("club_pair_up_request.pair_up_request_id", "DESC");
             $query = $this->db->get()->result_array();
            // print_r($query);die();
             foreach ($query as $key) {
                $dataEvent =$this->db->get_where('club_event',array('event_id' =>$key['event_id']))->result_array();
                  $image =$this->get_image_url('users',$key['id']);
                $user_member ='0';
                $data1[] = array(
                    'request_id'   =>  $key['requestid'],
                    'friend_authtoken'    => $key['apiKey'],
                    'gender'       =>  $key['gender'],
                    'event_id'     =>$key['event_id'],
                    'full_name'    => $key['full_name'],
                    'phone_number' => $key['phone_number'],
                    'dob'          => $key['dob'],
                    'club_place_id'=>  $dataEvent['0']['club_place_id'],
                    'date'        => $dataEvent['0']['date'],
                    'time'        => $dataEvent['0']['time'],
                    'image'        =>$image);
             }
                return $data1;
        }
    /*This  Functin  is Used To  Reject pairUp Request reject_pairUp_request*/
    public function reject_pairUp_request($user_id,$friend_id,$event_id){
             $this->db->where('user_id',$friend_id);
             $this->db->where('friend_id',$user_id);
              $this->db->where('event_id',$event_id);
             $this->db->delete('club_pair_up_request');

     }

       /*This  Functin  is Used To  Reject pairUp Request cancel_pairUp_request*/
    public function cancel_pairUp_request($user_id,$friend_id,$event_id){
              $this->db->where('user_id',$user_id);
              $this->db->where('friend_id',$friend_id);
              $this->db->where('event_id',$event_id);
              $this->db->delete('club_pair_up_request');

     }
     /* This  Function is used to Accesspt pair Up Request  */
     public function accept_pair_up_request($user_id,$friend_id,$event_id){
             $data   = array('user_id' => $user_id,
                            'friend_id'   => $friend_id,
                            'event_id'   =>$event_id,
                             'status'  =>'1');
            $this->db->insert('club_my_pairup',$data);

             $data1   = array('user_id' => $friend_id,
                            'friend_id'  => $user_id,
                            'event_id'   =>$event_id,
                             'status'  =>'1');
            $this->db->insert('club_my_pairup',$data1);
            $id = $this->db->insert_id();
             $this->db->where('user_id',$friend_id);
             $this->db->where('friend_id',$user_id);
             $this->db->where('event_id',$event_id);
             $this->db->delete('club_pair_up_request');
            return $id;
         }
    /* this function is used to Get all pair Up Request  in Server */
    
     public function my_pairUpfriend_list($user_id ='0'){
             $this->db->select('club_my_pairup.pairup_id,club_my_pairup.friend_id,club_my_pairup.event_id,club_users.gender,club_users.full_name,club_users.dob,club_users.id,club_users.phone_number,club_users.apiKey');
                $this->db->from('club_my_pairup');
                $this->db->join('club_users', 'club_users.id = club_my_pairup.friend_id');
                $this->db->where('club_my_pairup.user_id =',$user_id);
                 $this->db->where('club_my_pairup.status','1');
                 $this->db->order_by("club_my_pairup.pairup_id", "DESC");
             $query = $this->db->get()->result_array();
            // print_r($query);die();
             foreach ($query as $key) {
                   $dataEvent =$this->db->order_by("event_id", "desc")->get_where('club_event',array('event_id' =>$key['event_id']))->result_array();
                  $image =$this->get_image_url('users',$key['friend']);
                $user_member ='0';
                $data1[] = array(
                  'request_id'     =>  $key['requestid'],
                  'friend_authtoken' => $key['apiKey'],
                  'gender'       =>  $key['gender'],
                  'event_id'     =>$key['event_id'],
                  'full_name'    => $key['full_name'],
                  'phone_number' => $key['phone_number'],
                  'dob'          => $key['dob'],
                  'club_place_id'=>  $dataEvent['0']['club_place_id'],
                  'date'        => $dataEvent['0']['date'],
                  'time'        => $dataEvent['0']['time'],
                  'image'        =>$image);
             }
                return $data1;
        }
        public function getSubscriptionPlanlist_monthly(){
            return $this->db->get('club_monthly_subscriptionplanmaster')->result_array();
        }
        //  this function is used to insert data  in   server  
        public function getSubscriptionPlanlist_activates($userid,$subscriptionplanmaster_id,$currency,$amount,$status,$transaction_id,$method,$transaction_type){
         if($transaction_type =='1'){
            $end = date('d-m-Y', strtotime('+1 month'));
         }elseif($transaction_type =='2'){
            $end = date('d-m-Y', strtotime('+1 years'));
         }else{
             $end = '0';
         }
   
            $this->db->update('club_subscriptionplanmaster_activate', array('activate_status' =>'3'), array('user_id' => $userid));
            $data   = array(   'user_id'          => $userid,
                                'subscriptionplanmaster_id'=> $subscriptionplanmaster_id,
                                'amount'          => $amount,
                                'currency'        => $currency,
                                'status'          => $status,
                                'transaction_id'  => $transaction_id,
                                'method'          =>$method,
                                'date'           =>$end,
                            'transaction_type' =>$transaction_type);
                    $this->db->insert('club_subscriptionplanmaster_activate',$data);
                    $id = $this->db->insert_id();
                    return $id;
        }
  /* send_report this function is  used to  Send  Report  Admin*/
   public function send_report($user_id,$friend,$title,$msg){
         $data1   = array('user_id'     => $user_id,
                            'friend_id' => $friend,
                            'title'    =>$title,
                             'msg'     =>$msg);
            $this->db->insert('club_reports',$data1);
           return  $this->db->insert_id();
   }
   public function club_premium_user_use_insert($userid,$friend,$premium_id,$transaction_id){
            $data   = array( 'user_id'        => $userid,
                            'friend_id'       => $friend,
                            'premium_id'      =>$premium_id,
                            'transaction_id'  =>$transaction_id);
        $this->db->insert('club_premium_user_use',$data);
        $id = $this->db->insert_id();
        return $id;
   }
   public function club_premium_user_call_insert($userid,$friend,$premium_id,$transaction_id){
        $data   = array( 'user_id'        => $userid,
                        'friend_id'       => $friend,
                        'premium_id'      =>$premium_id,
                        'transaction_id'  =>$transaction_id);
    $this->db->insert('club_premium_user_call_insert',$data);
    $id = $this->db->insert_id();
    return $id;
    }
   public function user_radios_sating($userid,$notification_status,$radios_range){
    $radiosid =$this->db->get_where('club_user_update_location',array('user_id' =>$userid))->row()->user_location_id;
    if(!empty($radiosid)){
        if($radios_range){
            $data['radios_range']           = $radios_range;
        }
        $data['notification_status']           = $notification_status;
        $this->db->where('user_location_id', $radiosid);
        $this->db->update('club_user_update_location', $data);

        $id =  '1';
    }else{
        $data   = array( 'user_id'        => $userid,
                        'notification_status'       => $notification_status,
                        'radios_range'      =>$radios_range);
    $this->db->insert('club_user_update_location',$data);
    $id = $this->db->insert_id();
    }
     
  return $id;
}    
 public function  user_update_location($userid,$lat,$long){
    $radiosid =$this->db->get_where('club_user_update_location',array('user_id' =>$userid))->row()->user_location_id;
    if(!empty($radiosid)){
        $this->load->helper('date');
        $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
        $date1 = $date->format('d-m-Y');
        $time = $date->format(' H:i'); 
       // 2019-03-17 17:28:19
        $timestmp =  $date1.$time;
        $data['lat']       = $lat;
        $data['long']      = $long;
        $data['update_on'] = $timestmp;
        $this->db->where('user_location_id', $radiosid);
        $this->db->update('club_user_update_location', $data);
        $id =  '1';
    }else{
        $data   = array( 'user_id'  => $userid,
                        'lat'       => $lat,
                        'long'      =>$long);
    $this->db->insert('club_user_update_location',$data);
    $id = $this->db->insert_id();
    }
  return $id;
   }

   public function check_pairup($user_id,$friend_id,$event_id){
      $event_id = $this->db->get_where('club_my_pairup',array('user_id'=>$user_id,'friend_id'=>$friend_id,'event_id'=>$event_id))->row()->pairup_id;

return $event_id;
   }
  //  This  function is used to get  all  Subscription Plan list_post
  public function getSubscriptionPlanlist_post(){
        return $this->db->get('club_subscriptionplanmaster')->result_array();
  }
  public function user_radios_sating_view($userid){
    $query = $this->db->get_where('club_user_update_location',array('user_id'=>$userid))->result_array();
            foreach($query as  $val){
                $data[] =array('notification_status'=>$val['notification_status'],
                            'radios_range'=>$val['radios_range']);
                            }
             return $data;
        }
// this  function  is used  to get  all  broad cast  value  
public function received_broadcast_view($user_id){
    $this->db->select('club_broadcasts.id,club_broadcasts.event_id,club_broadcasts.date,club_broadcasts.time,
    club_broadcasts.place_id,club_broadcasts.sender_id,
                       club_users.apiKey,club_users.gender,club_users.full_name,club_users.dob,club_users.id,
                       club_users.phone_number');
       $this->db->from('club_broadcasts');
       $this->db->join('club_users', 'club_users.id = club_broadcasts.sender_id');
       $this->db->where('club_broadcasts.receiver_id',$user_id);
       $this->db->order_by("club_broadcasts.id", "DESC");
    $query = $this->db->get()->result_array();
   // print_r($query);die();
    foreach ($query as $key){
        $user_member ='0';
        $user_member = $this->db->get_where('club_user_member',array('event_id' =>$eventid,'user_id' =>$friend_id ,'member_states'=>'1'))->result_array();
          
        foreach($user_member as $val){
           $member_id  = $this->db->get_where('club_users',array('phone_number'=>$val['member_phone_number']))->row()->id;
           $images  = $this->get_image_url('users',$member_id); 
           $get_members[] = array("member_id"    =>$val['member_id'],
                           "user_id"      =>$val['user_id'],
                           "event_id"      =>$val['event_id'],
                           "place_id"      =>$val['place_id'],
                           "member_phone_number"=>$val['member_phone_number'],
                           "member_states"=>$val['member_states'],
                           "member_name"=>$val['member_name'],
                           "member_age"=>$val['member_age'],
                           "member_sex" =>$val['member_sex'],
                           'images' =>$images);
        }
         $image =$this->get_image_url('users',$key['id']);
      $group_type =$this->db->get_where('club_event',array('event_id'=>$key['event_id']))->row()->group_type;
       $data1[] = array(
           'broadcast_id'   =>  $key['id'],
            'event_id'       => $key['event_id'],
            'club_place_id' => $key['place_id'],
            'date'          => $key['date'],
            'time'          => $key['time'],
           'mambers'       => array_merge($get_members),
           'friend_authtoken' => $key['apiKey'],
           'gender'       =>  $key['gender'],
           'full_name'    => $key['full_name'],
           'phone_number' => $key['phone_number'],
           'dob'          => $key['dob'],
           'group_type'   => $group_type,
           'image'        =>$image);
    }
       return $data1;
}
// club_contact_us
    public function contact_us($userid,$full_name,$email,$phone_number,$msg){
            $data = array( 'user_id'     => $userid,
                        'full_name'   => $full_name,
                        'email'       =>$email,
                        'phone_number'=> $phone_number,
                        'msg'         =>$msg,);
        $this->db->insert('club_contact_us',$data);
        return $this->db->insert_id();
    }


    

         /*cancel_drink_request*/
        public function cancel_drink_request($event_id){
             $this->db->where('event_id',$event_id);
             $this->db->delete('club_request_a_drink');
             //Delete  Accepted
             $this->db->where('event_id',$event_id);
             $this->db->delete('club_accept_drink_request');

         }

         /*accept_friend_request*/
         public function accept_drink_request($user_id,$event_id){
            // echo $user_id;die;
            $friend_id = $this->db->get_where('club_accept_drink_request',array('friend_id'=>$user_id,'event_id'=>$event_id))->row()->friend_id;
             if($friend_id == $user_id){
                $data['status'] = '1';
                $this->db->where('friend_id', $user_id);
                $this->db->update('club_accept_drink_request', $data);
                $id ="1";
             }else{
            $request_id = $this->db->get_where('club_request_a_drink',array('event_id'=>$event_id))->result_array();
        //    echo $request_id['0']['user_id'];die;
             $data   = array('friend_id'        => $user_id,
                            'request_a_drink_id'=> $request_id['0']['id'],
                            'send_by'           => $request_id['0']['user_id'],
                             'event_id'         =>$event_id,
                              'status'          =>'1');
            $this->db->insert('club_accept_drink_request',$data);
            $id = $this->db->insert_id();
             }
            return $id;
         }
       /*  reject_drink_request*/
         public function reject_drink_request($user_id,$event_id){
            $friend_id = $this->db->get_where('club_accept_drink_request',array('friend_id'=>$user_id,'event_id'=>$event_id))->row()->friend_id;
            if($friend_id == $user_id){
               $data['status'] = '3';
               $this->db->where('friend_id', $user_id);
               $this->db->update('club_accept_drink_request', $data);
               $id ="1";
            }else{
            $request_id = $this->db->get_where('club_request_a_drink',array('event_id'=>$event_id))->row()->id;
             $data   = array('friend_id'        => $user_id,
                            'request_a_drink_id'=> $request_id,
                             'event_id'         =>$event_id,
                              'status'          =>'3');
            $this->db->insert('club_accept_drink_request',$data);
            $id = $this->db->insert_id();
            }
            return $id;
         }
         //   $this->db->insert('club_broadcast',$dat)
       public function insert($sender_id,$receiver_id,$club_name,$event_id,$place_id,$date,$time){
        $dat = array( 'sender_id'   => '2',//$sender_id,
        'receiver_id'=>$val['user_id'],
        'club_name'  =>'2',//$club_name,
        'event_id'   =>'30',//$club_id,
        'place_id'   =>'4545456',//$club_place_id,
        'date'       =>'sdfsdf',//$date,
        'time'       =>'sdfsdf',//$time,
        );

        $this->db->insert('club_broadcasts',$dat);
        return $this->db->insert_id();
       }

/******

    *

    *

    * This  function used to   view profile pic 

    */
    public    function get_image_url($type = '' , $id = '')

    {
        if(file_exists('upload_files/'.$type.'/'.$id.'.jpg'))
            $image_url  =   base_url().'upload_files/'.$type.'/'.$id.'.jpg';
        else
            $image_url  =   base_url().'upload_files/user.png';
        return $image_url;

    }

    
}//end


