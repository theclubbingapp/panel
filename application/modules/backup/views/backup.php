
<style type="text/css">
.h_title{
background: url(assets/images/bg_gallery.jpg) repeat-x;
color: #bebebe;
cursor: pointer;
font-size: 14px;
font-weight: normal;
height: 22px;
padding: 7px 0 0 15px;
text-shadow: #0E0E0E 0px 1px 1px;
}

.message_box {
margin-top: 10px;
}

.error, .success {
/*padding: 20px 20px 20px 40px;*/
max-width: 525px;
margin: auto;
padding: 10px 10px 10px 45px;
margin-bottom: 5px;
border-style: solid;
border-width: 1px;
background-position: 10px 10px;
background-repeat: no-repeat;
}

.error {
background-color: #f5dfdf;
background-image: url(assets/images/error.png);
border-color: #ce9e9e;
}

.success {
background-color: #e8f5df;
background-image: url(assets/images/success.png);
border-color: #9ece9e;
}
</style>
</head>
<body>
<div class="h_title">
Get Site and Database Backup
</div>
<div class="message_box">
<?php
if (isset($success) && strlen($success)) {
echo '<div class="success">';
echo '<p>' . $success . '</p>';
echo '</div>';
}

if (isset($errors) && strlen($errors)) {
echo '<div class="error">';
echo '<p>' . $errors . '</p>';
echo '</div>';
}

if (validation_errors()) {
echo validation_errors('<div class="error">', '</div>');
}
?>
</div>
<?php
$back_url = $this->uri->uri_string();
$key = 'referrer_url_key';
$this->session->set_flashdata($key, $back_url);
?>
<?php echo form_open(base_url() . 'backup' , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>
<fieldset>
<section>

        <div class="form-group ">
            <label for="backup_type" class="control-label col-lg-3" required>Backup Type</label>
            <div class="col-lg-6">
                <select class="form-control select2" name="backup_type" id="backup_type">
                <option value="" selected disabled>Backup Type</option>
                <option value="1" <?php echo (isset($success) && strlen($success) ? '' : (set_value('backup_type') == '1' ? 'selected' : '')) ?>>DB Backup</option>
                    <option value="2" <?php echo (isset($success) && strlen($success) ? '' : (set_value('backup_type') == '2' ? 'selected' : '')) ?>>Site Backup</option>
                </select>
                </div>
                <div style='color:red;'>
            <?php echo form_error('backup_type'); ?>
            </div>
        </div>
        <div class="form-group ">
            <label for="file_type" class="control-label col-lg-3" required>File Type</label>
            <div class="col-lg-6">
                <select class="form-control select2" name="file_type" id="file_type">
                <option value="" selected disabled>File Type</option>
                 <option value="1" <?php echo (isset($success) && strlen($success) ? '' : (set_value('file_type') == 1 ? 'selected' : '')) ?>>ZIP</option>
                 <option value="2" <?php echo (isset($success) && strlen($success) ? '' : (set_value('file_type') == 2 ? 'selected' : '')) ?>>GZIP</option>
                </select>
                </div>
                <div style='color:red;'>
            <?php echo form_error('file_type'); ?>
            </div>
        </div>
        </fieldset>
        <div class="form-group">
        <div class="col-lg-offset-3 col-lg-6">
        <button type="submit" name="backup" value="backup" class="button btn btn-primary">Get Backup</button>
            
        </div>
    </div>
<?php
echo form_close();
?>
