<?php
/**
* Database Backup
* - Used to make backups of the database system
* @author Adan Rehtla <adan.rehtla@aamcommercial.com.au>
*/
class Databasebackup_model extends CI_Model {

    private $CI;
    private $lpFilePath = APPPATH . 'cache/backup/';

    // Tables to ignore when doing a backup
    private $laIgnoreTables = array(
        'ci_sessions',
        'any_other_tables_to_ignore'
    );




    /**
     * Constructor
     * @author Adan Rehtla <adan.rehtla@aamcommercial.com.au>
     */
    function __construct() {
        parent::__construct();
        $this->CI = &get_instance();
    }



    /**
     * Check and empty the cache location for storing the backups during transit
     * @author Adan Rehtla <adan.rehtla@aamcommercial.com.au>
     * @return json                     Result
     */
    public function check_cache() {

        $arrRet = false;

        if( ! file_exists( $this->lpFilePath ) ) {
            $arrRet = mkdir( $this->lpFilePath );
            $arrRet = true;
        }

        $files = glob( $this->lpFilePath . '*' ); // get all file names present in folder
        foreach( $files as $file ){ // iterate files
            if( is_file( $file ) ) unlink( $file ); // delete the file
        }

        return (object) $arrRet;
    }



    /**
     * Build a list of tables to backup
     * @author Adan Rehtla <adan.rehtla@aamcommercial.com.au>
     * @return json                     Result
     */
    /*public function find_tables() {

        $arrRet = false;
        $katoch ='1';

        if($katoch =='1') {   

            $this->db->_protect_identifiers = FALSE;
            $this->db->start_cache();
            $this->db->distinct();
            $this->db->select("user_id");
            $this->db->from("club_admin");
            //$this->db->where("table_schema", $this->db->dbprefix . $this->db->database);
            $this->db->stop_cache();

            // Get List of all tables to backup
            $laTableNameResult = $this->db->get()->result();

            $this->db_read->flush_cache();

            if( !empty($laTableNameResult) ) {
                foreach($laTableNameResult as $loTableName) {
                    if( ! in_array( $loTableName->table_name, $this->laIgnoreTables ) ) {
                        $arrRet[] = $loTableName->table_name;
                    }
                }
            }

            $this->db_read->_protect_identifiers = TRUE;
        }

        return (object) $arrRet;
    }*/



    /**
     * Backup the database to Amazon S3
     * @author Adan Rehtla <adan.rehtla@aamcommercial.com.au>
     * @param  interger $lpUserId       Logged in User ID
     * @return json                     Result
     */
    public function backup() {

        ini_set('max_execution_time', 3600); // 3600 seconds = 60 minutes
        ini_set('memory_limit', '-1'); // unlimited memory
        //error_reporting(E_ALL); // show errors
        //ini_set('display_errors', 1); // display errors

        $arrRet = array();

        $lpBackupCount = 0;
        $zipNumFiles = 0;
        $zipStatus = 0;

        if($zipStatus=='0') {

            $this->db->save_queries = false;

            $this->load->dbutil( $this->db_read, TRUE );

          //  $laBackupTables = $this->find_tables();
//print_r($laBackupTables);die;
$laBackupTables = 'katoch';

            if( !empty($laBackupTables) && $this->check_cache() ) {

               

                    $lpFilename = $this->lpFilePath . 'backup-' . $lpTableName . '-' . date('Ymd-His') . '.sql';

                    $prefs = array(
                        'tables'                => $lpTableName,
                        'format'                => 'sql',
                        'filename'              => $lpFilename,
                        'add_drop'              => TRUE,
                        'add_insert'            => TRUE,
                        'newline'               => "\n",
                        'foreign_key_checks'    => TRUE,
                        'chunk_size'            => 1000
                    );

                    if( !file_exists( $lpFilename ) ){
                        if( $this->dbutil->backup( $prefs ) ) {
                            $lpBackupCount++;
                        }
                    }

                    sleep(1); // used to help reduce the CPU load

                    //break; // used to debug and testing to only process the first database table
                

                try {
                    // ZIP up all the individual GZIPs
                    $zip = new ZipArchive();
                    $zip_file = $this->lpFilePath . '-' . date('Ymd-His') . '.zip';
                    $zip_files = glob( $this->lpFilePath . 'backup-*' ); // get all file names present in folder starting with 'backup-'

                    if( $zip->open( $zip_file, ZIPARCHIVE::CREATE ) !== TRUE ) {
                        exit("cannot open <$zip_file>\n");
                    }

                    if( !empty($zip_files) ) {
                        foreach( $zip_files as $file_to_zip ) { // iterate files
                            $zip->addFile( $file_to_zip, basename( $file_to_zip ) );
                        }
                    }

                    $zipNumFiles = $zip->numFiles;
                    $zipStatus = $zip->status;
                    $zip->close();

                } catch (Vi_exception $e) {
                    print( strip_tags( $e->errorMessage() ) );
                }

                // Upload the file to Amazon S3    
                // REMOVED AWS S3 UPLOAD CODE
                // REMOVED AWS S3 UPLOAD CODE
                // REMOVED AWS S3 UPLOAD CODE

                // Clean up cache files
                $this->check_cache();
            }

            $this->db->save_queries = true;

        }

        if( !empty($lpBackupCount) && !empty($zip) && !empty($zip_file) ) {
            return (object) array( 'success'=>true, 'totalTables'=>$lpBackupCount, 'zipNumFiles'=>$zipNumFiles, 'zipStatus'=>$zipStatus, 'zipFile'=>$zip_file );
        } else {
            return (object) array( 'success'=>false );
        }
    }




}