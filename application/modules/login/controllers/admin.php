<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*	
 *	@Developer  : Hr Katoch
 *	16th April, 2019
 *	Creative Item
 *	clubmate.com
 */
 class Admin extends CI_Controller
{
    
    
    function __construct()
    {
        parent::__construct();
		$this->load->model('admin_login');
		$this->load->library("session");
		$this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        /*cache control*/
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 17 JAN 1991 05:00:00 GMT");
    }
	//**********Default function, redirects to logged in user area*************//
	public function index(){
	
	   $this->template
		->set_layout('admin_login')
		->enable_parser(FALSE)
		->title(SITE_NAME.'  Admin Login')
		->build('login');	
		}
	
	public function login(){
		/*if ($this->session->userdata('admin_login') == 1){
            redirect(ADMIN_PATH.'/dashboard', 'refresh');exit;
          }*/
			$this->form_validation->set_rules('email', 'Email', 'required');
		    $this->form_validation->set_rules('password', 'Password', 'required');
	   if ($this->form_validation->run() == FALSE)
		{
			redirect(ADMIN_LOGIN_PATH);exit;
		}
		else
		{
			$credential	=	array(	'email' => $this->input->post('email') , 'password' => md5($this->input->post('password')));
			if($credential){
					/***Checking login credential for superadmin ****/
				$query = $this->admin_login->dologin($credential);
				if ($query['admin_id']){
						$this->session->set_userdata('admin_id', $query['admin_id']);
						$this->session->set_userdata('name', $query['username']);
						$this->session->set_userdata('login_type', 'admin');
						$this->session->set_userdata('admin_login', '1');
						$this->session->set_flashdata('success', 'Admin Login successfully');
						redirect(ADMIN_PATH);exit;
						//redirect(ADMIN_PATH, 'refresh');exit;
				}else{
					$this->session->set_flashdata('error','Invalid Email or password');
					
					redirect(ADMIN_LOGIN_PATH, 'refresh');exit;	
				}
			}
		  }
	}
    
}
