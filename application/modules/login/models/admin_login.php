<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_login extends CI_Model {
	
	function __construct()
    {
        parent::__construct();
    }
	
	function clear_cache()
	{
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
	}
	function get_type_name_by_id($type,$type_id='',$field='name')
	{
		return	$this->db->get_where($type,array($type.'_id'=>$type_id))->row()->$field;	
	}
	public function dologin($credential){
        #get data
        $query        = $this->db->select('*')
                            ->from('club_admin')
                            ->where($credential)
                            ->limit(1)
                            ->get();
        if ($query->num_rows() > 0){
            $id = $query->row()->admin_id;
            $this->update_last_login($id);
            #user data
            $admin_id = $query->row()->admin_id;
         } else {
            $admin_id = '';
        }
        #retun array
        $arrReturn = array('admin_id' => $admin_id,'status'=>$query->row()->status,'username'=>$query->row()->username);
        return $arrReturn;
	}
	  /*
   * @ this function is used to  Update user   login  info
   */
  public function update_last_login($id){
	$ip_addr      = $this->general->get_real_ipaddr();
	$this->db->update('club_admin', array('last_login' => date('Y-m-d h:m:s'),'ip_address' =>$ip_addr), array('admin_id' => $id));
	return $this->db->affected_rows() == 1;
}
	
	////////IMAGE URL//////////
	function get_image_url($type = '' , $id = '')
	{
		if(file_exists('uploads/'.$type.'_image/'.$id.'.jpg'))
			$image_url	=	base_url().'uploads/'.$type.'_image/'.$id.'.jpg';
		else
			$image_url	=	base_url().'uploads/user.jpg';
			
		return $image_url;
	}
}

