<div class="card card-login mx-auto mt-5">
	<div class="card-header">Login</div>
		<div class="card-body">
			<?php if ($this->session->flashdata('success')) { ?>
			<h3>
				  
			<?php $flash_Message =$this->session->flashdata('success');
			echo "<div style='color:green;'>$flash_Message<div>";
			 ?>
			</h3>
			<?php } ?>
			<?php if ($this->session->flashdata('error')) { ?>
			<h3>
			<?php $flash_Message =$this->session->flashdata('error');
			echo "<div style='color:red;'>$flash_Message<div>"; ?>
			</h3>
			<?php } ?>


			<?php echo form_open(base_url() . 'login/admin/login' , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>
				<div class="form-group">
					<div class="form-label-group">
					<input type="email" id="inputEmail" class="form-control" name="email" placeholder="Email address" required="required" autofocus="autofocus">
					<label for="inputEmail">Email address</label>
					</div>
				</div>
				<div class="form-group">
					<div class="form-label-group">
					<input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required="required">
					<label for="inputPassword">Password</label>
					</div>
				</div>
				<div class="form-group">
					<div class="checkbox">
					<label>
						<input type="checkbox" value="remember-me">
						Remember Password
					</label>
					</div>
				</div>
				<button type="submit"class="btn btn-primary btn-block">
									Login</button>
				<?php echo form_close();?>
				<div class="text-center">
				<a class="d-block small" href="forgot-password.html">Forgot Password?</a>
				</div>
		 </div>
	</div>