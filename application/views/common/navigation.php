<body id="page-top">

  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="<?php echo site_url(ADMIN_PATH);?>">ClubMate</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
      <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar Search -->
 <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
        <div class="input-group-append">
          <button class="btn btn-primary" type="button">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>


    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0">
     <li class="nav-item dropdown no-arrow mx-1">
        <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-bell fa-fw"></i>
          <?php $notification_count = $this->db->limit(9)->get_where('club_subscriptionplanmaster_activate',array('notification_activated_status'=>'2'))->num_rows();
                $notification_counts = $this->db->get_where('club_subscriptionplanmaster_activate',array('notification_activated_status'=>'2'))->num_rows();?>
          <span class="badge badge-danger"><?=$notification_count; 
           if($notification_counts >'9'){ echo '+';}?></span>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <?php $msg_count = $this->db->limit(9)->get_where('club_reports',array('msg_seen_Status'=>'2'))->num_rows();
                $msg_counts = $this->db->get_where('club_reports',array('msg_seen_Status'=>'2'))->num_rows();?>
      <li class="nav-item dropdown no-arrow mx-1">
        <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-envelope fa-fw"></i>
          <span class="badge badge-danger"><?=$msg_count; 
           if($msg_counts >'9'){ echo '+';}?></span>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="messagesDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item dropdown no-arrow">
        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-user-circle fa-fw"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
          <a class="dropdown-item" href="<?php echo base_url().'admin/admin/settings'?>">Settings</a>
         
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo base_url().'admin/admin/logout'?>" data-toggle="modal" data-target="#logoutModal">Logout</a>
        </div>
      </li>
    </ul>

  </nav>
  <div id="wrapper">

<!-- Sidebar -->
<ul class="sidebar navbar-nav">
  <li class="nav-item <?php if ($page_name == 'dashboard') echo ' active'; ?> " >
	<a class="nav-link" href="<?php echo site_url(ADMIN_PATH);?>">
	  <i class="fas fa-fw fa-tachometer-alt"></i>
	  <span>Dashboard</span>
	</a>
  </li>

  <li class="nav-item dropdown <?php if ($page_name == 'user_list') echo ' active'; ?>">
	<a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  <i class="fa fa-users" aria-hidden="true"></i>
	  <span>Users</span>
	</a>
	<div class="dropdown-menu" aria-labelledby="pagesDropdown">
  <a class="dropdown-item" href="<?php echo site_url(ADMIN_PATH."/add_news_users");?>">
  <i class="fa fa-plus" aria-hidden="true"></i>
	  <span>Add user</span>
	</a>
  <a class="dropdown-item" href="<?php echo site_url(ADMIN_PATH."/users_list");?>">
  <i class="fa fa-id-card" aria-hidden="true"></i>
	  <span>All  users</span>
	</a>

	  <a class="dropdown-item" href="<?php echo site_url(ADMIN_PATH."/active_users_list");?>">
    <i class="fa fa-unlock" aria-hidden="true"></i>
	  <span>Active users</span>
	</a>
  <a class="dropdown-item" href="<?php echo site_url(ADMIN_PATH."/block_users_list");?>">
  <i class="fa fa-lock" aria-hidden="true"></i>
	  <span>Blocked Users</span>
	</a>
  </li>



  <li class="nav-item dropdown <?php if ($page_name == 'events') echo ' active'; ?>">
	<a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	  <i class="fas fa-fw fa-folder"></i>
	  <span>Entries</span>
	</a>
	<div class="dropdown-menu" aria-labelledby="pagesDropdown">
	  <a class="dropdown-item" href="<?php echo site_url(ADMIN_PATH."/events_new");?>">
  <i class="fa fa-calendar" aria-hidden="true"></i>
	  <span>New Entries</span>
	</a>
  <a class="dropdown-item" href="<?php echo site_url(ADMIN_PATH."/events_old");?>">
  <i class="fa fa-calendar" aria-hidden="true"></i>
	  <span>Old Entries</span>
	</a>
  </li>

  <li class="nav-item dropdown <?php if ($page_name == 'pairup') echo ' active'; ?>">
	<a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  <i class="fa fa-cubes" aria-hidden="true"></i>
	  <span>Pairup</span>
	</a>
	<div class="dropdown-menu" aria-labelledby="pagesDropdown">
	  <a class="dropdown-item" href="<?php echo site_url(ADMIN_PATH."/pairup_requests");?>">
  <i class="fa fa-users" aria-hidden="true"></i>
	  <span>Requested Pair Ups</span>
	</a>
  <a class="dropdown-item" href="<?php echo site_url(ADMIN_PATH."/pairup_accepted");?>">
  <i class="fa fa-users" aria-hidden="true"></i>
	  <span>Accepted Pair Ups </span>
	</a>
  </li>

  
  <li class="nav-item dropdown <?php if ($page_name == 'transaction') echo ' active'; ?>">
	<a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  <i class="fa fa-credit-card" aria-hidden="true"></i>
	  <span>Transactions</span>
	</a>
	<div class="dropdown-menu" aria-labelledby="pagesDropdown">
	  <a class="dropdown-item" href="<?php echo site_url(ADMIN_PATH."/month_transaction");?>">
    <i class="fa fa-cc-mastercard" aria-hidden="true"></i>
	  <span>Monthly Plan </span>
	</a>
  <a class="dropdown-item" href="<?php echo site_url(ADMIN_PATH."/year_transaction");?>">
  <i class="fa fa-cc-mastercard" aria-hidden="true"></i>
	  <span>Yearly Plan </span>
	</a>
  </li>

  <li class="nav-item dropdown <?php if ($page_name == 'Packages') echo ' active'; ?>">
	<a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  <i class="fa fa-window-restore" aria-hidden="true"></i>
	  <span>Packages</span>
	</a>
	<div class="dropdown-menu" aria-labelledby="pagesDropdown">
	  <a class="dropdown-item" href="<?php echo site_url(ADMIN_PATH."/month_plan");?>">
    <i class="fa fa-cc-mastercard" aria-hidden="true"></i>
	  <span>Monthly Plans</span>
	</a>
  <a class="dropdown-item" href="<?php echo site_url(ADMIN_PATH."/year_plan");?>">
  <i class="fa fa-cc-mastercard" aria-hidden="true"></i>
	  <span>Yearly Plans</span>
	</a>
  </li>

<li  class="nav-item <?php if ($page_name == 'members') echo ' active'; ?>">
	<a class="nav-link" href="<?php echo site_url(ADMIN_PATH."/members");?>">
  <i class="fa fa-user-plus" aria-hidden="true"></i>
	  <span>Group Members</span>
	</a> 
	</li>
	
  <li  class="nav-item <?php if ($page_name == 'report') echo ' active'; ?>">
	<a class="nav-link" href="<?php echo site_url(ADMIN_PATH."/report");?>">
  <i class="fa fa-flag-checkered" aria-hidden="true"></i>
	  <span>Reported Users</span>
	</a>
  </li>
    <li  class="nav-item <?php if ($page_name == 'system_backup') echo ' active'; ?>">
	<a class="nav-link" href="<?php echo site_url(BACK_UP_PATH."");?>">
  <i class="fa fa-download" aria-hidden="true"></i>
	  <span>System BackUp</span>
	</a>
  </li>
    <li  class="nav-item <?php if ($page_name == 'contact_us') echo ' active'; ?>">
	<a class="nav-link" href="<?php echo site_url(ADMIN_PATH."/contact_us");?>">
  <i class="fa fa-cubes" aria-hidden="true"></i>
	  <span>Contact us</span>
	</a>
  </li>
</ul>