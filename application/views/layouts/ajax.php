<?php echo doctype("html5");?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php 
        $meta = array(array("name"=>"charset","content"=>"utf-8"),
        array("name"=>"viewport","content"=>"width=device-width, initial-scale=1.0"),
        array("name"=>"description","content"=>""),
        array("name"=>"author","content"=>""),
        array("name"=>"keywords","content"=>"")
        );?>
    <?php 	echo meta($meta); ?>
	<title><?php echo $template['title']; ?></title>
    <base href="<?php echo site_url();?>" />	 
    <?php
		if(isset($additionalcss) && count($additionalcss)>0)
		{
			for($ac = 0; $ac < count($additionalcss); $ac++)
			{
				echo link_tag($additionalcss[$ac]);
			}
		}
	?>  
    <?php
		echo link_tag('assets/admin/images/favicon.ico',"shortcut icon", "");
	?> 
</head>
<!-- END HEAD -->
<body>	
    <?php echo $template['body']; ?>
</body>
	
<script src="<?php echo ADMIN_ASSETS_PATH; ?>js/jquery-1.8.3.min.js"></script>
<?php
if(isset($additionaljs) && count($additionaljs)>0)
{
	for($aj = 0; $aj < count($additionaljs); $aj++){
		echo '<script src="'.$additionaljs[$aj].'" type="text/javascript" language="javascript"></script>';
	}
}
?>  

<script>
	jQuery(document).ready(function() {    
	   //App.init(); // initlayout and core plugins
	   <?php echo @$onloadjs; ?>
	   //$("#trigger_fullscreen").click();
	});
</script>    
</html>