
	<link rel="stylesheet" href="<?=ADMIN_ASSETS_PATH?>js/datatables/responsive/css/datatables.responsive.css">
	<link rel="stylesheet" href="<?=ADMIN_ASSETS_PATH?>js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="<?=ADMIN_ASSETS_PATH?>js/select2/select2.css">
   	<!-- Bottom Scripts -->
	<script src="<?=ADMIN_ASSETS_PATH?>js/gsap/main-gsap.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/bootstrap.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/joinable.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/resizeable.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/neon-api.js"></script>
    <script src="<?=ADMIN_ASSETS_PATH?>js/jquery.validate.min.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/fullcalendar/fullcalendar.min.js"></script>
    <script src="<?=ADMIN_ASSETS_PATH?>js/bootstrap-datepicker.js"></script>
    <script src="<?=ADMIN_ASSETS_PATH?>js/fileinput.js"></script>
    
    <script src="<?=ADMIN_ASSETS_PATH?>js/jquery.dataTables.min.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/datatables/TableTools.min.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/dataTables.bootstrap.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/datatables/jquery.dataTables.columnFilter.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/datatables/lodash.min.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/datatables/responsive/js/datatables.responsive.js"></script>
    <script src="<?=ADMIN_ASSETS_PATH?>js/select2/select2.min.js"></script>
    
	<script src="<?=ADMIN_ASSETS_PATH?>js/neon-calendar.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/neon-chat.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/neon-custom.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/neon-demo.js"></script>





<!--  DATA TABLE EXPORT CONFIGURATIONS-->                      
<script type="text/javascript">

	jQuery(document).ready(function($)
	{
		

		var datatable = $("#table_export").dataTable();
		
		$(".dataTables_wrapper select").select2({
			minimumResultsForSearch: -1
		});
	});
		
</script>
		
    

    
