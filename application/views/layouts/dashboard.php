
<!DOCTYPE html>
<html lang="en" dir="<?php if ($text_align == 'right-to-left') echo 'rtl';?>">
<head>
	


    <?php
	$system_name	=	$this->db->get_where('settings' , array('type'=>'system_name'))->row()->description;
	$system_title	=	$this->db->get_where('settings' , array('type'=>'system_title'))->row()->description;
	$text_align  	=	$this->db->get_where('settings' , array('type'=>'text_align'))->row()->description;
	$account_type 	=	$this->session->userdata('login_type');
	?>
	<title><?php  echo $page_title;?> | <?php echo $system_title;?></title>
    
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content=" School Manager Pro - Creativeitem" />
	<meta name="author" content="Creativeitem" />
	
	
	
    <link rel="stylesheet" href="<?=ADMIN_ASSETS_PATH?>js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="<?=ADMIN_ASSETS_PATH?>css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="<?=ADMIN_ASSETS_PATH?>css/bootstrap.css">
	<link rel="stylesheet" href="<?=ADMIN_ASSETS_PATH?>css/neon-core.css">
	<link rel="stylesheet" href="<?=ADMIN_ASSETS_PATH?>css/neon-theme.css">
	<link rel="stylesheet" href="<?=ADMIN_ASSETS_PATH?>css/neon-forms.css">
    
	<link rel="stylesheet" href="<?=ADMIN_ASSETS_PATH?>css/custom.css">
	<?php if ($text_align == 'right-to-left') :?>
    	<link rel="stylesheet" href="<?=ADMIN_ASSETS_PATH?>css/neon-rtl.css">
	<?php endif;?>
	<script src="<?=ADMIN_ASSETS_PATH?>js/jquery-1.11.0.min.js"></script>

	<!--[if lt IE 9]><script src="<?=ADMIN_ASSETS_PATH?>js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<link rel="shortcut icon" href="<?=ADMIN_ASSETS_PATH?>images/favicon.png">
    <link rel="stylesheet" href="<?=ADMIN_ASSETS_PATH?>css/font-icons/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?=ADMIN_ASSETS_PATH?>js/vertical-timeline/css/component.css">
    <link rel="stylesheet" href="<?=ADMIN_ASSETS_PATH?>js/datatables/responsive/css/datatables.responsive.css">
	
</head>
<body class="page-body" >
	<div class="page-container <?php if ($text_align == 'right-to-left') echo 'right-sidebar';?>" >
		
		 <?=$this->load->view('common/navigation');?>	
		<div class="main-content">
		

			   <?=$this->load->view('common/header');?>

           <h3 style="margin:20px 0px; color:#818da1; font-weight:200;">
           	<i class="entypo-right-circled"></i> 
				<?php echo $page_title;?>
           </h3>
         <?php echo $template['body']; ?>
			

		
        <!-- END HEADER -->
        <!-- NAVIGATION -->
        <?=$this->load->view('common/footer');?>

		</div>
		<?php //include 'chat.php';?>
        	
	</div>
     <?=$this->load->view('common/modal');?>
<link rel="stylesheet" href="<?=ADMIN_ASSETS_PATH?>js/datatables/responsive/css/datatables.responsive.css">
	<link rel="stylesheet" href="<?=ADMIN_ASSETS_PATH?>js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="<?=ADMIN_ASSETS_PATH?>js/select2/select2.css">
   	<!-- Bottom Scripts -->
	<script src="<?=ADMIN_ASSETS_PATH?>js/gsap/main-gsap.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/bootstrap.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/joinable.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/resizeable.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/neon-api.js"></script>
    <script src="<?=ADMIN_ASSETS_PATH?>js/jquery.validate.min.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/fullcalendar/fullcalendar.min.js"></script>
    <script src="<?=ADMIN_ASSETS_PATH?>js/bootstrap-datepicker.js"></script>
    <script src="<?=ADMIN_ASSETS_PATH?>js/fileinput.js"></script>
    
    <script src="<?=ADMIN_ASSETS_PATH?>js/jquery.dataTables.min.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/datatables/TableTools.min.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/dataTables.bootstrap.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/datatables/jquery.dataTables.columnFilter.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/datatables/lodash.min.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/datatables/responsive/js/datatables.responsive.js"></script>
    <script src="<?=ADMIN_ASSETS_PATH?>js/select2/select2.min.js"></script>
    
   
	<script src="<?=ADMIN_ASSETS_PATH?>js/neon-calendar.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/neon-chat.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/neon-custom.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/neon-custom-ajax.js"></script>
	<script src="<?=ADMIN_ASSETS_PATH?>js/neon-demo.js"></script>


	<script src="<?=ADMIN_ASSETS_PATH?>/js/toastr.js"></script>

   	<!-- Bottom Scripts -->
	




  



<!-- SHOW TOASTR NOTIFIVATION -->
<?php if ($this->session->flashdata('flash_message') != ""):?>

<script type="text/javascript">
	toastr.success('<?php echo $this->session->flashdata("flash_message");?>');
</script>
<script type="text/javascript">
	toastr.warning('<?php echo $this->session->flashdata("flash_error");?>');
</script>

<?php endif;?>

<!--  DATA TABLE EXPORT CONFIGURATIONS-->                      
<script type="text/javascript">

	jQuery(document).ready(function($)
	{
		

		var datatable = $("#table_export").dataTable();
		
		$(".dataTables_wrapper select").select2({
			minimumResultsForSearch: -1
		});
	});
		
</script>
		
    
</body>
</html>
    
    
